package net.asiafm.view.infiniteCycleView;

import static net.asiafm.view.infiniteCycleView.Utils.setupImage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.viewpager.widget.PagerAdapter;
import java.util.List;
import net.asiafm.R;


public class HorizontalPagerAdapter extends PagerAdapter {


    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private  List<Integer> imagerResource;

    public HorizontalPagerAdapter(final Context context, final List<Integer> imagerResource) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.imagerResource = imagerResource;
    }

    @Override
    public int getCount() {
        return imagerResource.size();
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view;

        view = mLayoutInflater.inflate(R.layout.radio_item, container, false);
        setupImage(mContext, (ImageView) view.findViewById(R.id.image), imagerResource.get(position));
        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }
}
