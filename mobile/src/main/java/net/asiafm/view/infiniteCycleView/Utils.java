package net.asiafm.view.infiniteCycleView;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;


/**
 * Created by GIGAMOLE on 8/18/16.
 */
public class Utils {

    public static void setupImage(final Context context, final ImageView imageView, final int imageResource) {
        setupImage(context, imageView, imageResource, -1);
    }

    public static void setupImage(final Context context, final ImageView imageView, final int imageResource, final int position) {

        Glide.with(context)
                .load(imageResource)
                .error(imageResource)
                .into(imageView);
    }

//    public static class LibraryObject {
//
//        private int imagerRes;
//        private String mUrl;
//        private String mImage;
//        private String[] mImages = null;
//
//        public LibraryObject(final int image) {
//            imagerRes = image;
//
//        }
//        public LibraryObject(final String url, final String image) {
//            mUrl = url;
//            mImage = image;
//        }
//
//        public LibraryObject(final String url, final String[] images) {
//            mUrl = url;
//            mImages = images;
//        }
//
//        public String getUrl() {
//            return mUrl;
//        }
//
//        public String getImage() {
//            return mImage;
//        }
//
//        public String[] getImages() {
//            return mImages;
//        }
//
//        public int getImagerRes() {
//            return imagerRes;
//        }
//
//
//    }
}
