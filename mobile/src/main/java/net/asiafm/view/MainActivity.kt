package net.asiafm.view

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import kotlinx.android.synthetic.main.rv_item_style1.*
import kotlinx.coroutines.*
import net.asiafm.Global
import net.asiafm.R
import net.asiafm.asiaAPI.viewModel.MainObjectModel
import net.asiafm.databinding.ActivityMainBinding
import net.asiafm.listener.MediaPlayListenerManager
import net.asiafm.listener.MediaPlayerListener
import net.asiafm.listener.alarm.PlayReceiver
import net.asiafm.shared.MusicService
import net.asiafm.shared.MusicService.Companion.getIsStartMediaPlayer
import net.asiafm.shared.MusicService.Companion.isStartMediaPlayer
import net.asiafm.shared.MusicService.Companion.setIsStartMediaPlayer
import net.asiafm.shared.PlaybackStatus
import net.asiafm.shared.RadioManager
import net.asiafm.storagedData.StorageDataMaintain
import net.asiafm.util.StatusBarUtil
import net.asiafm.view.ui.OnlineFragment
import net.asiafm.view.ui.OnlineFragment.Companion.errorDefaultImg
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class MainActivity : AppCompatActivity(), MediaPlayerListener {

    private lateinit var binding: ActivityMainBinding
    private val mainScope = MainScope()
    private val timeInterval = 60000 * 3L
    private var job: Job? = null

    lateinit var broadcastReceiver: BroadcastReceiver


    companion object {
        val TAG = "MainActivity_Tag"
        val mainObjectModel = MainObjectModel()

        @SuppressLint("StaticFieldLeak")
        var radioManagerInstance: RadioManager? = null
        var statusBarHeight = 0
        var streamURL: String = "https://stream.rcs.revma.com/xpgtqc74hv8uv"
        var isSignIn: Boolean = false

        var radioManagerDefaultImage: Int = R.drawable.icon_logo_927


        fun setStatusBarHeightValues(value: Int) {
            statusBarHeight = value
        }


        @BindingAdapter("mainPlayerImage")
        @JvmStatic
        fun mainPlayerImage(imageView: ImageView, url: String) {
            Glide.with(imageView.context).load(url).placeholder(R.drawable.main_player_default_927)
                .error(R.drawable.main_player_default_927)
                .transform(CenterCrop(), RoundedCorners(8))
                .into(imageView)

        }

    }

    init {
        radioManagerInstance = RadioManager.with(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = mainObjectModel

        StatusBarUtil.transparentStatusBar(window)
        MediaPlayListenerManager.getInstance().registerListener(this)
        Global.getScreenSize(this)

        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupWithNavController(binding.bottomNavigationView, navController)

        broadcastReceiver = ChangeRadioChannel(binding, radioManagerInstance!!)


        // register mediaPlayer server
        registeredBroadCast()

        binding.btnPlay.setImageResource(R.drawable.icon_mediaplayer_play)

        binding.btnPlay.setOnClickListener {

            if (!isStartMediaPlayer) {
                isStartMediaPlayer = true
                OnlineFragment().callRecentlyMusicApiData(OnlineFragment.currentRadioIndex)
                radioManagerInstance!!.playOrPause(streamURL)
            } else {
                isStartMediaPlayer = false
                radioManagerInstance!!.playOrPause(streamURL)
            }

            viewControl(isStartMediaPlayer)

        }

        autoStartPlayMusic()
        startUpdatesRecentMusic()

        if (StorageDataMaintain.getIsOpenAlarmStartClock(this)) {
            openAlarmStartTime()
        }

        if (StorageDataMaintain.getIsOpenAlarmStopClock(this)) {
            openAlarmStopTime()
        }

    }


    @Subscribe
    fun onEvent(status: String) {
        when (status) {
            PlaybackStatus.LOADING -> {
            }

            PlaybackStatus.PLAYING -> {
                isStartMediaPlayer = true
                OnlineFragment.binding.imgPlay.visibility = View.INVISIBLE
                OnlineFragment.binding.tvLive.visibility = View.VISIBLE
                OnlineFragment.binding.tvOnAir.visibility = View.INVISIBLE
            }

            PlaybackStatus.PAUSED -> {
                OnlineFragment.binding.imgPlay.visibility = View.VISIBLE
                OnlineFragment.binding.tvLive.visibility = View.INVISIBLE
                OnlineFragment.binding.tvOnAir.visibility = View.VISIBLE
            }

            PlaybackStatus.ERROR -> Toast.makeText(this, R.string.no_stream, Toast.LENGTH_SHORT)
                .show()
        }


        binding.btnPlay.setImageResource(
            if (status == PlaybackStatus.PLAYING) R.drawable.icon_mediaplayer_pause
            else R.drawable.icon_mediaplayer_play
        )
    }

    override fun isPlayListener(isPlay: Boolean) {
        radioManagerInstance!!.stopRadio()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)

    }

    override fun onDestroy() {
        super.onDestroy()

        stopService(Intent(this, MusicService::class.java))
        radioManagerInstance!!.unbind()
        RadioManager.resetRadioManager()
        unregisterReceiver(broadcastReceiver)
        radioManagerInstance = null
        mainScope.cancel()
        reset()
        finish()
    }

    override fun onResume() {
        super.onResume()
        radioManagerInstance!!.bind()
    }


    private fun reset() {
        setIsStartMediaPlayer(false)
        errorDefaultImg = R.drawable.main_default_927
        OnlineFragment.logOutReset(0)
        mainObjectModel.setImageUrl("")
        mainObjectModel.setMusicSinger("I LOVE ASIA FM")
        mainObjectModel.setMusicName("92.7 亞洲電台")
    }

    //=================================================================================================//
    private fun registeredBroadCast() {
        val intentFilter = IntentFilter("changeChannel")
        registerReceiver(broadcastReceiver, intentFilter)

    }

    class ChangeRadioChannel(_binding: ActivityMainBinding, _radioManager: RadioManager) : BroadcastReceiver() {

        private val radioManager = _radioManager
        val binding = _binding

        @SuppressLint("SetTextI18n")
        override fun onReceive(context: Context?, intent: Intent) {

            val extras = intent.extras

            if (extras != null) {
                streamURL = extras.getString("channelStream")!!
                when (streamURL) {
                    // 92.7
                    "https://stream.rcs.revma.com/xpgtqc74hv8uv" -> {
                        radioManagerDefaultImage = R.drawable.main_player_default_927
                        binding.imgMediaplayerPhoto.setImageResource(R.drawable.main_player_default_927)
                        binding.tvMediaPlayerTitle.text = "92.7 亞洲電台"
                        binding.tvMediaPlayerSubtitle.text = "I LOVE ASIA FM"
                    }
                    // 92.3
                    "https://stream.rcs.revma.com/kydend74hv8uv" -> {
                        radioManagerDefaultImage = R.drawable.main_player_default_923
                        binding.imgMediaplayerPhoto.setImageResource(R.drawable.main_player_default_923)
                        binding.tvMediaPlayerTitle.text = "92.3 亞太電台"
                        binding.tvMediaPlayerSubtitle.text = "I LOVE ASIA FM"
                    }
                    // 89.5
                    "https://stream.rcs.revma.com/e0tdah74hv8uv" -> {
                        radioManagerDefaultImage = R.drawable.main_player_default_895
                        binding.imgMediaplayerPhoto.setImageResource(R.drawable.main_player_default_895)
                        binding.tvMediaPlayerTitle.text = "89.5 飛揚電台"
                        binding.tvMediaPlayerSubtitle.text = "I LOVE FLY RADIO FM"
                    }
                }
            }

            if (getIsStartMediaPlayer()) {
                Log.d(TAG, "channelStream = ${extras!!.getString("channelStream")}")
                radioManager.playOrPause(extras.getString("channelStream"))

            }
        }
    }

    //=================================================================================================//
    //  開 APP後判斷是否撥放音樂
    private fun autoStartPlayMusic() = lifecycleScope.launch {
        delay(4000L)
        if (StorageDataMaintain.getIsStartRadio(this@MainActivity)) {
            setIsStartMediaPlayer(true)
            radioManagerInstance!!.playOrPause(streamURL)
        }
    }


    //  每 3分鐘刷新最新10首歌
    private fun startUpdatesRecentMusic() {
        stopUpdatesRecentMusic()
        job = mainScope.launch {
            while (true) {
                OnlineFragment().callRecentlyMusicApiData(OnlineFragment.currentRadioIndex)
                delay(timeInterval)
            }
        }
    }

    private fun stopUpdatesRecentMusic() {
        job?.cancel()
        job = null
    }

//=================================================================================================//
    /** 進主畫面註冊鬧鐘
     * https://developer.android.com/training/scheduling/alarms?hl=zh-cn#kotlin
     * https://xiang1023.blogspot.com/2017/11/android-alarmmanager.html
     * 请勿将闹钟的触发时间设置得过于精确。 使用 setInexactRepeating() 代替 setRepeating()。当您使用 setInexactRepeating() 时，
     * Android 会同步来自多个应用的重复闹钟，并同时触发它们。这可以减少系统必须唤醒设备的总次数，从而减少耗电量。
     * 从 Android 4.4（API 级别 19）开始，所有重复闹钟都是不精确的。请注意，尽管 setInexactRepeating() 是对 setRepeating() 的改进，
     * 但如果应用的每个实例都大约在同一时间连接至服务器，仍会使服务器不堪重负。
     * 因此，如上所述，对于网络请求，请为您的闹钟添加一些随机性。
     */

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun openAlarmStartTime() {

        val hour = StorageDataMaintain.getAlarmStartTime(this@MainActivity)["Hour"]!!.toInt()
        val minutes = StorageDataMaintain.getAlarmStartTime(this@MainActivity)["Minutes"]!!.toInt()

        // Set the alarm to start
        val calendar: Calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(Calendar.HOUR_OF_DAY, hour)
            set(Calendar.MINUTE, minutes)
            set(Calendar.SECOND, 0)
        }

        val alarmMgr = getSystemService(ALARM_SERVICE) as AlarmManager
        val alarmIntent =
            Intent(this, PlayReceiver::class.java).putExtra("msg", "start alarm from mainActivity")
                .let { intent ->
                    PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                }

        // With setInexactRepeating(), you have to use one of the AlarmManager interval
        if (isOverAlarmTime(hour, minutes)) {
            alarmMgr.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                alarmIntent
            )
        }

    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun openAlarmStopTime() {

        val hour = StorageDataMaintain.getAlarmStopTime(this@MainActivity)["Hour"]!!.toInt()
        val minutes = StorageDataMaintain.getAlarmStopTime(this@MainActivity)["Minutes"]!!.toInt()

        // Set the alarm to start
        val calendar: Calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(Calendar.HOUR_OF_DAY, hour)
            set(Calendar.MINUTE, minutes)
            set(Calendar.SECOND, 0)
        }

        val alarmMgr = getSystemService(ALARM_SERVICE) as AlarmManager
        val alarmIntent =
            Intent(this, PlayReceiver::class.java).putExtra("msg", "stop alarm from mainActivity")
                .let { intent ->
                    PendingIntent.getBroadcast(this, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                }

        // With setInexactRepeating(), you have to use one of the AlarmManager interval
        if (isOverAlarmTime(hour, minutes)) {
            alarmMgr.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                alarmIntent
            )
        }

    }

    private fun isOverAlarmTime(hour: Int, minutes: Int): Boolean {

        // 鬧鐘時間
        val settingTime = Calendar.getInstance()
        settingTime.set(Calendar.HOUR_OF_DAY, hour)
        settingTime.set(Calendar.MINUTE, minutes)

        //  現在時間
        val currentTime: Calendar = Calendar.getInstance()

        return settingTime > currentTime
    }

//=================================================================================================//

    private fun viewControl(isStartMediaPlayer: Boolean) {
        val img = findViewById<ImageView>(R.id.img_play)
        val tvLive = findViewById<TextView>(R.id.tv_live)
        val tvOnAir = findViewById<TextView>(R.id.tv_on_air)

        if (img != null && tvLive != null && tvOnAir != null) {
            if (isStartMediaPlayer) {
                img.visibility = View.INVISIBLE
                tvOnAir.visibility = View.INVISIBLE
                tvLive.visibility = View.VISIBLE
            } else {
                img.visibility = View.VISIBLE
                tvOnAir.visibility = View.VISIBLE
                tvLive.visibility = View.INVISIBLE
            }
        }
    }


}