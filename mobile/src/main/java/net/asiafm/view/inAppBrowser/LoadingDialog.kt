package net.asiafm.view.inAppBrowser

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import net.asiafm.listener.WebLoadingListenerManager
import net.asiafm.R
import net.asiafm.databinding.DialogLoadingBinding
import net.asiafm.view.ui.login.LoginActivity


class LoadingDialog : DialogFragment() {
    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout(LoginActivity.windowWidth , LoginActivity.windowHeight)
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        val binding: DialogLoadingBinding = DataBindingUtil.inflate(inflater , R.layout.dialog_loading , container , false)

        //  Loading 過程中. User 手動按下物理返回鍵， 防呆處理。
        dialog!!.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(dialog: DialogInterface , keyCode: Int , event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    WebLoadingListenerManager.getInstance().sendBroadCast()
                    dialog.cancel()
                    return true
                }
                return false
            }
        })
        return binding.root
    }
}