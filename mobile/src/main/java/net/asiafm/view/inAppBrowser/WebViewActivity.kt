package net.asiafm.view.inAppBrowser

import android.os.Bundle
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil

import net.asiafm.R
import net.asiafm.databinding.ActivityWebViewBinding
import net.asiafm.listener.WebLoadingListener
import net.asiafm.listener.WebLoadingListenerManager

class WebViewActivity : AppCompatActivity() , WebLoadingListener {

    private var loadingDialog: LoadingDialog? = null
    private lateinit var binding: ActivityWebViewBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this , R.layout.activity_web_view)
        WebLoadingListenerManager.getInstance().registerListener(this)

        this.window.statusBarColor = ContextCompat.getColor(this , R.color.blue00BFFF)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(this , R.color.blue00BFFF))
        binding.toolbar.materialToolbar.title = ""

        if (intent.getStringExtra("ToolbarTitle") != null) {
            binding.toolbar.tvToolbarTitle.text = intent.getStringExtra("ToolbarTitle")
        } else {
            binding.toolbar.tvToolbarTitle.text = "亞洲電台"
        }


        // Loading Animate
        loadingDialog = LoadingDialog()
        loadingDialog!!.show(this.supportFragmentManager , "loading dialog")

        //獲得值
        val intent = intent

//        binding.toolbar.toolbarLogo.setImageBitmap(PdfTool().pdfToBitmap(this, R.raw.logo)[0])
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { onBackPressed() }

        // 開啟網頁設定
        //支持javascript
        binding.webview.settings.javaScriptEnabled = true

        // 設置可以支持缩放
        binding.webview.settings.setSupportZoom(true)
        binding.webview.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        binding.webview.webViewClient = WebViewClient()
//        binding.webview.loadUrl(youtubeVideoUrl)
        //傳值
        when {
            intent.getStringExtra("LinkURL") != null -> {
                val youtubeVideoUrl = "https://www.youtube.com/watch?v=${intent.getStringExtra("LinkURL")}"
                binding.webview.loadUrl(youtubeVideoUrl)
            }

            intent.getStringExtra("QueryString") != null -> {
                val youtubeVideoUrl = "https://www.youtube.com/results?search_query=${intent.getStringExtra("QueryString")}"
                binding.webview.loadUrl(youtubeVideoUrl)
            }

            intent.getStringExtra("extraUrl") != null -> {
                binding.webview.loadUrl(intent.getStringExtra("extraUrl")!!)
            }
        }

    }

    override fun notifyViewCancelCall() {
        loadingDialog!!.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        WebLoadingListenerManager.getInstance().unRegisterListener(this)
    }


}