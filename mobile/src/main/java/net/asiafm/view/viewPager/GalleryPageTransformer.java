package net.asiafm.view.viewPager;


import android.view.View;

import androidx.viewpager.widget.ViewPager;

public class GalleryPageTransformer implements ViewPager.PageTransformer {
    private static final float MAX_ROTATION=20.0f;
    private static final float MIN_SCALE=0.75f;
    private static final float MAX_TRANSLATE=20.0f;
    private static final float MAX_TRANSLATEY= 40.0f;

    @Override
    public void transformPage(View page, float position) {

        if(position < -1 ) {
            page.setTranslationX(MAX_TRANSLATE);
            page.setScaleX(MIN_SCALE);
            page.setScaleY(MIN_SCALE);
            page.setRotationY(-MAX_ROTATION);
            page.setTranslationY(-MAX_TRANSLATEY);

        }
        else if(position<=0) {
            page.setTranslationX(-MAX_TRANSLATE*position);
            float scale=MIN_SCALE+(1-MIN_SCALE)*(1.0f+position);
            page.setScaleX(scale);
            page.setScaleY(scale);
            page.setTranslationY(MAX_TRANSLATEY*position);
            page.setRotationY(MAX_ROTATION*position);
        }
        else if(position<=1) {
            page.setTranslationX(-MAX_TRANSLATE*position);
            page.setTranslationY(-MAX_TRANSLATEY*position);
            float scale=MIN_SCALE+(1-MIN_SCALE)*(1.0f-position);
            page.setScaleX(scale);
            page.setScaleY(scale);
            page.setRotationY(MAX_ROTATION*position);
        }
        else {
            page.setTranslationX(-MAX_TRANSLATE);
            page.setTranslationY(-MAX_TRANSLATEY);
            page.setScaleX(MIN_SCALE);
            page.setScaleY(MIN_SCALE);
            page.setRotationY(MAX_ROTATION);
        }
    }
}
