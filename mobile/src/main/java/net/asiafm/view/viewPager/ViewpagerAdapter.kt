package net.asiafm.view.viewPager

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide

import net.asiafm.R
import net.asiafm.view.ui.OnlineFragment

class ViewpagerAdapter(private val mContext: Context) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup , position: Int): Any {
        var aaa = position
        aaa %= OnlineFragment.mImages.size
        val info: Int = OnlineFragment.mImages[aaa]
        val view: View = LayoutInflater.from(mContext).inflate(R.layout.banner_img_layout , null)
        val ivPortrait: ImageView = view.findViewById<View>(R.id.img) as ImageView
        Glide.with(mContext).load(info).into(ivPortrait)

        container.addView(view)
        return view
    }

    override fun getCount(): Int {
        return Integer.MAX_VALUE
    }

    override fun isViewFromObject(view: View , `object`: Any): Boolean {
        return view === `object`
    }

    override fun destroyItem(container: ViewGroup , position: Int , `object`: Any) {
        val view = `object` as View
        container.removeView(view)
    }
}