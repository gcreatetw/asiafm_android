package net.asiafm.view.viewPager

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import net.asiafm.R
import net.asiafm.util.PdfTool

/**
 * Created by Administrator on 2018/11/28.
 * banner的适配器
 */
class BannerPagerAdapter(list: List<Int> , private val mContext: Context) : PagerAdapter() {
    private var mList: List<Int>? = null
    private var defaultImg = R.drawable.icon_back //默认图片
    private var mRoundCorners = -1
    private var mMaxNumber //最大banner数
            = 0

    /**
     * 默认
     * @param defaultImg
     */
    fun setDefaultImg(defaultImg: Int) {
        this.defaultImg = defaultImg
    }

    /**
     * 设置圆角
     * @param mRoundCorners
     */
    fun setmRoundCorners(mRoundCorners: Int) {
        this.mRoundCorners = mRoundCorners
    }

    /**
     * 点击回调
     */
    interface OnClickImagesListener {
        fun onImagesClick(position: Int)
    }

    private var mImagesListener: OnClickImagesListener? = null
    fun setOnClickImagesListener(listener: OnClickImagesListener?) {
        mImagesListener = listener
    }

    override fun getCount(): Int {
        return Int.MAX_VALUE
    }

    override fun isViewFromObject(view: View , `object`: Any): Boolean {
        return view === `object`
    }

    override fun destroyItem(container: ViewGroup , position: Int , `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(container: ViewGroup , position: Int): Any {
        val view = LayoutInflater.from(mContext).inflate(R.layout.banner_img_layout , container , false)
        val imageView = view.findViewById<View>(R.id.img) as ImageView
        val index = position % mMaxNumber
        LoadImage(mList!![index] , imageView)
        //OnClick
        imageView.setOnClickListener { mImagesListener!!.onImagesClick(index) }
        container.addView(view)
        return view
    }

    /**
     * 加载图片
     */
    fun LoadImage(url: Int , imageview: ImageView?) {
        val dm: Drawable = BitmapDrawable(mContext.resources , PdfTool().pdfToBitmap(mContext ,url)[0])
        if (mRoundCorners == -1) {
            Glide.with(mContext).load(dm).centerCrop().dontAnimate().placeholder(defaultImg) // 加载中图片
                .error(defaultImg) // 加载失败图片
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC) //设置磁盘缓存
                .into(imageview!!)
        } else {
            Glide.with(mContext).load(url).centerCrop().dontAnimate().placeholder(defaultImg) // 加载中图片
                .error(defaultImg) // 加载失败图片
                .transform(RoundedCorners(mRoundCorners)) //
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC) //设置磁盘缓存
                .into(imageview!!)
        }
    }

    init {
        // this.mList = list;
        if (mList == null) {
            mList = list
        }
        if (list.size > 9) {
            mMaxNumber = 9
        } else {
            mMaxNumber = list.size
        }
    }
}