package net.asiafm.view.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.GravityCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.android.material.appbar.AppBarLayout
import com.zhpan.bannerview.constants.PageStyle
import com.zhpan.bannerview.utils.BannerUtils
import net.asiafm.R
import net.asiafm.asiaAPI.RetrofitInstance
import net.asiafm.asiaAPI.dataModel.ApiObjectArticle
import net.asiafm.asiaAPI.viewModel.OnlineListViewModel
import net.asiafm.databinding.FragmentOnlineBinding
import net.asiafm.listener.AppBarLayoutStateChangeListener
import net.asiafm.model.adapter.BigNewsAdapter
import net.asiafm.model.adapter.SideMenuListAdapter
import net.asiafm.shared.MusicService.Companion.getIsStartMediaPlayer
import net.asiafm.shared.MusicService.Companion.isStartMediaPlayer
import net.asiafm.shared.MusicService.Companion.setIsStartMediaPlayer
import net.asiafm.storagedData.StorageDataMaintain
import net.asiafm.view.MainActivity
import net.asiafm.view.inAppBrowser.WebViewActivity
import net.asiafm.view.infiniteCycleView.HorizontalPagerAdapter
import net.asiafm.view.ui.dialog.TeachDialogFragment
import net.asiafm.view.ui.login.LoginActivity
import net.asiafm.view.ui.login.OtpResultDialogFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/** 線上收聽
 * */
class OnlineFragment : Fragment() {

    private lateinit var navController: NavController
    private var callArticle: Call<ApiObjectArticle>? = null
    private var isAsiaRadio = true

    private val handler: Handler = Handler(Looper.getMainLooper())
    private var runnable: Runnable = Runnable { setupBinding(makeApiCall()) }

    companion object {
        private const val TAG = "AsiaLog"

        lateinit var binding: FragmentOnlineBinding
        var errorDefaultImg = R.drawable.main_default_927

        var viewModel: OnlineListViewModel? = null

        //  亞洲 92.7，亞太92.3，飛揚89.5
        val mImages = intArrayOf(R.drawable.icon_logo_927, R.drawable.icon_logo_895, R.drawable.icon_logo_923)
        var isFistTimeInApp = true
        var currentRadioItem = 0


        var currentRadioIndex = 0

        fun logOutReset(index: Int) {
            currentRadioItem = index
            currentRadioIndex = index
        }

        @BindingAdapter("setImage")
        @JvmStatic
        fun onlineFragmentAlbumImage(imageView: ImageView, url: String?) {
            if (url.isNullOrEmpty()) {
                Glide.with(imageView.context).load(url).placeholder(errorDefaultImg).centerCrop()
                    .error(errorDefaultImg).into(imageView)
            } else {
                Glide.with(imageView.context).load(url).placeholder(R.drawable.main_default_927).centerCrop()
                    .error(R.drawable.icon_default_recently).into(imageView)
            }

        }
    }

    override fun onStart() {
        super.onStart()
        when (currentRadioItem) {
            //  亞洲 FM 92.7
            0 -> {
                requireActivity().window.statusBarColor = getColor(requireActivity(), R.color.blue1748C7)
                binding.mCollapsingLayout.setContentScrimColor(getColor(requireActivity(), R.color.blue1748C7))
                binding.onlineBackground.setBackgroundResource(R.mipmap.gradian_blue)
                Glide.with(requireActivity()).load(R.drawable.main_default_927).into(binding.imgRadioBanner)
            }
            //  亞太 FM 92.3
            2 -> {
                requireActivity().window.statusBarColor = getColor(requireActivity(), R.color.redE50606)
                binding.mCollapsingLayout.setContentScrimColor(getColor(requireActivity(), R.color.redE50606))
                binding.onlineBackground.setBackgroundResource(R.mipmap.gradian_red)
                Glide.with(requireActivity()).load(R.drawable.main_default_923).into(binding.imgRadioBanner)
            }
            //  飛揚調頻 FM 89.5
            1 -> {
                requireActivity().window.statusBarColor = getColor(requireActivity(), R.color.purple8A1BDF)
                binding.mCollapsingLayout.setContentScrimColor(getColor(requireActivity(), R.color.purple8A1BDF))
                binding.onlineBackground.setBackgroundResource(R.mipmap.gradian_purple)
                Glide.with(requireActivity()).load(R.drawable.main_default_895).into(binding.imgRadioBanner)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_online, container, false)
        navController = NavHostFragment.findNavController(this)

        initView()
        setInfiniteCycleViewPager()
        getModelInstance()

        if (StorageDataMaintain.getIsFirstInstallApp(requireContext())) {

            TeachDialogFragment().show(requireActivity().supportFragmentManager, "teach_dialog")
            StorageDataMaintain.setIsFirstInstallApp(requireContext(), false)
        }

//        if (isFistTimeInApp){
//            AdvertisingDialogFragment().show(requireActivity().supportFragmentManager , "advertising_dialog")
//
//        }

        //  XX大頭條
        getAsiaBigNewsData("asia927-bignews")

        try {
            handler.postDelayed(runnable, 2000)
        } catch (e: Exception) {
            Log.d(TAG, e.toString())
        }

//        setAdvertise()
//        setAdvertise2()

        binding.toolbarRadio.setOnClickListener {
            binding.mAppBarLayout.setExpanded(true)
            binding.nestView.scrollTo(0, 0)
        }

        binding.imgPlay.setOnClickListener {
            setIsStartMediaPlayer(true)
            MainActivity.radioManagerInstance!!.playOrPause(MainActivity.streamURL)
            binding.imgPlay.visibility = View.INVISIBLE
            binding.tvLive.visibility = View.VISIBLE
            binding.tvOnAir.visibility = View.INVISIBLE
            callRecentlyMusicApiData(currentRadioItem)
        }


        binding.imgShadow.setOnClickListener {

            if (binding.imgPlay.visibility == View.INVISIBLE || binding.imgPlay.visibility == View.GONE) {
                binding.imgPlay.visibility = View.VISIBLE
                isStartMediaPlayer = false
                MainActivity.radioManagerInstance!!.playOrPause(MainActivity.streamURL)
            }
        }

        return binding.root
    }

    private fun initView() {
        setViewLayoutParams()
        setToolBar()
        setSideMenu()

        if (StorageDataMaintain.getIsStartRadio(requireActivity())) {
            // 如果設定開 APP就撥放廣播
            binding.imgPlay.visibility = View.GONE
        }

        binding.toolbarRadio.setImageResource(mImages[0])

        binding.mAppBarLayout.addOnOffsetChangedListener(object : AppBarLayoutStateChangeListener() {
            override fun onStateChanged(appBarLayout: AppBarLayout?, toolbarStatus: ToolbarStatus) {
                when (toolbarStatus) {
                    ToolbarStatus.EXPANDED -> binding.toolbarRadio.visibility = View.INVISIBLE
                    ToolbarStatus.COLLAPSED -> binding.toolbarRadio.visibility = View.VISIBLE
                    ToolbarStatus.INTERMEDIATE -> binding.toolbarRadio.visibility = View.INVISIBLE
                }
            }
        })

        /** Recycler params
         * 最近撥放**/
        binding.onlineRecentlyMusic.tvTitle.visibility = View.GONE
        binding.onlineRecentlyMusic.componentRv.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        binding.onlineRecentlyMusic.componentRv.isNestedScrollingEnabled = false

        // 如果正在撥放.隱藏撥放紐
        if (isStartMediaPlayer) {
            binding.imgPlay.visibility = View.GONE
            binding.tvLive.visibility = View.VISIBLE
            binding.tvOnAir.visibility = View.INVISIBLE
        }

    }

    /** Toolbars **/
    private fun setToolBar() {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).setSupportActionBar(binding.mToolBar)
        (activity as AppCompatActivity).supportActionBar!!.title = ""
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        binding.mToolBar.setNavigationOnClickListener {
            binding.coordinator.openDrawer(GravityCompat.START)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_iteml, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.toolbar_setting -> {
                if (MainActivity.isSignIn) {
                    navController.navigate(R.id.action_onlineFragment_to_settingFragment)
                } else {
                    val args = Bundle()
                    args.putBoolean("feedbackResult", false)
                    args.putString("description", "請登入會員<br>使用個人設定功能")
                    val otpResultDialogFragment = OtpResultDialogFragment()
                    otpResultDialogFragment.arguments = args
                    otpResultDialogFragment.show(requireActivity().supportFragmentManager, "OTP_dialog")
                }

            }
        }
        return super.onOptionsItemSelected(item)
    }

    /** Side menu*/
    private fun setSideMenu() {
        binding.navView.layoutParams.width = (LoginActivity.windowWidth * 0.5).toInt()
        binding.rvSideMenu.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        val sideMenuItem = mutableListOf("ASIA大頭條", "飛揚大頭條", "生活情報讚", "活動報馬仔", "好康贈獎", "得獎名單")
        val adapter = SideMenuListAdapter(sideMenuItem)
        binding.rvSideMenu.adapter = adapter
        adapter.setOnItemClickListener { _, _, position ->
            binding.coordinator.closeDrawer(GravityCompat.START)
            val action = OnlineFragmentDirections.actionOnlineFragmentToSideMenuFragment1(sideMenuItem[position])
            navController.navigate(action)
        }
    }

    /** 設定畫面參數 **/
    private fun setViewLayoutParams() {
        /** 設置畫面離邊距離 */
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0, MainActivity.statusBarHeight, 0, 0)
        binding.coordinator.layoutParams = params

        binding.clTopContent.layoutParams.height = (LoginActivity.windowWidth * 1.05).toInt()
        binding.clMusicAlbum.layoutParams.width = (LoginActivity.windowWidth * 0.8).toInt()
        binding.clMusicAlbum.layoutParams.height = (LoginActivity.windowWidth * 0.8).toInt()

        // DrawerLayout背景设置的問题，去除黑色半透明遮罩
        binding.coordinator.setScrimColor(Color.TRANSPARENT)

    }

    /** 首頁上方切換頻道 functions **/
    private fun setInfiniteCycleViewPager() {
        /** 飛揚 89.5 (紫) , 亞太 92.3 (紅)  , 亞洲 92.7 (藍)  * */
        val radioIconResource = mutableListOf(R.drawable.icon_logo_927, R.drawable.icon_logo_895, R.drawable.icon_logo_923)
        binding.hicvp.adapter =
            HorizontalPagerAdapter(requireActivity(), radioIconResource)
        binding.hicvp.setCurrentItem(currentRadioItem, false)


        binding.hicvp.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
                binding.onlineRecentlyMusic.componentRv.isEnabled = false
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {

                var remainder = position
                remainder %= radioIconResource.size
                currentRadioItem = remainder
                binding.toolbarRadio.setImageResource(mImages[remainder])
                when (remainder) {
                    //  亞洲 FM 92.7
                    0 -> {
                        isAsiaRadio = true
                        currentRadioIndex = 0
                        errorDefaultImg = R.drawable.main_default_927
                        requireActivity().window.statusBarColor = getColor(requireActivity(), R.color.blue1748C7)
                        binding.onlineBackground.setBackgroundResource(R.mipmap.gradian_blue)
                        binding.mCollapsingLayout.setContentScrimColor(getColor(requireActivity(), R.color.blue1748C7))
                        binding.onlineBanner.tvTitle.text = "亞洲大頭條"

                        changeChannel(0)
                        callRecentlyMusicApiData(0)

                        if (callArticle != null) {
                            callArticle!!.cancel()
                            getAsiaBigNewsData("asia927-bignews")
                        }

                    }
                    //  亞太 FM 92.3
                    2 -> {
                        isAsiaRadio = true
                        currentRadioIndex = 2
                        errorDefaultImg = R.drawable.main_default_923

                        requireActivity().window.statusBarColor = getColor(requireActivity(), R.color.redE50606)
                        binding.onlineBackground.setBackgroundResource(R.mipmap.gradian_red)
                        binding.mCollapsingLayout.setContentScrimColor(getColor(requireActivity(), R.color.redE50606))
                        binding.onlineBanner.tvTitle.text = "亞太大頭條"

                        changeChannel(2)
                        callRecentlyMusicApiData(2)

                        if (callArticle != null) {
                            callArticle!!.cancel()
                            getAsiaBigNewsData("asia923-bignews")
                        }
                    }
                    //  飛揚調頻 FM 89.5
                    1 -> {
                        isAsiaRadio = false
                        currentRadioIndex = 1
                        errorDefaultImg = R.drawable.main_default_895

                        binding.onlineBackground.setBackgroundResource(R.mipmap.gradian_purple)
                        requireActivity().window.statusBarColor = getColor(requireActivity(), R.color.purple8A1BDF)
                        binding.mCollapsingLayout.setContentScrimColor(getColor(requireActivity(), R.color.purple8A1BDF))

                        binding.onlineBanner.tvTitle.text = "飛揚大頭條"

                        changeChannel(1)
                        callRecentlyMusicApiData(1)

                        if (callArticle != null) {
                            callArticle!!.cancel()
                            getFlyBigNewsData()
                        }
                    }
                }
                callApiData()
            }
        })

    }

    /** 亞洲大頭條**/
    private fun getAsiaBigNewsData(postSlug: String) {
        callArticle = RetrofitInstance.getAsiaApiInstance()!!.getBigNewsArticle(postSlug, 5)
        callArticle!!.enqueue(object : Callback<ApiObjectArticle> {
            override fun onResponse(call: Call<ApiObjectArticle>, response: Response<ApiObjectArticle>) {
                if (response.isSuccessful) {
                    val objectBigNews = response.body()!!
                    val articleImages = mutableListOf<String>()

                    for (element in objectBigNews.info) {
                        articleImages.add(element.imgUrl)
                    }

                    if (isAdded && activity != null) {
                        binding.onlineBanner.bannerView.setIndicatorSliderGap(BannerUtils.dp2px(6f))

                            .setIndicatorView(binding.onlineBanner.indicatorView)
                            .setIndicatorSliderColor(getColor(requireActivity(), R.color.black), getColor(requireActivity(), R.color.grayC0C1C1))
                            .setLifecycleRegistry(lifecycle).setRevealWidth(resources.getDimensionPixelOffset(R.dimen.dp_20))
                            .setPageMargin(resources.getDimensionPixelOffset(R.dimen.dp_20)).setPageStyle(PageStyle.MULTI_PAGE_SCALE)
                            .setRoundCorner(resources.getDimensionPixelOffset(R.dimen.dp_10))
                            .setOnPageClickListener { _: View?, _: Int ->
                                val currentItem: Int = binding.onlineBanner.bannerView.currentItem
                                val intent = Intent(context, WebViewActivity::class.java)
                                intent.putExtra("ToolbarTitle", binding.onlineBanner.tvTitle.text)
                                intent.putExtra("extraUrl", objectBigNews.info[currentItem].guid)
                                startActivity(intent)

                            }.setAdapter(BigNewsAdapter()).setCanLoop(true).setInterval(5000)
                            .setScrollDuration(2000)
                            .setAutoPlay(true).create(articleImages)

                        binding.onlineBanner.bannerView.requestDisallowInterceptTouchEvent(false)
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectArticle>, t: Throwable) {
            }
        })
    }

    /** 飛揚大頭條**/
    private fun getFlyBigNewsData() {
        callArticle = RetrofitInstance.getFlyRadioApiInstance()!!.getArticle("flyradio_news", 5)
        callArticle!!.enqueue(object : Callback<ApiObjectArticle> {
            override fun onResponse(call: Call<ApiObjectArticle>, response: Response<ApiObjectArticle>) {
                if (response.isSuccessful) {

                    val objectBigNews = response.body()!!
                    val articleImages = mutableListOf<String>()

                    for (element in objectBigNews.info) {
                        articleImages.add(element.imgUrl)
                    }

                    if (isAdded && activity != null) {
                        binding.onlineBanner.bannerView.setIndicatorSliderGap(BannerUtils.dp2px(6f))
                            .setIndicatorView(binding.onlineBanner.indicatorView)
                            .setIndicatorSliderColor(getColor(requireActivity(), R.color.black), getColor(requireActivity(), R.color.grayC0C1C1))
                            .setLifecycleRegistry(lifecycle).setPageMargin(resources.getDimensionPixelOffset(R.dimen.dp_20))
                            .setRevealWidth(resources.getDimensionPixelOffset(R.dimen.dp_20)).setPageStyle(PageStyle.MULTI_PAGE_SCALE)
                            .setRoundCorner(resources.getDimensionPixelOffset(R.dimen.dp_10))
                            .setOnPageClickListener { _: View?, _: Int ->
                                val currentItem: Int = binding.onlineBanner.bannerView.currentItem
                                val intent = Intent(context, WebViewActivity::class.java)
                                intent.putExtra("ToolbarTitle", "飛揚大頭條")
                                intent.putExtra("extraUrl", objectBigNews.info[currentItem].guid)
                                startActivity(intent)

                            }.setAdapter(BigNewsAdapter()).setCanLoop(true).setInterval(5000)
                            .setScrollDuration(2000)
                            .setAutoPlay(true).create(articleImages)

                        binding.onlineBanner.bannerView.requestDisallowInterceptTouchEvent(false)
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectArticle>, t: Throwable) {
            }
        })
    }


    /** 廣告一**/
//    private fun setAdvertise() {
//        val dm: Drawable = BitmapDrawable(resources , PdfTool().pdfToBitmap(requireActivity() , R.raw.default_advertise)[0])
//        Glide.with(requireActivity()).load(dm).into(binding.onlineAdvertise.imgAdvertise)
//    }

    /** 廣告二**/
//    private fun setAdvertise2() {
//        val dm: Drawable = BitmapDrawable(resources , PdfTool().pdfToBitmap(requireActivity() , R.raw.default_advertise2)[0])
//        Glide.with(requireActivity()).load(dm).into(binding.onlineAdvertise2.imgAdvertise)
//    }


    @SuppressLint("SetTextI18n")
    private fun changeChannel(channelId: Int) {

        val intent = Intent("changeChannel") //設定廣播識別碼

        when (channelId) {
            //  亞洲 FM 92.7
            0 -> {
                Glide.with(requireActivity()).load(R.drawable.main_default_927).transform(CenterCrop(), RoundedCorners(25))
                    .into(binding.imgRadioBanner)
                intent.putExtra("channelStream", "https://stream.rcs.revma.com/xpgtqc74hv8uv")
                viewModel!!.musicSinger.set("I LOVE ASIA FM")
                viewModel!!.musicName.set("92.7 亞洲電台")
            }
            //  飛揚調頻 FM 89.5
            1 -> {
                Glide.with(requireActivity()).load(R.drawable.main_default_895).transform(CenterCrop(), RoundedCorners(25))
                    .into(binding.imgRadioBanner)
                intent.putExtra("channelStream", "https://stream.rcs.revma.com/e0tdah74hv8uv")
                viewModel!!.musicSinger.set("I LOVE FLY RADIO FM")
                viewModel!!.musicName.set("89.5 飛揚電台")

            }
            //  亞太 FM 92.3
            2 -> {
                Glide.with(requireActivity()).load(R.drawable.main_default_923).transform(CenterCrop(), RoundedCorners(25))
                    .into(binding.imgRadioBanner)
                intent.putExtra("channelStream", "https://stream.rcs.revma.com/kydend74hv8uv")
                viewModel!!.musicSinger.set("I LOVE ASIA FM")
                viewModel!!.musicName.set("92.3 亞太電台")
            }
        }

        requireActivity().sendBroadcast(intent)
    }

    private fun getModelInstance(): OnlineListViewModel? {
        if (viewModel == null) {
            viewModel = ViewModelProvider(requireActivity()).get(OnlineListViewModel::class.java)
        }
        return viewModel
    }

    private fun setupBinding(viewModel: OnlineListViewModel) {
        binding.setVariable(net.asiafm.BR.viewModel, viewModel)
        binding.executePendingBindings()

        if (binding.tvSinger.text.isEmpty() && binding.tvMusicName.text.isEmpty()) {
            viewModel.musicSinger.set("I LOVE ASIA FM")
            viewModel.musicName.set("92.7 亞洲電台")
        }


        //  最近播放
        binding.onlineRecentlyMusic.componentRv.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
            adapter = viewModel.getRecentlyMusicAdapter()
            hasFixedSize()
        }

        //  生活情報讚
        binding.onlineLifeNews.tvTitle.text = "生活情報讚"
        binding.onlineLifeNews.componentRv.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
            adapter = viewModel.getLifeNewsAdapter()
            hasFixedSize()
        }

        //  活動報馬仔
        binding.onlineCouponActivity.tvTitle.text = "活動報馬仔"
        binding.onlineCouponActivity.componentRv.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
            adapter = viewModel.getCouponActivityAdapter()
            hasFixedSize()
        }

        //  好康贈獎
        binding.onlineCouponGift.tvTitle.text = "好康贈禮"
        binding.onlineCouponGift.componentRv.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
            adapter = viewModel.getCouponGiftAdapter()
            hasFixedSize()
        }

    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): OnlineListViewModel {
        // 最近10首歌
        viewModel!!.getRecentlyMusicDataObserver().observe(requireActivity(), {
            if (it != null)
                viewModel!!.setRecentlyMusicAdapterData(it.latestMusic)
        })

        //  生活情報讚
        viewModel!!.getLifeNewsDataObserver().observe(requireActivity(), {
            if (it != null)
                viewModel!!.setLifeNewsAdapterData(it.info)
        })

        //  活動報馬仔
        viewModel!!.getCouponActivityDataObserver().observe(requireActivity(), {
            if (it != null)
                viewModel!!.setCouponActivityAdapterData(it.info)

        })

        //  好康贈獎
        viewModel!!.getCouponGiftDataObserver().observe(requireActivity(), {
            if (it != null)
                viewModel!!.setCouponGiftAdapterData(it.info)
        })

        callRecentlyMusicApiData(currentRadioItem)
        callApiData()

        return viewModel!!
    }

    private fun callApiData() {
        OnlineListViewModel.callCancel()
        viewModel!!.getLifeNewsApiData(isAsiaRadio)
        viewModel!!.getCouponActivityApiData(isAsiaRadio)
        viewModel!!.getCouponGiftApiData(isAsiaRadio)
    }

    fun callRecentlyMusicApiData(selectedIndex: Int) {
        OnlineListViewModel.callRecentlyMusicCancel()
        viewModel!!.getRecentlyMusicApiData(selectedIndex)
    }


    override fun onPause() {
        super.onPause()
        isFistTimeInApp = false
        callArticle!!.cancel()
    }

    override fun onStop() {
        super.onStop()
        callArticle!!.cancel()
    }

    override fun onDestroy() {
        super.onDestroy()
        OnlineListViewModel.callCancel()
        viewModel = null
        handler.removeCallbacks(runnable)
    }

}

