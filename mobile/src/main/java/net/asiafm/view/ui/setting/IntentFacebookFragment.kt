package net.asiafm.view.ui.setting

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import net.asiafm.Global
import net.asiafm.R
import net.asiafm.databinding.FragmentSetting5Binding
import net.asiafm.view.MainActivity
import net.asiafm.view.inAppBrowser.WebViewActivity

/** 官方臉書粉絲團*/
class IntentFacebookFragment : Fragment() {

    private lateinit var binding: FragmentSetting5Binding

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_setting5 , container , false)

        initView()
        viewAction()

        return binding.root
    }


    private fun initView() {
        /** 設置畫面離邊距離 */
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT , FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0 , MainActivity.statusBarHeight , 0 , 0)
        binding.llContent.layoutParams = params
        setToolBar()
    }

    /** Toolbars **/
    private fun setToolBar() {
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.blue00BFFF)
        binding.toolbarFacebook.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity() , R.color.blue00BFFF))
        binding.toolbarFacebook.tvToolbarTitle.text = "官方臉書粉絲團"
        binding.toolbarFacebook.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbarFacebook.materialToolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    private fun viewAction() {
        binding.tvFacebookFm927.setOnClickListener {
            if (Global.isAppInstalled(requireActivity() , "com.facebook.katana")) {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.data = Uri.parse("fb://page/115266555162342")
                startActivity(intent)
            } else {
                val intent = Intent(context , WebViewActivity::class.java)
                intent.putExtra("extraUrl" , "https://www.facebook.com/asiaradiofamily")
                startActivity(intent)
            }
        }

        binding.tvFacebookFm923.setOnClickListener {
            if (Global.isAppInstalled(requireActivity() , "com.facebook.katana")) {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.data = Uri.parse("fb://page/1009171459190589")
                startActivity(intent)
            } else {
                val intent = Intent(context , WebViewActivity::class.java)
                intent.putExtra("extraUrl" , "https://www.facebook.com/asiafm923")
                startActivity(intent)
            }
        }

        binding.tvFacebookFm895.setOnClickListener {
            if (Global.isAppInstalled(requireActivity() , "com.facebook.katana")) {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.data = Uri.parse("fb://page/237126950007339")
                startActivity(intent)
            } else {
                val intent = Intent(context , WebViewActivity::class.java)
                intent.putExtra("extraUrl" , "https://www.facebook.com/flyradioFM89.5")
                startActivity(intent)
            }
        }

    }


}