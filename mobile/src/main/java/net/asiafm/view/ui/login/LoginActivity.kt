package net.asiafm.view.ui.login

import android.app.Activity
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import net.asiafm.Global
import net.asiafm.R
import net.asiafm.databinding.ActivityLoginBinding
import net.asiafm.util.StatusBarUtil
import net.asiafm.view.MainActivity

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private val LogTag = "firebase"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        StatusBarUtil.transparentStatusBar(window)
        getWindowSize(this)

        replaceFragment(this, LoginStart0Fragment())
        MainActivity.setStatusBarHeightValues(Global.getStatusBarHeight(this))


        // firebase function
        Global.isAppOpen = true
        FirebaseMessaging.getInstance().subscribeToTopic("news")
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) return@OnCompleteListener
            val token = task.result
            Log.d(LogTag, "firebase token : $token")
        })

    }


    override fun onBackPressed() {
        val fragmentManager = supportFragmentManager
        if (fragmentManager.backStackEntryCount > 1) {
            fragmentManager.popBackStackImmediate()
        } else {
            finish()
        }
    }

    // 取得當前 MainActivity fragment container 是哪個fragment
    companion object {

        var windowWidth = 0
        var windowHeight = 0

        @JvmName("getWindowSize")
        @JvmStatic
        fun getWindowSize(mActivity: Activity) {
            val dm = DisplayMetrics()
            mActivity.windowManager.defaultDisplay.getMetrics(dm)
            windowHeight = dm.heightPixels
            windowWidth = dm.widthPixels
        }

        fun replaceFragment(mainActivity: FragmentActivity, fragment: Fragment) {
            val fragmentManager = mainActivity.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out, R.anim.slide_left_in, R.anim.slide_right_out)
            fragmentTransaction.replace(R.id.container, fragment, "")
            fragmentTransaction.addToBackStack(fragment.toString())
            fragmentTransaction.commit()
        }
    }

}