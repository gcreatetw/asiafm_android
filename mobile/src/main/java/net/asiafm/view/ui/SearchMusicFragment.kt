package net.asiafm.view.ui

import android.annotation.SuppressLint
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager


import net.asiafm.view.infiniteCycleView.HorizontalPagerAdapter

import net.asiafm.R
import net.asiafm.asiaAPI.viewModel.SearchMusicListViewModel
import net.asiafm.databinding.FragmentSearchMusicBinding
import net.asiafm.util.PdfTool
import net.asiafm.view.MainActivity
import net.asiafm.view.ui.login.LoginActivity
import java.text.SimpleDateFormat
import java.util.*


class SearchMusicFragment : Fragment() {

    private lateinit var binding: FragmentSearchMusicBinding
    private var currentRadioIndex = 1

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_search_music , container , false)

        initView()
        setInfiniteCycleViewPager()
        setSpinnerData()
        setupBinding(makeApiCall())

        return binding.root
    }

    private fun initView() {

        binding.clBackground.layoutParams.height = (LoginActivity.windowHeight * 0.36).toInt()

        //  初始設定在亞洲電台
        binding.clBackground.background = BitmapDrawable(resources , PdfTool().pdfToBitmap(requireActivity() , R.raw.bg_blue)[0])
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.blue1748C7)

        //  因背景延伸到status bar ，固最上層元件需補回status bar 高度
        val newLayoutParams: ConstraintLayout.LayoutParams = binding.tvShowList.layoutParams as ConstraintLayout.LayoutParams
        newLayoutParams.topMargin = MainActivity.statusBarHeight
        binding.tvShowList.layoutParams = newLayoutParams

        //  下拉高度設定 6 個 item 高
        binding.spinnerTime.setDropdownMaxHeight((resources.getDimension(R.dimen.ms__item_height) * 6).toInt())
        binding.spinnerTime.listView.overScrollMode = View.OVER_SCROLL_NEVER
        binding.spinnerTime.listView.isScrollbarFadingEnabled = false

    }

    private fun setInfiniteCycleViewPager() {
        /** 飛揚 89.5 (紫) , 亞太 92.3 (紅)  , 亞洲 92.7 (藍)  * */
        val radioIconResource = mutableListOf(R.drawable.icon_logo_927 , R.drawable.icon_logo_895 , R.drawable.icon_logo_923)
        binding.hicvp.adapter =
            HorizontalPagerAdapter(requireActivity(), radioIconResource)
        binding.hicvp.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int , positionOffset: Float , positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {

                var remainder = position
                remainder %= radioIconResource.size

                when (remainder) {
                    //  亞洲 FM 92.7
                    0 -> {
                        binding.clBackground.background = BitmapDrawable(resources , PdfTool().pdfToBitmap(requireActivity() , R.raw.bg_blue)[0])
                        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.blue1748C7)
                        currentRadioIndex = 1
                        makeApiCall()
                    }
                    //  飛揚調頻 FM 89.5
                    1 -> {
                        binding.clBackground.background = BitmapDrawable(resources , PdfTool().pdfToBitmap(requireActivity() , R.raw.bg_purple)[0])
                        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.purple8A1BDF)
                        currentRadioIndex = 3
                        makeApiCall()
                    }
                    //  亞太 FM 92.3
                    2 -> {
                        binding.clBackground.background = BitmapDrawable(resources , PdfTool().pdfToBitmap(requireActivity() , R.raw.bg_red)[0])
                        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.redE50606)
                        currentRadioIndex = 2
                        makeApiCall()
                    }
                }
            }
        })
    }


    private fun setSpinnerData() {
        setSpinnerDate()
        setSpinnerTime()
    }

    private fun setSpinnerDate() {
        val dateLists = mutableListOf<String>()
        for (i: Int in 0..6) {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_MONTH , -i)
            val df = SimpleDateFormat("yyyy-MM-dd" , Locale.TAIWAN)
            dateLists.add(df.format(calendar.time))
        }
        binding.spinnerDate.setItems(dateLists)
        binding.spinnerDate.setOnItemSelectedListener { _ , _ , _ , _ ->
            makeApiCall()
        }

    }

    private fun setSpinnerTime() {

        binding.spinnerTime.setItems("00:00-02:00" , "02:00-04:00" , "04:00-06:00" , "06:00-08:00" , "08:00-10:00" ,
            "10:00-12:00" , "12:00-14:00" , "14:00-16:00" , "16:00-18:00" , "18:00-20:00" , "20:00-22:00" , "22:00-00:00")

        when {
            isCurrentInTimeScope(0 , 2) -> binding.spinnerTime.selectedIndex = 0
            isCurrentInTimeScope(2 , 4) -> binding.spinnerTime.selectedIndex = 1
            isCurrentInTimeScope(4 , 6) -> binding.spinnerTime.selectedIndex = 2
            isCurrentInTimeScope(6 , 8) -> binding.spinnerTime.selectedIndex = 3
            isCurrentInTimeScope(8 , 10) -> binding.spinnerTime.selectedIndex = 4
            isCurrentInTimeScope(10 , 12) -> binding.spinnerTime.selectedIndex = 5
            isCurrentInTimeScope(12 , 14) -> binding.spinnerTime.selectedIndex = 6
            isCurrentInTimeScope(14 , 16) -> binding.spinnerTime.selectedIndex = 7
            isCurrentInTimeScope(16 , 18) -> binding.spinnerTime.selectedIndex = 8
            isCurrentInTimeScope(18 , 20) -> binding.spinnerTime.selectedIndex = 9
            isCurrentInTimeScope(20 , 22) -> binding.spinnerTime.selectedIndex = 10
            isCurrentInTimeScope(22 , 24) -> binding.spinnerTime.selectedIndex = 11
        }

        binding.spinnerTime.setOnItemSelectedListener { _ , _ , _ , _ ->
            makeApiCall()
        }

    }


    /**判断当前系统时间是否在指定时间的范围内
     * @param beginHour
     * 开始小时，例如22
     * @param endHour
     * 结束小时，例如 8
     * @return true表示在范围内，否则false
     */
    private fun isCurrentInTimeScope(beginHour: Int , endHour: Int): Boolean {
        val calendarCurrent = Calendar.getInstance()    //  現在時間

        val calendarStart = Calendar.getInstance()      //  開始時間
        calendarStart.set(Calendar.HOUR_OF_DAY , beginHour)
        calendarStart.set(Calendar.MINUTE , 0)
        calendarStart.set(Calendar.SECOND , 0)

        val calendarEnd = Calendar.getInstance()        //  結束時間
        calendarEnd.set(Calendar.HOUR_OF_DAY , endHour)
        calendarEnd.set(Calendar.MINUTE , 0)
        calendarEnd.set(Calendar.SECOND , 0)

        // startTime <=  now <=  endTime
        return !calendarCurrent.time.before(calendarStart.time) && !calendarCurrent.time.after(calendarEnd.time)

    }

    private fun setupBinding(viewModel: SearchMusicListViewModel) {

        binding.setVariable(net.asiafm.BR.viewModel , viewModel)
        binding.executePendingBindings()

        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireActivity() , LinearLayoutManager.VERTICAL , false)
        }
    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): SearchMusicListViewModel {

        val viewModel = ViewModelProvider(this).get(SearchMusicListViewModel::class.java)
        viewModel.getApiData(currentRadioIndex , binding.spinnerDate.text.toString() , binding.spinnerTime.text.toString())
        viewModel.getRecyclerListDataObserver().observe(requireActivity() , {
            if (it != null) {
                // update the adapter
                viewModel.setAdapterData(it)
                binding.rvContent.scrollToPosition(0)
                // add footer view
                if (isAdded && viewModel.getAdapter().footerLayoutCount == 0) {
                    viewModel.getAdapter().addFooterView(layoutInflater.inflate(R.layout.asia_privacy_footer , null))
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.WRAP_CONTENT)
                    params.setMargins(0 , 100 , 0 , 200)
                    viewModel.getAdapter().footerLayout!!.layoutParams = params
                }
            }

        })
        return viewModel
    }


}