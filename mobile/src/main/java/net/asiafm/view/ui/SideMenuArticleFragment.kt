package net.asiafm.view.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import net.asiafm.R
import net.asiafm.asiaAPI.viewModel.SideMenuListViewModel
import net.asiafm.databinding.FragmentSideMenu1Binding
import net.asiafm.view.MainActivity

class SideMenuArticleFragment : Fragment() {

    private lateinit var binding: FragmentSideMenu1Binding
    private val args: SideMenuArticleFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_side_menu1 , container , false)

        initView()

        return binding.root
    }

    private fun initView() {
        /** 設置畫面離邊距離 */
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT , FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0 , MainActivity.statusBarHeight , 0 , 0)
        binding.llContent.layoutParams = params
        setToolBar()
        setupBinding(makeApiCall())
    }

    /** Toolbars **/
    private fun setToolBar() {
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.blue00BFFF)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity() , R.color.blue00BFFF))
        binding.toolbar.tvToolbarTitle.text = args.toolbarTitle
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    private fun setupBinding(viewModel: SideMenuListViewModel) {
        binding.setVariable(net.asiafm.BR.viewModel , viewModel)
        binding.executePendingBindings()
        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireActivity() , LinearLayoutManager.VERTICAL , false)
        }
    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): SideMenuListViewModel {
        val viewModel = ViewModelProvider(this).get(SideMenuListViewModel::class.java)
        viewModel.getRecyclerListDataObserver().observe(requireActivity() , {
            if (it != null) {
                // update the adapter
                viewModel.setAdapterData(it.info)

                // add footer view
                if (isAdded && viewModel.getAdapter().footerLayoutCount == 0) {
                    viewModel.getAdapter().addFooterView(layoutInflater.inflate(R.layout.asia_privacy_footer , null))
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.WRAP_CONTENT)
                    params.setMargins(0 , 100 , 0 , 200)
                    viewModel.getAdapter().footerLayout!!.layoutParams = params
                }

            } else {
                Toast.makeText(requireActivity() , "No fetch data" , Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.getApiData(args.toolbarTitle)

        return viewModel
    }


}