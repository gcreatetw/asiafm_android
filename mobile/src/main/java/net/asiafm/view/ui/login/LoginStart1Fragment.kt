package net.asiafm.view.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import net.asiafm.R
import net.asiafm.databinding.FragmentLoginStart1Binding
import net.asiafm.view.MainActivity


class LoginStart1Fragment : Fragment() {

    private lateinit var binding: FragmentLoginStart1Binding

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_login_start1 , container , false)

        //  註冊會員
        binding.tvLogonRegistered.setOnClickListener {
            LoginActivity.replaceFragment(requireActivity() , LoginStart2FragmentRegistered())
        }

        //  登入
        binding.tvLoginSignIn.setOnClickListener {
            LoginActivity.replaceFragment(requireActivity() , LoginStart2FragmentLogin())
        }

        //  訪客直接登入
        binding.tvLoginVisitor.setOnClickListener {
            startActivity(Intent(requireContext() , MainActivity::class.java))
            requireActivity().finish()
            MainActivity.isSignIn = false
        }

        return binding.root
    }

}