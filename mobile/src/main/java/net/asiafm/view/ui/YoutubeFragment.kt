package net.asiafm.view.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import net.asiafm.R
import net.asiafm.asiaAPI.viewModel.YoutubePlayListViewModel
import net.asiafm.databinding.FragmentYoutubeBinding
import net.asiafm.view.MainActivity
import androidx.lifecycle.ViewModelProviders





class YoutubeFragment : Fragment() {

    private lateinit var binding: FragmentYoutubeBinding
    private lateinit var navController: NavController

    private val channelId = "UCPoQPt-ptT-X-Fl4nvtKuzg"

    //    private val playlistId = "UUC-RHF_77zQdKcA75hr5oTQ"


    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_youtube , container , false)
        navController = NavHostFragment.findNavController(this)

        initView()
        setupBinding(makeApiCall())


        return binding.root
    }

    private fun initView() {
        /** 設置畫面離邊距離 */
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT , FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0 , MainActivity.statusBarHeight , 0 , 0)
        binding.llContent.layoutParams = params

        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.blue00BFFF)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity() , R.color.blue00BFFF))
        binding.toolbar.materialToolbar.title = ""
        binding.toolbar.tvToolbarTitle.text = "影音專區"

    }

    private fun setupBinding(viewModel: YoutubePlayListViewModel) {

        binding.setVariable(net.asiafm.BR.viewModel , viewModel)
        binding.executePendingBindings()

        binding.rvYoutubeList.apply {
            layoutManager = LinearLayoutManager(requireActivity() , LinearLayoutManager.VERTICAL , false)
            hasFixedSize()

            /** UnderLine for each item
            val decoration = DividerItemDecoration(requireActivity(),LinearLayoutManager.VERTICAL)
            addItemDecoration(decoration)
             * */
        }
    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): YoutubePlayListViewModel {
        var viewModel = ViewModelProvider(this).get(YoutubePlayListViewModel::class.java)
        viewModel.getRecyclerListDataObserver().observe(requireActivity() , {
            if (it != null) {
                // update the adapter
                viewModel.setAdapterData(it.items)

                // add footer view
                if (isAdded && viewModel.getAdapter().footerLayoutCount == 0) {
                    viewModel.getAdapter().addFooterView(layoutInflater.inflate(R.layout.asia_privacy_footer , null))
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.WRAP_CONTENT)
                    params.setMargins(0 , 100 , 0 , 200)
                    viewModel.getAdapter().footerLayout!!.layoutParams = params
                }

            } else {
                Toast.makeText(requireActivity() , "No fetch data" , Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.isLoading.observe(requireActivity(), { isLoading ->
            if (isLoading != null) {
                if (isLoading) {
                    // hide your progress bar
                    binding.loadingProgress.visibility = View.VISIBLE
                }else{
                    binding.loadingProgress.visibility = View.GONE
                }
            }
        })
        viewModel.getApiData()

        return viewModel
    }

}