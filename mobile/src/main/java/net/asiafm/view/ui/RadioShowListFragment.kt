package net.asiafm.view.ui

import android.annotation.SuppressLint
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import net.asiafm.R
import net.asiafm.asiaAPI.viewModel.ShowListViewModel
import net.asiafm.databinding.FragmentRadioShowListBinding
import net.asiafm.util.PdfTool
import net.asiafm.util.dayOfWeek
import net.asiafm.view.MainActivity
import net.asiafm.view.infiniteCycleView.HorizontalPagerAdapter
import net.asiafm.view.ui.login.LoginActivity
import java.util.*


class RadioShowListFragment : Fragment() {

    private lateinit var binding: FragmentRadioShowListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_radio_show_list, container, false)

        initView()
        setInfiniteCycleViewPager()
        setupBinding(makeApiCall())

        return binding.root
    }

    private fun initView() {

        binding.clBackground.layoutParams.height = (LoginActivity.windowHeight * 0.36).toInt()

        //  初始設定在亞洲電台
        val dm: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(requireActivity(), R.raw.bg_blue)[0])
        binding.clBackground.background = dm
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity(), R.color.blue1748C7)

        //  因背景延伸到status bar ，固最上層元件需補回status bar 高度
        val newLayoutParams: ConstraintLayout.LayoutParams = binding.tvShowList.layoutParams as ConstraintLayout.LayoutParams
        newLayoutParams.topMargin = MainActivity.statusBarHeight
        binding.tvShowList.layoutParams = newLayoutParams


        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            hasFixedSize()
            val decoration = DividerItemDecoration(requireActivity(), LinearLayoutManager.VERTICAL)
            addItemDecoration(decoration)
        }

    }

    private fun setInfiniteCycleViewPager() {
        /** 飛揚 89.5 (紫) , 亞太 92.3 (紅)  , 亞洲 92.7 (藍)  * */
        val radioIconResource = mutableListOf(R.drawable.icon_logo_927, R.drawable.icon_logo_895, R.drawable.icon_logo_923)

        binding.hicvp.adapter = HorizontalPagerAdapter(requireActivity(), radioIconResource)
        binding.hicvp.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {

                var remainder = position
                remainder %= radioIconResource.size

                when (remainder) {
                    //  亞洲 FM 92.7
                    0 -> {
                        val dm: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(requireActivity(), R.raw.bg_blue)[0])
                        binding.clBackground.background = dm
                        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity(), R.color.blue1748C7)
                        ShowListViewModel.currentRadioIndexSlug = "asia927"
                    }
                    //  飛揚調頻 FM 89.5
                    1 -> {
                        val dm: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(requireActivity(), R.raw.bg_purple)[0])
                        binding.clBackground.background = dm
                        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity(), R.color.purple8A1BDF)
                        ShowListViewModel.currentRadioIndexSlug = "flyradio"
                    }
                    //  亞太 FM 92.3
                    2 -> {
                        val dm: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(requireActivity(), R.raw.bg_red)[0])
                        binding.clBackground.background = dm
                        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity(), R.color.redE50606)
                        ShowListViewModel.currentRadioIndexSlug = "asia923"
                    }
                }
                makeApiCall()

            }
        })
    }

    private fun setupBinding(viewModel: ShowListViewModel) {

        binding.setVariable(net.asiafm.BR.viewModel, viewModel)
        binding.executePendingBindings()

        binding.rvWeek.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        }
        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        }

    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): ShowListViewModel {
        val viewModel = ViewModelProvider(this).get(ShowListViewModel::class.java)
        viewModel.getObjectShowListDataObserver().observe(requireActivity(), {
            if (it != null) {
                // update the adapter
                viewModel.setAdapterData(it)

                // add footer view
                if (isAdded && viewModel.getAdapter().footerLayoutCount == 0) {
                    viewModel.getAdapter().addFooterView(layoutInflater.inflate(R.layout.asia_privacy_footer, null))
                    val params: LinearLayout.LayoutParams =
                        LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    params.setMargins(0, 100, 0, 200)
                    viewModel.getAdapter().footerLayout!!.layoutParams = params
                }

            } else {
                Toast.makeText(requireActivity(), "No fetch data", Toast.LENGTH_SHORT).show()
            }
        })
        viewModel.getShowListApiData(ShowListViewModel.currentRadioIndexSlug, ShowListViewModel.currentDayOfWeekIndex)

        return viewModel
    }

    override fun onStop() {
        super.onStop()
        ShowListViewModel.currentDayOfWeekIndex = Calendar.getInstance().dayOfWeek - 1
        ShowListViewModel.currentRadioIndexSlug = "asia927"
    }

}