package net.asiafm.view.ui.dialog

import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import net.asiafm.R
import net.asiafm.databinding.DialogfragmentAdvertisingBinding
import net.asiafm.util.PdfTool
import net.asiafm.view.ui.login.LoginActivity


class AdvertisingDialogFragment : DialogFragment() {

    private lateinit var binding: DialogfragmentAdvertisingBinding

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout(LoginActivity.windowWidth  , LoginActivity.windowHeight)
            dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        }
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.dialogfragment_advertising , container , false)

        val dm: Drawable = BitmapDrawable(resources , PdfTool().pdfToBitmap(requireActivity() , R.raw.default_advertiising)[0])
        Glide.with(requireActivity()).load(dm).into(binding.imgAdvertising)


        binding.imgClose.setOnClickListener {
            dismiss()
        }


        return binding.root
    }


}