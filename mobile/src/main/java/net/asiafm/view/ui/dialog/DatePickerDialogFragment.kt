package net.asiafm.view.ui.dialog

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import net.asiafm.R
import net.asiafm.databinding.DialogDatepickerBinding
import net.asiafm.view.ui.login.LoginActivity


class DatePickerDialogFragment : DialogFragment() {

    private lateinit var binding: DialogDatepickerBinding
    private var day = 1
    private var month = 1
    private var year = 2021


    override fun onStart() {
        super.onStart()
        dialog!!.window!!.setLayout((LoginActivity.windowWidth * 0.8).toInt() , (LoginActivity.windowHeight * 0.5).toInt())
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.bg_wheel_picker)
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.dialog_datepicker , container , false)

        val datePicker = binding.datePicker

        binding.btnCheck.setOnClickListener {
            day = datePicker.dayOfMonth
            month = datePicker.month + 1
            year = datePicker.year

            val intent = Intent("updateText") //設定廣播識別碼
            intent.putExtra("birthday" , String.format("%s-%02d-%02d" , year , month , day)) //設定廣播夾帶參數
            requireActivity().sendBroadcast(intent)
            dismiss()

        }

        binding.imgCloseNP.setOnClickListener {
            dismiss()
        }

        return binding.root
    }

}