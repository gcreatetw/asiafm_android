package net.asiafm.view.ui.setting

import android.os.Bundle
import android.text.InputType
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.gson.JsonObject
import net.asiafm.Global
import net.asiafm.R
import net.asiafm.asiaAPI.RetrofitInstance
import net.asiafm.asiaAPI.dataModel.ApiObjectUpdatePassword
import net.asiafm.databinding.FragmentSetting2Binding
import net.asiafm.storagedData.StorageDataMaintain
import net.asiafm.view.MainActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/** 修改密碼*/
class ModifyUserPasswordFragment : Fragment(), TextView.OnEditorActionListener {

    private lateinit var binding: FragmentSetting2Binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting2, container, false)

        initView()
        setView()

        binding.btnCheck.setOnClickListener {

            if (binding.settingNewPassword.edInput.text.toString() == binding.settingNewPassword2.edInput.text.toString()) {
                getUpdatePasswordResult()
            } else {
                Toast.makeText(requireContext(), "兩次新密碼不相符", Toast.LENGTH_SHORT).show()
            }
        }


        return binding.root

    }

    private fun initView() {
        /** 設置畫面離邊距離 */
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0, MainActivity.statusBarHeight, 0, 0)
        binding.llContent.layoutParams = params
        setToolBar()

    }

    /** Toolbars **/
    private fun setToolBar() {
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity(), R.color.blue00BFFF)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.blue00BFFF))
        binding.toolbar.tvToolbarTitle.text = "變更密碼"
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    private fun setView() {
        //  EditText title
        binding.settingOldPassword.tvTitle.text = "請輸入舊密碼"
        binding.settingNewPassword.tvTitle.text = "請輸入新密碼"
        binding.settingNewPassword2.tvTitle.text = "請再輸入一次新密碼"

        //  EditText binding softKeyboard\listener
        binding.settingOldPassword.edInput.setOnEditorActionListener(this)
        binding.settingNewPassword.edInput.setOnEditorActionListener(this)
        binding.settingNewPassword2.edInput.setOnEditorActionListener(this)
        binding.settingOldPassword.edInput.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        binding.settingNewPassword.edInput.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        binding.settingNewPassword2.edInput.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

    }

    private fun getUpdatePasswordResult() {

        val paramObject = JsonObject()
        paramObject.addProperty("userId", SettingFragment.userDataModel.userId)
        paramObject.addProperty("oldPassword", binding.settingOldPassword.edInput.text.toString())
        paramObject.addProperty("newPassword", binding.settingNewPassword.edInput.text.toString())


        RetrofitInstance.getAsiaApiInstance()!!.updateUserPassword(paramObject).enqueue(object : Callback<ApiObjectUpdatePassword> {
            override fun onResponse(call: Call<ApiObjectUpdatePassword>, response: Response<ApiObjectUpdatePassword>) {
                if (response.isSuccessful) {
                    val objectUpdatePasswordResult = response.body()!!
                    if (objectUpdatePasswordResult.code == 200) {
                        val map = HashMap<String, String>()
                        map["account"] = SettingFragment.userDataModel.userMobile
                        map["password"] = binding.settingNewPassword.edInput.text.toString()
                        StorageDataMaintain.saveMap(requireActivity(), map)

                        Toast.makeText(requireContext(), "密碼變更成功", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "密碼變更失敗", Toast.LENGTH_SHORT).show()
                    }
                    binding.settingOldPassword.edInput.setText("")
                    binding.settingNewPassword.edInput.setText("")
                    binding.settingNewPassword2.edInput.setText("")
                    Global.hideSoftKeyBroad(requireActivity(), binding.root)
                    clearFocus()
                }
            }

            override fun onFailure(call: Call<ApiObjectUpdatePassword>, t: Throwable) {
            }

        })
    }

    private fun clearFocus() {
        binding.settingOldPassword.edInput.clearFocus()
        binding.settingNewPassword.edInput.clearFocus()
        binding.settingNewPassword2.edInput.clearFocus()
    }


    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
            Global.hideSoftKeyBroad(requireActivity(), binding.root)
            clearFocus()
        }
        return true
    }


}