package net.asiafm.view.ui.login

import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import net.asiafm.R
import net.asiafm.asiaAPI.RetrofitInstance
import net.asiafm.asiaAPI.UserDataModel
import net.asiafm.asiaAPI.dataModel.ApiObjectLoginResult
import net.asiafm.databinding.FragmentLoginStart0Binding
import net.asiafm.storagedData.StorageDataMaintain
import net.asiafm.util.PdfTool
import net.asiafm.view.MainActivity
import net.asiafm.view.ui.setting.SettingFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginStart0Fragment : Fragment() {

    private lateinit var binding: FragmentLoginStart0Binding

    val handler: Handler = Handler(Looper.getMainLooper())

    private var runnable: Runnable = object : Runnable {
        override fun run() {

            if (StorageDataMaintain.getIsLogin(requireContext())) {
                getUserInfo()
                startActivity(Intent(requireContext(), MainActivity::class.java))
                requireActivity().finish()
            } else {
                LoginActivity.replaceFragment(activity!!, LoginStart1Fragment())
            }

            handler.postDelayed(this, 4000)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login_start0, container, false)

        viewAnimation()

        handler.postDelayed(runnable, 4000)

        return binding.root
    }

    private fun viewAnimation() {

        //  Logo 往上平移
        binding.imgLoginLogo.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.login_logo_translate).apply { fillAfter = true })
        //  文字淡入
        binding.tvLoginWelcome.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.login_text_alpha).apply { fillAfter = true })
        //  音符插圖淡入
        binding.imgLoginMusicNote.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.login_text_alpha).apply { fillAfter = true })
        val dm: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(requireActivity(), R.raw.illustration_start_music)[0])
        Glide.with(requireActivity()).load(dm).into(binding.imgLoginMusicNote)
    }

    override fun onStop() {
        super.onStop()
        handler.removeCallbacks(runnable)
    }

    private fun getUserInfo() {
        val map = StorageDataMaintain.loadMap(requireContext())

        val paramObject = JsonObject()
        paramObject.addProperty("username", map["account"].toString())
        paramObject.addProperty("password", map["password"].toString())

        RetrofitInstance.getAsiaApiInstance()!!.getLoginResult(paramObject)
            .enqueue(object : Callback<ApiObjectLoginResult> {
                override fun onResponse(call: Call<ApiObjectLoginResult>, response: Response<ApiObjectLoginResult>) {
                    if (response.isSuccessful) {
                        val objectLoginResult = response.body()!!

                        when (objectLoginResult.code) {
                            200 -> {
                                MainActivity.isSignIn = true
                                val dataLists = objectLoginResult.data
                                SettingFragment.userDataModel = UserDataModel(dataLists.userId,
                                    dataLists.avatar, dataLists.name, dataLists.gender,
                                    dataLists.birthday, dataLists.address, dataLists.email, dataLists.phone)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<ApiObjectLoginResult>, t: Throwable) {

                }
            })

    }
}