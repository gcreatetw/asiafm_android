package net.asiafm.view.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.gson.JsonObject
import net.asiafm.Global
import net.asiafm.R
import net.asiafm.asiaAPI.RetrofitInstance
import net.asiafm.asiaAPI.UserDataModel
import net.asiafm.asiaAPI.dataModel.ApiObjectLoginResult
import net.asiafm.asiaAPI.dataModel.ApiObjectRegister
import net.asiafm.databinding.FragmentLoginStart2RegisteredBinding
import net.asiafm.storagedData.StorageDataMaintain
import net.asiafm.view.MainActivity
import net.asiafm.view.ui.setting.SettingFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit

/** 會員註冊*/
class LoginStart2FragmentRegistered : Fragment(), TextView.OnEditorActionListener {

    private lateinit var binding: FragmentLoginStart2RegisteredBinding
    private var isAlreadyGetOTP = false

    // [ OTP declare_auth]
    private lateinit var auth: FirebaseAuth
    private var storedVerificationId: String = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private lateinit var credential: PhoneAuthCredential

    private val otpResultDialogFragment = OtpResultDialogFragment()
    private val args = Bundle()

    companion object {
        private const val TAG = "AsiaLog"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login_start2_registered, container, false)
        //  EditText binding softKeyboard\listener
        binding.editInputMobile.setOnEditorActionListener(this)
        binding.editInputPassword.setOnEditorActionListener(this)
        binding.editInputOTP.setOnEditorActionListener(this)

        toolbarView()
        // Initialize Firebase Auth
        auth = Firebase.auth

        binding.btnRegistered.setOnClickListener {

            if (binding.editInputOTP.text!!.toString().trim().isEmpty()) {
                Toast.makeText(requireActivity(), "請輸入驗證碼", Toast.LENGTH_SHORT).show()
                args.putBoolean("feedbackResult", false)
                args.putString("description", "請輸入驗證碼")
                otpResultDialogFragment.arguments = args
                otpResultDialogFragment.show(requireActivity().supportFragmentManager, "OTP_dialog")

            } else {
                verifyPhoneNumberWithCode(storedVerificationId, binding.editInputOTP.text!!.toString().trim())
            }

            Global.hideSoftKeyBroad(requireActivity(), binding.root)
            binding.editInputMobile.clearFocus()
            binding.editInputPassword.clearFocus()
            binding.editInputOTP.clearFocus()
        }

        binding.btnGetOTP.setOnClickListener {
            getOTP()
        }


        binding.btnStart.setOnClickListener {
            MainActivity.isSignIn = true
            startActivity(Intent(requireContext(), MainActivity::class.java))
            requireActivity().finish()
        }

        return binding.root
    }


    /** Toolbar Setting */
    private fun toolbarView() {
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0, MainActivity.statusBarHeight, 0, 0)
        binding.llContent.layoutParams = params
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity(), R.color.redC70000)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.redC70000))
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar.materialToolbar)
        setHasOptionsMenu(true)
        binding.toolbar.materialToolbar.title = ""
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { (activity as FragmentActivity).onBackPressed() }
    }

    /** 參考DOC
     * https://firebase.google.com/docs/auth/android/phone-auth?authuser=1#enable-app-verification
     * */
    private fun getOTP() {
        if (binding.editInputMobile.text.toString().trim().isEmpty()) {
            args.putBoolean("feedbackResult", false)
            args.putString("description", "請輸入電話號碼")
            otpResultDialogFragment.arguments = args
            otpResultDialogFragment.show(requireActivity().supportFragmentManager, "OTP_dialog")
        } else {
            val phoneNumber = "+886" + binding.editInputMobile.text.toString()


            callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                    // This callback will be invoked in two situations:
                    // 1 - Instant verification. In some cases the phone number can be instantly
                    //     verified without needing to send or enter a verification code.
                    // 2 - Auto-retrieval. On some devices Google Play services can automatically
                    //     detect the incoming verification SMS and perform verification without
                    //     user action.
                    Log.d(TAG, "onVerificationCompleted:$credential")
//                    signInWithPhoneAuthCredential(credential)
                }

                override fun onVerificationFailed(e: FirebaseException) {
                    // This callback is invoked in an invalid request for verification is made,
                    // for instance if the the phone number format is not valid.
                    Log.e(TAG, "onVerificationFailed", e)

                    if (e is FirebaseAuthInvalidCredentialsException) {
                        // Invalid request
                    } else if (e is FirebaseTooManyRequestsException) {
                        // The SMS quota for the project has been exceeded
                        Log.e(TAG, "onVerificationFailed", e)
                    }
                    // Show a message and update the UI
                    //Toast.makeText(requireActivity() , "電話號碼格式錯誤" , Toast.LENGTH_SHORT).show()
                }

                override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
                    // The SMS verification code has been sent to the provided phone number, we
                    // now need to ask the user to enter the code and then construct a credential
                    // by combining the code with a verification ID.
                    Log.d(TAG, "onCodeSent:$verificationId")
                    // Save verification ID and resending token so we can use them later
                    storedVerificationId = verificationId
                    resendToken = token
                    isAlreadyGetOTP = true
                }
            }

            if (isAlreadyGetOTP) {
                Log.d(TAG, "resendVerificationCode")
                resendVerificationCode(phoneNumber, resendToken)
            } else {
                Log.d(TAG, "startPhoneNumberVerification")
                startPhoneNumberVerification(phoneNumber)
            }
            args.putBoolean("feedbackResult", true)
            args.putString("description", "驗證碼已發送")
            otpResultDialogFragment.arguments = args
            otpResultDialogFragment.show(requireActivity().supportFragmentManager, "OTP_dialog")
        }
    }


    // [START on_start_check_user]
    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        //        val currentUser = auth.currentUser
        //        updateUI(currentUser)
    }

    private fun startPhoneNumberVerification(phoneNumber: String) {
        val options = PhoneAuthOptions.newBuilder(auth).setPhoneNumber(phoneNumber) // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(requireActivity()) // Activity (for callback binding)
            .setCallbacks(callbacks) // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    private fun verifyPhoneNumberWithCode(verificationId: String, OTP: String) {

        if (verificationId.isNotEmpty()) {
            val credential = PhoneAuthProvider.getCredential(verificationId, OTP)
            signInWithPhoneAuthCredential(credential)
        }
        updateUI()
    }

    // [START resend_verification]
    private fun resendVerificationCode(phoneNumber: String, token: PhoneAuthProvider.ForceResendingToken?) {
        val optionsBuilder = PhoneAuthOptions.newBuilder(auth).setPhoneNumber(phoneNumber)       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS)      // Timeout and unit
            .setActivity(requireActivity())                     // Activity (for callback binding)
            .setCallbacks(callbacks)                            // OnVerificationStateChangedCallbacks
        if (token != null) {
            optionsBuilder.setForceResendingToken(token)        // callback's ForceResendingToken
        }
        PhoneAuthProvider.verifyPhoneNumber(optionsBuilder.build())
    } // [END resend_verification]


    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential).addOnCompleteListener(requireActivity()) { task ->
            if (task.isSuccessful) {
                // Sign in success, update UI with the signed-in user's information
                val user = task.result?.user

                //  註冊成功，這邊預計家個上傳帳密道DB的功能
                sendAccountToDB()

            } else {
                // Sign in failed, display a message and update the UI
                args.putBoolean("feedbackResult", false)
                args.putString("description", "驗證碼錯誤或逾時")
                otpResultDialogFragment.arguments = args
                otpResultDialogFragment.show(requireActivity().supportFragmentManager, "OTP_dialog")

                if (task.exception is FirebaseAuthInvalidCredentialsException) {
                    // The verification code entered was invalid
                    Log.d(TAG, " The verification code entered was invalid = " + task.exception)
                }

            }
        }
    }

    private fun updateUI(user: FirebaseUser? = auth.currentUser) {
        val description = "恭喜您成功加入ASIA會員\n現在開始享受音樂！"
        binding.textRegistered.text = description
        binding.clRegisteredBefore.visibility = View.GONE
        binding.clRegisteredAfter.visibility = View.VISIBLE
        //  音符插圖淡入
        binding.imgLoginRadio.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.login_scale).apply { fillAfter = true })

    }


    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
            Global.hideSoftKeyBroad(requireActivity(), binding.root)
            binding.editInputMobile.clearFocus()
            binding.editInputPassword.clearFocus()
            binding.editInputOTP.clearFocus()
        }
        return true
    }

    private fun sendAccountToDB() {
        val paramObject = JsonObject()
        paramObject.addProperty("username", binding.editInputMobile.text.toString())
        paramObject.addProperty("password", binding.editInputPassword.text.toString())

        RetrofitInstance.getAsiaApiInstance()!!.getRegisterResult(paramObject).enqueue(object : Callback<ApiObjectRegister> {
            override fun onResponse(call: Call<ApiObjectRegister>, response: Response<ApiObjectRegister>) {
                if (response.isSuccessful) {
                    val objectRegister = response.body()!!

                    when (objectRegister.code) {

                        200 -> {
                            RetrofitInstance.getAsiaApiInstance()!!.getLoginResult(paramObject).enqueue(object : Callback<ApiObjectLoginResult> {
                                override fun onResponse(call: Call<ApiObjectLoginResult>, response: Response<ApiObjectLoginResult>) {
                                    if (response.isSuccessful) {
                                        val objectLoginResult = response.body()!!
                                        val dataLists = objectLoginResult.data
                                        SettingFragment.userDataModel = UserDataModel(dataLists.userId,
                                            dataLists.avatar,
                                            dataLists.name,
                                            dataLists.gender,
                                            dataLists.birthday,
                                            dataLists.address,
                                            dataLists.email,
                                            dataLists.phone)
                                        val map = HashMap<String, String>()
                                        map["account"] = binding.editInputMobile.text.toString()
                                        map["password"] = binding.editInputPassword.text.toString()
                                        StorageDataMaintain.saveMap(requireContext(), map)
                                        StorageDataMaintain.setIsLogin(requireContext(), true)

                                        //  跳轉畫面動畫
                                        updateUI()
                                    }

                                }

                                override fun onFailure(call: Call<ApiObjectLoginResult>, t: Throwable) {
                                }
                            })
                        }

                        400 -> {
                            val args = Bundle()
                            args.putBoolean("feedbackResult", false)
                            args.putString("description", objectRegister.errors)
                            val otpResultDialogFragment = OtpResultDialogFragment()
                            otpResultDialogFragment.arguments = args
                            otpResultDialogFragment.show(requireActivity().supportFragmentManager, "OTP_dialog")
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectRegister>, t: Throwable) {
            }

        })

    }

}