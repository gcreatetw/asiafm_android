package net.asiafm.view.ui.setting

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.atpc.util.permission.PermissionCheckUtil
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import net.asiafm.Global
import net.asiafm.R
import net.asiafm.asiaAPI.RetrofitInstance
import net.asiafm.asiaAPI.UserDataModel
import net.asiafm.asiaAPI.dataModel.ApiObjectUpdateUserInfo
import net.asiafm.databinding.FragmentSetting1Binding
import net.asiafm.util.ImageUtil
import net.asiafm.view.MainActivity
import net.asiafm.view.ui.dialog.DatePickerDialogFragment
import net.asiafm.view.ui.login.OtpResultDialogFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.InputStream


/** 修改會員資料*/
class ModifyUserInfoFragment : Fragment(), TextView.OnEditorActionListener {

    private lateinit var binding: FragmentSetting1Binding
    private lateinit var broadcastReceiver: UpdateUIText
    private var avatarBase64String: String = SettingFragment.userDataModel.userAvatarBase64String

    private val SELECT_AVATAR_IMAGE = 100


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting1, container, false)

        initView()
        setView()
        registeredBroadCast()


        binding.tvCheck.setOnClickListener {
            if (!isColumnBlack()) {
                val paramObject = JsonObject()
                paramObject.addProperty("userId", SettingFragment.userDataModel.userId)
                paramObject.addProperty("gender", binding.settingGender.tvInput.text.toString())
                paramObject.addProperty("birthday", binding.settingBirthday.tvInput.text.toString())
                paramObject.addProperty("address", binding.settingAddress.edInput.text.toString())
                paramObject.addProperty("avatar", avatarBase64String)
                paramObject.addProperty("name", binding.settingName.edInput.text.toString())
                paramObject.addProperty("email", binding.settingEmail.edInput.text.toString())

                Toast.makeText(requireActivity(), "資料上傳中", Toast.LENGTH_SHORT).show()

                RetrofitInstance.getAsiaApiInstance()!!.updateUserInfo(paramObject).enqueue(object : Callback<ApiObjectUpdateUserInfo> {
                    override fun onResponse(call: Call<ApiObjectUpdateUserInfo>, response: Response<ApiObjectUpdateUserInfo>) {
                        if (response.isSuccessful) {
                            val objectUpdateUserInfo = response.body()!!
                            val dataLists = objectUpdateUserInfo.data
                            SettingFragment.userDataModel = UserDataModel(dataLists.userId,
                                dataLists.avatar,
                                dataLists.name,
                                dataLists.gender,
                                dataLists.birthday,
                                dataLists.address,
                                dataLists.email,
                                dataLists.phone)

                            val args = Bundle()
                            args.putBoolean("feedbackResult", true)
                            args.putString("description", "修改成功")
                            val otpResultDialogFragment = OtpResultDialogFragment()
                            otpResultDialogFragment.arguments = args
                            otpResultDialogFragment.show(requireActivity().supportFragmentManager, "OTP_dialog")
                        }
                    }

                    override fun onFailure(call: Call<ApiObjectUpdateUserInfo>, t: Throwable) {
                    }
                })
            } else {
                Toast.makeText(requireActivity(), "名稱與信箱欄位不能空白。", Toast.LENGTH_SHORT).show()
            }

        }

        return binding.root
    }


    private fun initView() {
        /** 設置畫面離邊距離 */
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0, MainActivity.statusBarHeight, 0, 0)
        binding.llContent.layoutParams = params
        setToolBar()
    }

    /** Toolbars **/
    private fun setToolBar() {
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity(), R.color.blue00BFFF)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.blue00BFFF))
        binding.toolbar.tvToolbarTitle.text = "修改會員資料"
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    private fun setView() {
        //  EditText binding softKeyboard\listener
        binding.settingName.edInput.setOnEditorActionListener(this)
//        binding.settingPhone.edInput.setOnEditorActionListener(this)
        binding.settingEmail.edInput.setOnEditorActionListener(this)
        binding.settingAddress.edInput.setOnEditorActionListener(this)

        //  Avatar
        Glide.with(requireActivity()).load(ImageUtil().base64ConvertBitmap(avatarBase64String)).error(R.drawable.icon_setting_userphoto)
            .into(binding.imgUser)

        binding.llContent1.setOnClickListener {
            if (checkPermission()) {
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                startActivityForResult(intent, SELECT_AVATAR_IMAGE)
            } else {
                val permissionsArray = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permissionsArray, PermissionCheckUtil.ONCE_TIME_APPLY)
            }
        }

        //  EditText title
        binding.settingPhone.tvTitle.text = "手機"
        binding.settingPhone.edInput.isEnabled = false
        binding.settingPhone.tvTitle.setHintTextColor(ContextCompat.getColor(requireActivity(), R.color.gray808080))

        binding.settingGender.tvTitle.text = "性別"
        binding.settingAddress.tvTitle.text = "地址"
        binding.settingEmail.tvTitle.text = "信箱"

        val dataObject = SettingFragment.userDataModel
        binding.settingName.edInput.hint = "*必填"
        binding.settingName.edInput.setHintTextColor(ContextCompat.getColor(requireActivity(), R.color.redC70000))
        binding.settingName.edInput.setText(dataObject.userName)

        binding.settingPhone.edInput.setText(dataObject.userMobile)

        binding.settingEmail.edInput.hint = "*必填"
        binding.settingEmail.edInput.setHintTextColor(ContextCompat.getColor(requireActivity(), R.color.redC70000))
        binding.settingEmail.edInput.setText(dataObject.userEmail)

        binding.settingAddress.edInput.setText(dataObject.userAddress)

        //  性別點擊跳出 datePicker
        binding.settingGender.tvInput.text = dataObject.userGender
        binding.settingGender.llContent.setOnClickListener {
            val genderWheelPicker = GenderWheelPicker(binding.settingGender.tvInput.text.toString())
            genderWheelPicker.show(requireActivity().supportFragmentManager, "update_dialog")
        }

        //  生日點擊跳出 datePicker
        binding.settingBirthday.tvInput.text = dataObject.userBirthday
        binding.settingBirthday.llContent.setOnClickListener {
            val datePickerDialogFragment = DatePickerDialogFragment()
            datePickerDialogFragment.show(requireActivity().supportFragmentManager, "update_dialog")
        }

    }

    // 欄位空職確認
    private fun isColumnBlack(): Boolean {
        return binding.settingName.edInput.text.toString().isEmpty() || binding.settingEmail.edInput.text.toString().isEmpty()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            assert(data != null)
            when (requestCode) {
                SELECT_AVATAR_IMAGE -> {
                    val selectedImageUri: Uri = data!!.data!!
                    val imageStream: InputStream = requireActivity().contentResolver.openInputStream(selectedImageUri)!!
                    val selectedImage: Bitmap = BitmapFactory.decodeStream(imageStream)

                    startConvert(selectedImage)


                    Glide.with(this).load(selectedImage).error(R.drawable.icon_setting_userphoto).centerInside().into(binding.imgUser)
                }
            }
        }
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        Global.hideSoftKeyBroad(requireActivity(), v!!)
        binding.settingName.edInput.clearFocus()
        binding.settingPhone.edInput.clearFocus()
        binding.settingEmail.edInput.clearFocus()
        binding.settingAddress.edInput.clearFocus()
        return true
    }

    private fun checkPermission(): Boolean {
        val hasPermission: Int = checkSelfPermission(requireActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
        return hasPermission == PackageManager.PERMISSION_GRANTED
    }

    /** 更新畫面相關  **/
    private fun registeredBroadCast() {
        broadcastReceiver = UpdateUIText(binding)
        val intentFilter = IntentFilter("updateText")
        requireActivity().registerReceiver(broadcastReceiver, intentFilter)
    }

    private fun startConvert(bitmap: Bitmap) {

        Thread {
            avatarBase64String = ImageUtil().bitmapConvertBase64(bitmap)

        }.start()

    }

    class UpdateUIText(private val binding: FragmentSetting1Binding) : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {

            val extras = intent.extras
            if (extras != null) {
                if (!extras.getString("birthday").isNullOrEmpty()) {
                    binding.settingBirthday.tvInput.text = extras.getString("birthday").toString()
                }

                if (!extras.getString("gender").isNullOrEmpty()) {
                    binding.settingGender.tvInput.text = extras.getString("gender").toString()
                }

            }
        }
    }

}

