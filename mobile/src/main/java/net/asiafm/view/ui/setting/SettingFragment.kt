package net.asiafm.view.ui.setting

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.atpc.util.permission.PermissionCheckUtil
import com.bumptech.glide.Glide
import net.asiafm.R
import net.asiafm.asiaAPI.UserDataModel
import net.asiafm.databinding.FragmentSettingBinding
import net.asiafm.shared.MusicService.Companion.isStartMediaPlayer
import net.asiafm.storagedData.StorageDataMaintain
import net.asiafm.util.ImageUtil
import net.asiafm.view.MainActivity
import net.asiafm.view.ui.OnlineFragment
import net.asiafm.view.ui.login.LoginActivity


class SettingFragment : Fragment() {

    private lateinit var binding: FragmentSettingBinding
    private lateinit var navController: NavController

    companion object {
        lateinit var userDataModel: UserDataModel
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_setting , container , false)
        navController = NavHostFragment.findNavController(this)

        initView()
        viewAction()

        return binding.root
    }

    private fun initView() {
        /** 設置畫面離邊距離 */
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT , FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0 , MainActivity.statusBarHeight , 0 , 0)
        binding.llContent.layoutParams = params

        setToolBar()

        Glide.with(requireActivity()).load(ImageUtil().base64ConvertBitmap(userDataModel.userAvatarBase64String))
            .error(R.drawable.icon_setting_userphoto).into(binding.imgUser)
        binding.tvUserName.text = userDataModel.userName
        binding.tvUserMobile.text = String.format("手機：%s-***-***" , userDataModel.userMobile.substring(0 , 4))
    }

    /** Toolbars **/
    private fun setToolBar() {
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.blue00BFFF)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity() , R.color.blue00BFFF))
        binding.toolbar.tvToolbarTitle.text = "設定"
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    private fun viewAction() {

        /** 修改會員資料*/
        binding.tvSettingModifyData.setOnClickListener { navController.navigate(R.id.action_settingFragment_to_modifyUserInfoFragment) }
        /** 修改密碼*/
        binding.tvSettingChangePassword.setOnClickListener { navController.navigate(R.id.action_settingFragment_to_modifyUserPasswordFragment) }
        /** 帳號登出*/
        binding.tvSettingLogout.setOnClickListener {
            isStartMediaPlayer = false
            OnlineFragment.logOutReset(0)
            StorageDataMaintain.setIsLogin(requireContext() , false)
            startActivity(Intent(requireContext() , LoginActivity::class.java))
            requireActivity().finish()
        }

        /** 開啟APP自動撥放Radio */
        binding.switchSetting.isChecked = StorageDataMaintain.getIsStartRadio(requireContext())
        binding.switchSetting.setOnCheckedChangeListener { _ , isChecked ->
            if (isChecked) {
                StorageDataMaintain.setIsStartRadio(requireContext() , true)
            } else {
                StorageDataMaintain.setIsStartRadio(requireContext() , false)
            }

        }

        /** 設定鬧鐘 */
        binding.tvSettingClock.setOnClickListener {
            navController.navigate(R.id.action_settingFragment_to_alarmClockFragment)
        }

        /** 版面主題 */
        binding.baseline4.visibility = View.GONE
        binding.tvSettingPageColor.visibility = View.GONE

        /** 關於亞洲廣播家族*/
        binding.tvSettingAboutAsiaFamily.setOnClickListener { navController.navigate(R.id.action_settingFragment_to_aboutAsiaRadioFragment) }

        binding.tvSettingContactUs.setOnClickListener {

            val permissionsArray = arrayOf(Manifest.permission.CALL_PHONE)
            requestPermissions(permissionsArray , PermissionCheckUtil.ONCE_TIME_APPLY)

        }

        /** 官方網站*/
        binding.tvSettingOfficialWebsite.setOnClickListener { navController.navigate(R.id.action_settingFragment_to_intentAsiaWebFragment) }
        /** 官方臉書粉絲團*/
        binding.tvSettingOfficialFacebook.setOnClickListener { navController.navigate(R.id.action_settingFragment_to_intentFacebookFragment) }
        /** 官方Instagram*/
        binding.tvSettingOfficialInstagram.setOnClickListener { navController.navigate(R.id.action_settingFragment_to_intentInstagramFragment) }

    }

    /**
     * 请求权限结果回调
     * @param requestCode
     * @param permissions
     * @param grantResults
     */

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<out String> , grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode , permissions , grantResults)
        val callIntent = Intent(Intent.ACTION_CALL , Uri.parse("tel:032209207"))
        startActivity(callIntent)
    }


}