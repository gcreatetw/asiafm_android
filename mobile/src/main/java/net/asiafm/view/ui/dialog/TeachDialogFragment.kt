package net.asiafm.view.ui.dialog

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import net.asiafm.R
import net.asiafm.databinding.DialogTeachBinding
import net.asiafm.util.PdfTool


class TeachDialogFragment : DialogFragment() {

    private var isTouch = false

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.MATCH_PARENT)
            dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        }
    }


//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setStyle(STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val binding: DialogTeachBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_teach, container, false)

        val dm = BitmapDrawable(requireActivity().resources,
            PdfTool().pdfToBitmap(requireActivity(), R.raw.home_teaching_page01)[0])

        val dm2 = BitmapDrawable(requireActivity().resources,
            PdfTool().pdfToBitmap(requireActivity(), R.raw.home_teaching_page02)[0])

        Glide.with(requireActivity()).load(dm).into(binding.imgTeach)


        binding.imgTeach.setOnClickListener {
            if (!isTouch ){
                Glide.with(requireActivity()).load(dm2).into(binding.imgTeach)
                isTouch = true
                binding.btnCheck.visibility = View.VISIBLE
            }
        }

        binding.btnCheck.setOnClickListener {
            dismiss()
        }

        return binding.root
    }



}