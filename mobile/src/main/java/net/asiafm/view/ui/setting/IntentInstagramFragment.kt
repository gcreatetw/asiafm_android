package net.asiafm.view.ui.setting

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import net.asiafm.Global
import net.asiafm.R
import net.asiafm.databinding.FragmentSetting6Binding
import net.asiafm.view.MainActivity
import net.asiafm.view.inAppBrowser.WebViewActivity


/** 官方Instagram*/
class IntentInstagramFragment : Fragment() {

    private lateinit var binding: FragmentSetting6Binding

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_setting6 , container , false)

        initView()
        viewAction()

        return binding.root
    }

    private fun initView() {
        /** 設置畫面離邊距離 */
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT , FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0 , MainActivity.statusBarHeight , 0 , 0)
        binding.llContent.layoutParams = params
        setToolBar()
    }

    /** Toolbars **/
    private fun setToolBar() {
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.blue00BFFF)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity() , R.color.blue00BFFF))
        binding.toolbar.tvToolbarTitle.text = "官方Instagram"
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    private fun viewAction() {
        binding.tvSettingFm927.setOnClickListener {
            if (Global.isAppInstalled(requireActivity() , "com.instagram.android")) {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.data = Uri.parse("instagram://user?username=asiafm_927")
                startActivity(intent)
            } else {
                val intent = Intent(context , WebViewActivity::class.java)
                intent.putExtra("extraUrl" , "https://www.instagram.com/asiafm_927")
                startActivity(intent)
            }
        }

        binding.tvSettingFm923.setOnClickListener {
            if (Global.isAppInstalled(requireActivity() , "com.instagram.android")) {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.data = Uri.parse("instagram://user?username=asiafm_923")
                startActivity(intent)
            } else {
                val intent = Intent(context , WebViewActivity::class.java)
                intent.putExtra("extraUrl" , "https://www.instagram.com/asiafm_923")
                startActivity(intent)
            }
        }
    }

}