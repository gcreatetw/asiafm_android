package net.asiafm.view.ui.setting

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import net.asiafm.R
import net.asiafm.databinding.FragmentSetting4Binding
import net.asiafm.view.MainActivity
import net.asiafm.view.inAppBrowser.WebViewActivity

/** 官方網站*/
class IntentAsiaWebFragment : Fragment() {

    private lateinit var binding: FragmentSetting4Binding

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_setting4 , container , false)

        initView()
        viewAction()

        return binding.root
    }

    private fun initView() {
        /** 設置畫面離邊距離 */
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT , FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0 , MainActivity.statusBarHeight , 0 , 0)
        binding.llContent.layoutParams = params
        setToolBar()
    }

    /** Toolbars **/
    private fun setToolBar() {
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.blue00BFFF)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity() , R.color.blue00BFFF))
        binding.toolbar.tvToolbarTitle.text = "官方網站"
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    private fun viewAction() {
        binding.tvSettingFm927.setOnClickListener {
            val intent = Intent(context , WebViewActivity::class.java)
            intent.putExtra("extraUrl" , "https://www.asiafm.com.tw/")
            startActivity(intent)
        }

        binding.tvSettingFm923.setOnClickListener {
            val intent = Intent(context , WebViewActivity::class.java)
            intent.putExtra("extraUrl" , "https://www.asiafm.com.tw/")
            startActivity(intent)
        }

        binding.tvSettingFm895.setOnClickListener {
            val intent = Intent(context , WebViewActivity::class.java)
            intent.putExtra("extraUrl" , "https://www.flyradio.com.tw/")
            startActivity(intent)
        }
    }
}