package net.asiafm.view.ui.login

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import net.asiafm.R
import net.asiafm.databinding.DialogfragmentOtpResultBinding

class OtpResultDialogFragment : DialogFragment() {

    private lateinit var binding: DialogfragmentOtpResultBinding


    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout((LoginActivity.windowWidth * 0.75).toInt() , (LoginActivity.windowWidth * 0.75).toInt())
            dialog!!.window!!.setBackgroundDrawableResource(R.drawable.bg_white_ffffff)
        }
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.dialogfragment_otp_result , container , false)

        if (requireArguments().getBoolean("feedbackResult")) {
            Glide.with(requireActivity()).load(R.drawable.icon_setting_ok).into(binding.imgPopup)
        } else {
            Glide.with(requireActivity()).load(R.drawable.icon_setting_warring).into(binding.imgPopup)
        }

        binding.textPopup.text = Html.fromHtml(requireArguments().getString("description"), Html.FROM_HTML_MODE_COMPACT)

        binding.imgClose.setOnClickListener {
            dismiss()
        }

        return binding.root
    }


}