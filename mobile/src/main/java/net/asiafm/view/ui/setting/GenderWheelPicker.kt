package net.asiafm.view.ui.setting


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.aigestudio.wheelpicker.WheelPicker
import net.asiafm.R
import net.asiafm.databinding.GenderWheelPickerBinding
import net.asiafm.view.ui.login.LoginActivity


class GenderWheelPicker(private val userGender: String) : DialogFragment() {

    private lateinit var binding: GenderWheelPickerBinding

    companion object {
        var genderIndex = 0
    }

    override fun onStart() {
        super.onStart()
        dialog!!.window!!.setLayout((LoginActivity.windowWidth * 0.8).toInt() , (LoginActivity.windowHeight * 0.5).toInt())
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.bg_wheel_picker)
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.gender_wheel_picker , container , false)

        binding.imgCloseNP.setOnClickListener { dialog!!.dismiss() }

        val np = binding.numberPicker
        np.setSelectedItemPosition(genderIndex , false)

        val dataLists = mutableListOf("男" , "女")
        np.data = dataLists

        for (i: Int in 0 until dataLists.size) {
            if (userGender == dataLists[i]) {
                np.setSelectedItemPosition(i,false)
            }
        }

        np.setOnWheelChangeListener(object : WheelPicker.OnWheelChangeListener {
            override fun onWheelScrolled(offset: Int) {
                if (offset == 0) {
                    binding.btnCheck.setOnClickListener { dialog!!.dismiss() }
                }
            }

            override fun onWheelSelected(position: Int) {
                binding.btnCheck.setOnClickListener {
                    genderIndex = position
                    val intent = Intent("updateText") //設定廣播識別碼
                    intent.putExtra("gender" , dataLists[position])  //設定廣播夾帶參數
                    requireActivity().sendBroadcast(intent)

                    dialog!!.dismiss()
                }
            }

            override fun onWheelScrollStateChanged(state: Int) {}
        })

        return binding.root
    }
}