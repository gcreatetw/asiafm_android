package net.asiafm.view.ui.setting

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import net.asiafm.R
import net.asiafm.databinding.FragmentAlarmClockBinding
import net.asiafm.listener.alarm.PlayReceiver
import net.asiafm.storagedData.StorageDataMaintain
import net.asiafm.view.MainActivity
import java.util.*


class AlarmClockFragment : Fragment() {

    private val TAG = "AlarmClockFragment_TAG"
    private lateinit var binding: FragmentAlarmClockBinding


    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_alarm_clock , container , false)

        initView()


        viewAction()

        return binding.root
    }


    private fun initView() {
        /** 設置畫面離邊距離 */
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT , FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0 , MainActivity.statusBarHeight , 0 , 0)
        binding.llContent.layoutParams = params
        setToolBar()

        //  顯示鬧鐘時間
        if (StorageDataMaintain.getAlarmStartTime(requireContext()).isEmpty()) {
            val map = HashMap<String , String>()
            map["Hour"] = "08"
            map["Minutes"] = "00"
            StorageDataMaintain.setAlarmStartTime(requireContext() , map)
        }

        if (StorageDataMaintain.getAlarmStopTime(requireContext()).isEmpty()) {
            val map = HashMap<String , String>()
            map["Hour"] = "08"
            map["Minutes"] = "00"
            StorageDataMaintain.setAlarmStopTime(requireContext() , map)
        }

        binding.tvAlarmStartTime.text = String.format("%s:%s" , StorageDataMaintain.getAlarmStartTime(requireContext())["Hour"] , StorageDataMaintain.getAlarmStartTime(requireContext())["Minutes"])
        binding.tvAlarmStopTime.text = String.format("%s:%s" , StorageDataMaintain.getAlarmStopTime(requireContext())["Hour"] , StorageDataMaintain.getAlarmStopTime(requireContext())["Minutes"])

        /** 設定鬧鐘開啟或關閉*/
        binding.switchAlarmStartTime.isChecked = StorageDataMaintain.getIsOpenAlarmStartClock(requireContext())
        binding.switchAlarmStopTime.isChecked = StorageDataMaintain.getIsOpenAlarmStopClock(requireContext())

    }

    /** Toolbars **/
    private fun setToolBar() {
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity() , R.color.blue00BFFF)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity() , R.color.blue00BFFF))
        binding.toolbar.tvToolbarTitle.text = "設定鬧鐘"
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    private fun viewAction() {

        // 開關是否自動開啟音樂
        binding.switchAlarmStartTime.setOnCheckedChangeListener { _ , isChecked ->

            if (isChecked) {
                // 設定鬧鐘
                Log.d(TAG , "鬧鐘功能打開")
                StorageDataMaintain.setIsOpenAlarmStartClock(requireContext() , true)
                openAlarmStartTime()

            } else {
                Log.d(TAG , "鬧鐘功能關閉")
                StorageDataMaintain.setIsOpenAlarmStartClock(requireContext() , false)
                closeAlarmStartTime()
            }

        }

        // 開關是否自動關閉音樂
        binding.switchAlarmStopTime.setOnCheckedChangeListener { _ , isChecked ->

            if (isChecked) {
                // 設定鬧鐘
                Log.d(TAG , "鬧鐘功能打開")
                StorageDataMaintain.setIsOpenAlarmStopClock(requireContext() , true)
                openAlarmStopTime()

            } else {
                Log.d(TAG, "鬧鐘功能關閉")
                StorageDataMaintain.setIsOpenAlarmStopClock(requireContext() , false)
                closeAlarmStopTime()
            }

        }

        binding.tvEditAlarmStartTime.setOnClickListener {

            val hour = StorageDataMaintain.getAlarmStartTime(requireContext())["Hour"]!!.toInt()
            val minutes = StorageDataMaintain.getAlarmStartTime(requireContext())["Minutes"]!!.toInt()
            // time picker dialog
            val picker = TimePickerDialog(requireActivity() , { _ , sHour , sMinute ->
                val map = HashMap<String , String>()
                map["Hour"] = String.format("%02d" , sHour)
                map["Minutes"] = String.format("%02d" , sMinute)
                StorageDataMaintain.setAlarmStartTime(requireContext() , map)
                binding.tvAlarmStartTime.text = String.format("%2s:%2s" , String.format("%02d" , sHour) , String.format("%02d" , sMinute))

                if (StorageDataMaintain.getIsOpenAlarmStartClock(requireContext())) {
                    openAlarmStartTime()
                }

            } , hour , minutes , false)
            picker.show()
        }

        binding.tvEditAlarmStopTime.setOnClickListener {

            val hour = StorageDataMaintain.getAlarmStopTime(requireContext())["Hour"]!!.toInt()
            val minutes = StorageDataMaintain.getAlarmStopTime(requireContext())["Minutes"]!!.toInt()
            // time picker dialog
            val picker = TimePickerDialog(requireActivity() , { _ , sHour , sMinute ->
                val map = HashMap<String , String>()
                map["Hour"] = String.format("%02d" , sHour)
                map["Minutes"] = String.format("%02d" , sMinute)
                StorageDataMaintain.setAlarmStopTime(requireContext() , map)
                binding.tvAlarmStopTime.text = String.format("%2s:%2s" , String.format("%02d" , sHour) , String.format("%02d" , sMinute))

                if (StorageDataMaintain.getIsOpenAlarmStopClock(requireContext())) {
                    openAlarmStopTime()
                }

            } , hour , minutes , false)
            picker.show()
        }

    }

    private var alarmStartManager: AlarmManager? = null
    private lateinit var alarmStartIntent: PendingIntent

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun openAlarmStartTime() {
        Log.d(TAG , "Setting start alarmMassage")
        val hour = StorageDataMaintain.getAlarmStartTime(requireContext())["Hour"]!!.toInt()
        val minutes = StorageDataMaintain.getAlarmStartTime(requireContext())["Minutes"]!!.toInt()

        // Set the alarm to start
        val calendar: Calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()

            if (!isOverAlarmTime(hour , minutes)) {
                add(Calendar.DATE , 1)
            }
            set(Calendar.HOUR_OF_DAY , hour)
            set(Calendar.MINUTE , minutes)
            set(Calendar.SECOND , 0)
        }

        alarmStartManager = requireActivity().getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager

        alarmStartIntent = Intent(requireActivity() , PlayReceiver::class.java).putExtra("msg" , "start alarm from fragment")
            .let { intent ->
                PendingIntent.getBroadcast(requireActivity() , 1 , intent , PendingIntent.FLAG_UPDATE_CURRENT)
            }
        // With setInexactRepeating(), you have to use one of the AlarmManager interval
//        alarmMgr?.setInexactRepeating(AlarmManager.RTC_WAKEUP , calendar.timeInMillis , AlarmManager.INTERVAL_DAY , alarmIntent)

        // 精確鬧鐘
        alarmStartManager!![AlarmManager.RTC_WAKEUP , calendar.timeInMillis] = alarmStartIntent

    }

    private fun closeAlarmStartTime() {
        alarmStartManager?.cancel(alarmStartIntent)
    }

    private var alarmStopManager: AlarmManager? = null
    private lateinit var alarmStopIntent: PendingIntent

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun openAlarmStopTime() {
        Log.d(TAG, "Setting stop alarmMassage")
        val hour = StorageDataMaintain.getAlarmStopTime(requireContext())["Hour"]!!.toInt()
        val minutes = StorageDataMaintain.getAlarmStopTime(requireContext())["Minutes"]!!.toInt()

        // Set the alarm to start
        val calendar: Calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()

            if (!isOverAlarmTime(hour , minutes)) {
                add(Calendar.DATE , 1)
            }
            set(Calendar.HOUR_OF_DAY , hour)
            set(Calendar.MINUTE , minutes)
            set(Calendar.SECOND , 0)
        }

        alarmStopManager = requireActivity().getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager

        alarmStopIntent = Intent(requireActivity() , PlayReceiver::class.java).putExtra("msg" , "stop alarm from fragment")
            .let { intent ->
                PendingIntent.getBroadcast(requireActivity() , 2 , intent , PendingIntent.FLAG_UPDATE_CURRENT)
            }
        // With setInexactRepeating(), you have to use one of the AlarmManager interval
        alarmStopManager?.setInexactRepeating(AlarmManager.RTC_WAKEUP , calendar.timeInMillis , AlarmManager.INTERVAL_DAY , alarmStopIntent)

        // 精確鬧鐘
//        alarmStopManager!![AlarmManager.RTC_WAKEUP , calendar.timeInMillis] = alarmStopIntent

    }

    private fun closeAlarmStopTime() {
        alarmStopManager?.cancel(alarmStopIntent)
    }

    private fun isOverAlarmTime(hour: Int , minutes: Int): Boolean {

        // 鬧鐘時間
        val settingTime = Calendar.getInstance()
        settingTime.set(Calendar.HOUR_OF_DAY , hour)
        settingTime.set(Calendar.MINUTE , minutes)

        //  現在時間
        val currentTime: Calendar = Calendar.getInstance()

        return settingTime > currentTime
    }

}