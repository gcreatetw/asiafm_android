package net.asiafm.view.ui.login

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.gson.JsonObject
import net.asiafm.Global
import net.asiafm.R.*
import net.asiafm.asiaAPI.RetrofitInstance
import net.asiafm.asiaAPI.UserDataModel
import net.asiafm.asiaAPI.dataModel.ApiObjectLoginResult
import net.asiafm.databinding.FragmentLoginStart2LoginBinding
import net.asiafm.storagedData.StorageDataMaintain
import net.asiafm.view.MainActivity
import net.asiafm.view.inAppBrowser.LoadingDialog
import net.asiafm.view.ui.setting.SettingFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/** 會員登入*/
class LoginStart2FragmentLogin : Fragment(), TextView.OnEditorActionListener {

    private lateinit var binding: FragmentLoginStart2LoginBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, layout.fragment_login_start2_login, container, false)
        //  EditText binding softKeyboard\listener
        binding.editInputMobile.setOnEditorActionListener(this)
        binding.editInputPassword.setOnEditorActionListener(this)

        toolbarView()

        binding.tvForgetPassword.setOnClickListener {
            val email = "services@asiafm.com.tw"
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
            intent.putExtra(Intent.EXTRA_SUBJECT, "亞洲廣播家族APP 忘記密碼")
            intent.putExtra(Intent.EXTRA_TEXT, "請填寫您所註冊的手機號碼及以下資訊，我們將會協助您取得您的密碼。\n" +
                    "\n" +
                    "真實姓名(必填)：\n" +
                    "\n" +
                    "手機號碼(必填)：\n" +
                    "\n" +
                    "電子信箱(必填)：信箱若與您於APP上填寫之個人資料一致，將加速您取得密碼。\n" +
                    "\n" +
                    "用戶名稱：")
            startActivity(intent)
        }

        binding.editInputMobile.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                binding.tvLoginMobile.setTextColor(ContextCompat.getColor(requireActivity(), color.redC70000))
            } else {
                binding.tvLoginMobile.setTextColor(ContextCompat.getColor(requireActivity(), color.black000000))
            }
        }

        binding.editInputPassword.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                binding.tvLoginPassword.setTextColor(ContextCompat.getColor(requireActivity(), color.redC70000))
            } else {
                binding.tvLoginPassword.setTextColor(ContextCompat.getColor(requireActivity(), color.black000000))
            }
        }



        binding.btnLogin.setOnClickListener {
            val loadingDialog = LoadingDialog()
            loadingDialog.show(requireActivity().supportFragmentManager, "loading dialog")

            val paramObject = JsonObject()
            paramObject.addProperty("username", binding.editInputMobile.text.toString())
            paramObject.addProperty("password", binding.editInputPassword.text.toString())

            RetrofitInstance.getAsiaApiInstance()!!.getLoginResult(paramObject).enqueue(object : Callback<ApiObjectLoginResult> {
                override fun onResponse(call: Call<ApiObjectLoginResult>, response: Response<ApiObjectLoginResult>) {
                    if (response.isSuccessful) {
                        val objectLoginResult = response.body()!!

                        when (objectLoginResult.code) {

                            200 -> {
                                MainActivity.isSignIn = true
                                startActivity(Intent(requireContext(), MainActivity::class.java))

                                val dataLists = objectLoginResult.data
                                SettingFragment.userDataModel = UserDataModel(
                                    dataLists.userId, dataLists.avatar, dataLists.name,
                                    dataLists.gender, dataLists.birthday, dataLists.address,
                                    dataLists.email, dataLists.phone)

                                val map = HashMap<String, String>()
                                map["account"] = binding.editInputMobile.text.toString()
                                map["password"] = binding.editInputPassword.text.toString()
                                StorageDataMaintain.saveMap(requireContext(), map)
                                StorageDataMaintain.setIsLogin(requireContext(), true)

                                requireActivity().finish()
                            }

                            400 -> {
                                val args = Bundle()
                                args.putBoolean("feedbackResult", false)
                                args.putString("description", objectLoginResult.errors)
                                val otpResultDialogFragment = OtpResultDialogFragment()
                                otpResultDialogFragment.arguments = args
                                otpResultDialogFragment.show(requireActivity().supportFragmentManager, "OTP_dialog")
                            }
                        }
                        loadingDialog.dismiss()
                    }
                }

                override fun onFailure(call: Call<ApiObjectLoginResult>, t: Throwable) {
                    loadingDialog.dismiss()
                }

            })
        }

        /** 第三方登入測試
        private val thirdLogin = ThirdLogin.getThirdLoginInstance()
        private lateinit var auth: FirebaseAuth
        private lateinit var loginType: LoginType

        //FB
        private lateinit var callbackManager: CallbackManager
        // google sing in
        auth = Firebase.auth
        thirdLogin.googleSignInBuild(requireActivity())
        binding.googleSignIn.setOnClickListener {
        loginType = LoginType.GOOGLE
        startActivityForResult(thirdLogin.mGoogleSignInClient!!.signInIntent, 200)
        }

        binding.googleSignOut.setOnClickListener {

        if (loginType == LoginType.GOOGLE ){
        AuthUI.getInstance().signOut(requireContext())
        }else{
        Firebase.auth.signOut()
        }
        }
        // FB
        callbackManager = CallbackManager.Factory.create();
        binding.fblogin.setReadPermissions("email");
        // If using in a fragment
        binding.fblogin.fragment = this;

        // Callback registration
        binding.fblogin.setOnClickListener {
        loginType = LoginType.FACEBOOK
        }

        binding.fblogin.registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {
        override fun onSuccess(loginResult: LoginResult?) {
        // App code
        handleFacebookAccessToken(loginResult!!.accessToken);
        }

        override fun onCancel() {
        // App code
        Log.d("ben", "callbackManager onCancel")
        }

        override fun onError(exception: FacebookException) {
        // App code
        Log.d("ben", "callbackManager exception = $exception")
        }
        })


        // If the access token is available already assign it.
        var accessToken = AccessToken.getCurrentAccessToken()

        var profileTracker = object : ProfileTracker() {
        override fun onCurrentProfileChanged(oldProfile: Profile?, currentProfile: Profile?) {
        // App code
        if (currentProfile != null){
        Log.d("ben", "currentProfile.id = ${currentProfile!!.id}")
        Log.d("ben", "currentProfile.name = ${currentProfile.name}")
        Log.d("ben", "currentProfile.firstName = ${currentProfile.firstName}")
        Log.d("ben", "currentProfile.middleName = ${currentProfile.middleName}")
        Log.d("ben", "currentProfile.lastName = ${currentProfile.lastName}")
        Log.d("ben", "currentProfile.linkUri = ${currentProfile.linkUri}")
        }

        }
        }
         */
        return binding.root
    }


    /** Toolbar 登入 Setting */
    private fun toolbarView() {
        val params = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0, MainActivity.statusBarHeight, 0, 0)
        binding.llContent.layoutParams = params
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireActivity(), color.redC70000)
        binding.toolbar.materialToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity(), color.redC70000))

        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar.materialToolbar)
        setHasOptionsMenu(true)
        binding.toolbar.materialToolbar.title = ""
        binding.toolbar.materialToolbar.setNavigationIcon(drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { (activity as FragmentActivity).onBackPressed() }
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
            Global.hideSoftKeyBroad(requireActivity(), binding.root)
            binding.editInputMobile.clearFocus()
            binding.editInputPassword.clearFocus()
        }
        return true
    }

    /** 第三方登入測試
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)

    if (loginType == LoginType.FACEBOOK) {
    callbackManager.onActivityResult(requestCode, resultCode, data)
    } else if (loginType == LoginType.GOOGLE) {
    if (requestCode == 200) {
    val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
    try {
    // Google Sign In was successful, authenticate with Firebase
    val account = task.getResult(ApiException::class.java)
    Log.d("ben", "firebaseAuthWithGoogle account.id:" + account.id)
    Log.d("ben", "firebaseAuthWithGoogle account.idToken:" + account.idToken)
    thirdLogin.firebaseAuthWithGoogle(requireActivity(), account.idToken!!)
    } catch (e: ApiException) {
    // Google Sign In failed, update UI appropriately
    Log.w("ben", "Google sign in failed", e)
    }
    }
    }
    }

    fun handleFacebookAccessToken(token: AccessToken) {

    val credential = FacebookAuthProvider.getCredential(token.token)
    ThirdLogin.mAuth!!.signInWithCredential(credential)
    .addOnCompleteListener(requireActivity()) { task ->
    if (task.isSuccessful) {
    // Sign in success, update UI with the signed-in user's information
    Log.d("ben", "handleFacebookAccessToken signInWithCredential:success")
    val user = ThirdLogin.mAuth!!.currentUser
    Log.d("ben", "handleFacebookAccessToken user.uid = ${user!!.uid}")
    Log.d("ben", "handleFacebookAccessToken user.providerData[0].photoUrl = ${user.providerData[0].photoUrl}")
    Log.d("ben", "handleFacebookAccessToken user.providerData[0].email = ${user.providerData[0].email}")
    Log.d("ben", "handleFacebookAccessToken user.metadata = ${user.metadata}")
    } else {
    // If sign in fails, display a message to the user.
    Log.d("ben", "handleFacebookAccessToken signInWithCredential:failure", task.exception)
    }
    }
    }

    enum class LoginType {
    GOOGLE, FACEBOOK
    }
     */
}