package net.asiafm.media

import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import android.support.v4.media.MediaBrowserCompat.MediaItem
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.text.TextUtils
import android.util.Log
import androidx.media.MediaBrowserServiceCompat
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import net.asiafm.shared.MediaNotificationManager
import net.asiafm.shared.MusicService
import net.asiafm.shared.PlaybackStatus
import org.greenrobot.eventbus.EventBus


class MobileMediaService : MusicService() {

    private val iBinder: IBinder = LocalBinder()

    inner class LocalBinder : Binder() {
        val service: MobileMediaService
            get() = this@MobileMediaService
    }


    override fun onBind(intent: Intent): IBinder {
        return iBinder
    }

    override fun onUnbind(intent: Intent): Boolean {
        if (status == PlaybackStatus.IDLE) stopSelf()
        return super.onUnbind(intent)
    }



}