package net.asiafm.asiaAPI

import net.asiafm.asiaAPI.dataModel.*
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

interface AsiaAPIs {

    /** 登入*/
    @Headers("Content-Type: application/json")
    @POST("user/login")
    fun getLoginResult(@Body jsonObject: JsonObject): Call<ApiObjectLoginResult>

    /** 註冊*/
    @Headers("Content-Type: application/json")
    @POST("user/register")
    fun getRegisterResult(@Body jsonObject: JsonObject): Call<ApiObjectRegister>

    /** 會員資料更新*/
    @Headers("Content-Type: application/json")
    @POST("user/update")
    fun updateUserInfo(@Body jsonObject: JsonObject): Call<ApiObjectUpdateUserInfo>

    /** 會員密碼更新*/
    @Headers("Content-Type: application/json")
    @POST("user/updatePassword")
    fun updateUserPassword(@Body jsonObject: JsonObject): Call<ApiObjectUpdatePassword>

    /** 最近播放10首歌*/
    @Headers("Content-Type: application/json")
    @GET("channel/info")
    fun getLatestMusic(@Query("stationID") radioID: Int): Call<ApiObjectLatestMusic>

    /**@param postSlug
     * 亞洲大頭條:asia927-bignews
     * 亞太大頭條:asia923-bignews
     * */
    @Headers("Content-Type: application/json")
    @GET("post/asia/info")
    fun getBigNewsArticle(@Query("postSlug") postSlug: String , @Query("limit") ArticleCount: Int): Call<ApiObjectArticle>


    @Headers("Content-Type: application/json")
    @GET("post/info")
    fun getArticle(@Query("postType") postType: String , @Query("limit") ArticleCount: Int): Call<ApiObjectArticle>


    /**@param postSlug
     * 亞洲電台 : asia927， 亞太電台 : asia923
     * @param weekdayId
     * 星期日 : 1020 , 星期一 : 1021 , 星期二 : 1022 , 星期三 : 1023 , 星期四 : 1024 , 星期五 : 1025 , 星期六 : 1026
     * */
    @Headers("Content-Type: application/json")
    @GET("station/program")
    fun getShowList(@Query("stationSlug") postSlug: String , @Query("weekdayId") weekdayId: Int): Call<ApiObjectShowList>


    /**@param stationID
     * 亞洲電台 : 1， 亞太電台 : 2
     * @param startDate
     *  2021-08-04
     *  @param timePeriod
     *  14:00:00-18:00:00
     * */
    @Headers("Content-Type: application/json")
    @GET("search/music")
    fun getSearchMusicList(@Query("stationID") stationID: Int , @Query("startDate") startDate: String, @Query("timePeriod") timePeriod: String):
            Call<ApiObjectSearchMusic>


    /** 影音專區 */
    @Headers("Content-Type: application/json")
    @GET("youtube/channel")
    fun getYoutubeVideoList(): Call<ApiObjectYoutubePlayerList>

}