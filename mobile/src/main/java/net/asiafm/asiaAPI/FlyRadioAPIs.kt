package net.asiafm.asiaAPI


import net.asiafm.asiaAPI.dataModel.ApiObjectArticle
import net.asiafm.asiaAPI.dataModel.ApiObjectLatestMusic
import net.asiafm.asiaAPI.dataModel.ApiObjectSearchMusic
import net.asiafm.asiaAPI.dataModel.ApiObjectShowList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface FlyRadioAPIs {


    /** 最近播放10首歌*/
    @Headers("Content-Type: application/json")
    @GET("channel/info")
    fun getLatestMusic(@Query("stationID") radioID: Int): Call<ApiObjectLatestMusic>


    @Headers("Content-Type: application/json")
    @GET("post/info")
    fun getArticle(@Query("postType") postType: String , @Query("limit") ArticleCount: Int): Call<ApiObjectArticle>

    /**@param postSlug
     * 飛揚電台 : flyradio
     * @param weekdayId
     * 星期日 : 1020 , 星期一 : 1021 , 星期二 : 1022 , 星期三 : 1023 , 星期四 : 1024 , 星期五 : 1025 , 星期六 : 1026
     * */
    @Headers("Content-Type: application/json")
    @GET("station/program")
    fun getShowList(@Query("stationSlug") postSlug: String , @Query("weekdayId") weekdayId: Int): Call<ApiObjectShowList>


    /**@param stationID
     * 飛揚電台 : 3，
     * @param startDate
     *  2021-08-04
     *  @param timePeriod
     *  14:00:00-18:00:00
     * */
    @Headers("Content-Type: application/json")
    @GET("search/music")
    fun getSearchMusicList(@Query("stationID") stationID: Int , @Query("startDate") startDate: String, @Query("timePeriod") timePeriod: String): Call<ApiObjectSearchMusic>


}