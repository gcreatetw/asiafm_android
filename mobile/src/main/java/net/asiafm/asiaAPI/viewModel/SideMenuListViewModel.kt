package net.asiafm.asiaAPI.viewModel

import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.asiafm.asiaAPI.RetrofitInstance

import net.asiafm.asiaAPI.dataModel.ApiObjectArticle
import net.asiafm.asiaAPI.dataModel.Info
import net.asiafm.model.adapter.SideMenuArticleAdapter
import net.asiafm.view.inAppBrowser.WebViewActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SideMenuListViewModel : ViewModel() {

    private var recyclerListData: MutableLiveData<ApiObjectArticle> = MutableLiveData()
    private var callListApi: Call<ApiObjectArticle>? = null
    private var articleListsAdapter: SideMenuArticleAdapter = SideMenuArticleAdapter(mutableListOf())
    private var toolbarTitle = ""

    fun getAdapter(): SideMenuArticleAdapter {
        return articleListsAdapter
    }

    fun setAdapterData(data: MutableList<Info>) {
        articleListsAdapter.data = data
        articleListsAdapter.notifyDataSetChanged()

        articleListsAdapter.setOnItemClickListener { _ , view , i ->
            val intent = Intent(view.context , WebViewActivity::class.java)
            intent.putExtra("ToolbarTitle" , toolbarTitle)
            intent.putExtra("extraUrl" , data[i].guid)
            startActivity(view.context , intent , null)
        }
    }

    fun getRecyclerListDataObserver(): MutableLiveData<ApiObjectArticle> {
        return recyclerListData
    }

    fun getApiData(postType: String) {
        val asiaApiInstance = RetrofitInstance.getAsiaApiInstance()!!
        val flyRadioApiInstance = RetrofitInstance.getFlyRadioApiInstance()!!
        toolbarTitle = postType

        when (postType) {
            "ASIA大頭條" -> callListApi = asiaApiInstance.getArticle("asianews" , 20)
            "飛揚大頭條" -> callListApi = flyRadioApiInstance.getArticle("flyradio_news" , 20)
            "生活情報讚" -> callListApi = asiaApiInstance.getArticle("lifereport" , 20)
            "活動報馬仔" -> callListApi = asiaApiInstance.getArticle("activityreport" , 20)
            "好康贈獎" -> callListApi = asiaApiInstance.getArticle("goodluckgift" , 20)
            "得獎名單" -> callListApi = asiaApiInstance.getArticle("winnerslist" , 20)
        }

        callListApi!!.enqueue(object : Callback<ApiObjectArticle> {
            override fun onResponse(call: Call<ApiObjectArticle> , response: Response<ApiObjectArticle>) {
                if (response.isSuccessful) {
                    recyclerListData.postValue(response.body())
                } else {
                    recyclerListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ApiObjectArticle> , t: Throwable) {
                recyclerListData.postValue(null)
            }

        })
    }

}