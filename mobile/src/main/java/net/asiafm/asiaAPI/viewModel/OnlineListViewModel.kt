package net.asiafm.asiaAPI.viewModel


import android.annotation.SuppressLint
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.asiafm.asiaAPI.RetrofitInstance
import net.asiafm.asiaAPI.dataModel.ApiObjectArticle
import net.asiafm.asiaAPI.dataModel.ApiObjectLatestMusic
import net.asiafm.asiaAPI.dataModel.Info
import net.asiafm.asiaAPI.dataModel.LatestMusic
import net.asiafm.model.adapter.*
import net.asiafm.shared.MusicService.Companion.getIsStartMediaPlayer
import net.asiafm.view.MainActivity
import net.asiafm.view.inAppBrowser.WebViewActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("NotifyDataSetChanged")
class OnlineListViewModel : ViewModel() {
    private val asiaApiInstance = RetrofitInstance.getAsiaApiInstance()!!
    private val flyRadioApiInstance = RetrofitInstance.getFlyRadioApiInstance()!!

    val imageUrl = ObservableField<String>()
    val musicSinger = ObservableField<String>()
    val musicName = ObservableField<String>()

    companion object {
        var callRecentlyMusicListApi: Call<ApiObjectLatestMusic>? = null

        //        var callBigNewsListApi: Call<ApiObjectArticle>? = null
        var callLifeNewsListApi: Call<ApiObjectArticle>? = null
        var callCouponActivityListApi: Call<ApiObjectArticle>? = null
        var callCouponGiftListApi: Call<ApiObjectArticle>? = null

        fun callCancel() {
            if (callLifeNewsListApi != null) callLifeNewsListApi!!.cancel()
            if (callCouponActivityListApi != null) callCouponActivityListApi!!.cancel()
            if (callCouponGiftListApi != null) callCouponGiftListApi!!.cancel()
        }

        fun callRecentlyMusicCancel() {
            if (callRecentlyMusicListApi != null) callRecentlyMusicListApi!!.cancel()

        }
    }

    //  最近10首歌
    private var recentlyMusicListData: MutableLiveData<ApiObjectLatestMusic> = MutableLiveData()
    private var recentlyMusicListsAdapter: RecentlyMusicAdapter = RecentlyMusicAdapter(mutableListOf())

    fun getRecentlyMusicAdapter(): RecentlyMusicAdapter {
        return recentlyMusicListsAdapter
    }

    fun setRecentlyMusicAdapterData(data: MutableList<LatestMusic>) {
        recentlyMusicListsAdapter.data = data
        recentlyMusicListsAdapter.notifyDataSetChanged()
        recentlyMusicListsAdapter.setOnItemClickListener { _, view, position ->
            val queryString =
                String.format("%s+%s", data[position].song_name, data[position].artist_name)
            val intent = Intent(view.context, WebViewActivity::class.java)
            intent.putExtra("QueryString", queryString)
            startActivity(view.context, intent, null)
        }

        if (getIsStartMediaPlayer()) {
            MainActivity.mainObjectModel.setMusicSinger(data[0].artist_name)
            MainActivity.mainObjectModel.setMusicName(data[0].song_name)
            musicSinger.set(data[0].artist_name)
            musicName.set(data[0].song_name)

            if (data[0].picture_url.isNullOrEmpty()) {
                MainActivity.mainObjectModel.setImageUrl("")
                imageUrl.set("")
            } else {
                MainActivity.mainObjectModel.setImageUrl(data[0].picture_url)
                imageUrl.set(data[0].picture_url)
            }
        }

    }

    fun getRecentlyMusicDataObserver(): MutableLiveData<ApiObjectLatestMusic> {
        return recentlyMusicListData
    }


    fun getRecentlyMusicApiData(selectedIndex: Int) {

        when (selectedIndex) {
            0 -> callRecentlyMusicListApi = asiaApiInstance.getLatestMusic(1)
            1 -> callRecentlyMusicListApi = flyRadioApiInstance.getLatestMusic(3)
            2 -> callRecentlyMusicListApi = asiaApiInstance.getLatestMusic(2)
        }


        callRecentlyMusicListApi!!.enqueue(object : Callback<ApiObjectLatestMusic> {
            override fun onResponse(call: Call<ApiObjectLatestMusic>, response: Response<ApiObjectLatestMusic>) {
                if (response.isSuccessful) {
                    recentlyMusicListData.postValue(response.body())
                } else {
                    recentlyMusicListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ApiObjectLatestMusic>, t: Throwable) {
                lifeNewsListData.postValue(null)
            }

        })
    }


    //  XX 大頭條
    private var bigNewsListData: MutableLiveData<ApiObjectArticle> = MutableLiveData()
    private var bigNewsListsAdapter: BigNewsAdapter = BigNewsAdapter()
    private var callBigNewsListApi: Call<ApiObjectArticle>? = null
    private val articleImages = mutableListOf<String>()

    fun getBigNewsAdapter(): BigNewsAdapter {
        return bigNewsListsAdapter
    }

    fun setBigNewsImageUrlLists(data: MutableList<Info>) {
        articleImages.clear()

        for (element in data) {
            articleImages.add(element.imgUrl)
        }

    }

    fun returnBigNewsImageUrlLists(): MutableList<String> {
        return articleImages
    }

    fun getBigNewsDataObserver(): MutableLiveData<ApiObjectArticle> {
        return bigNewsListData
    }

    // check ok
    fun getBigNewsApiData(isAsiaRadio: Boolean, postSlug: String) {

        callBigNewsListApi = if (isAsiaRadio) {
            asiaApiInstance.getBigNewsArticle(postSlug, 5)
        } else {
            flyRadioApiInstance.getArticle(postSlug, 5)
        }

        callBigNewsListApi!!.enqueue(object : Callback<ApiObjectArticle> {
            override fun onResponse(call: Call<ApiObjectArticle>, response: Response<ApiObjectArticle>) {
                if (response.isSuccessful) {
                    bigNewsListData.postValue(response.body())
                } else {
                    bigNewsListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ApiObjectArticle>, t: Throwable) {
                bigNewsListData.postValue(null)
            }

        })
    }


    //  生活情報讚
    private var lifeNewsListData: MutableLiveData<ApiObjectArticle> = MutableLiveData()
    private var lifeNewsListsAdapter: LifeNewsAdapter = LifeNewsAdapter(mutableListOf())

    fun getLifeNewsAdapter(): LifeNewsAdapter {
        return lifeNewsListsAdapter
    }

    fun setLifeNewsAdapterData(data: MutableList<Info>) {
        lifeNewsListsAdapter.data = data
        lifeNewsListsAdapter.notifyDataSetChanged()
        lifeNewsListsAdapter.setOnItemClickListener { _, view, i ->
            val intent = Intent(view.context, WebViewActivity::class.java)
            intent.putExtra("ToolbarTitle", "生活情報讚")
            intent.putExtra("extraUrl", data[i].guid)
            startActivity(view.context, intent, null)
        }
    }

    fun getLifeNewsDataObserver(): MutableLiveData<ApiObjectArticle> {
        return lifeNewsListData
    }

    fun getLifeNewsApiData(isAsiaRadio: Boolean) {

        callLifeNewsListApi = if (isAsiaRadio) {
            asiaApiInstance.getArticle("lifereport", 10)
        } else {
            flyRadioApiInstance.getArticle("lifereport", 10)
        }

        callLifeNewsListApi!!.enqueue(object : Callback<ApiObjectArticle> {
            override fun onResponse(call: Call<ApiObjectArticle>, response: Response<ApiObjectArticle>) {
                if (response.isSuccessful) {
                    lifeNewsListData.postValue(response.body())
                } else {
                    lifeNewsListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ApiObjectArticle>, t: Throwable) {
                lifeNewsListData.postValue(null)
            }

        })
    }


    // 活動報馬仔
    private var couponActivityListData: MutableLiveData<ApiObjectArticle> = MutableLiveData()
    private var couponActivityListsAdapter: CouponActivityAdapter =
        CouponActivityAdapter(mutableListOf())

    fun getCouponActivityAdapter(): CouponActivityAdapter {
        return couponActivityListsAdapter
    }

    fun setCouponActivityAdapterData(data: MutableList<Info>) {
        couponActivityListsAdapter.data = data
        couponActivityListsAdapter.notifyDataSetChanged()
        couponActivityListsAdapter.setOnItemClickListener { _, view, i ->
            val intent = Intent(view.context, WebViewActivity::class.java)
            intent.putExtra("ToolbarTitle", "活動報馬仔")
            intent.putExtra("extraUrl", data[i].guid)
            startActivity(view.context, intent, null)
        }
    }

    fun getCouponActivityDataObserver(): MutableLiveData<ApiObjectArticle> {
        return couponActivityListData
    }

    fun getCouponActivityApiData(isAsiaRadio: Boolean) {

        callCouponActivityListApi = if (isAsiaRadio) {
            asiaApiInstance.getArticle("activityreport", 10)
        } else {
            flyRadioApiInstance.getArticle("activityreport", 10)
        }

        callCouponActivityListApi!!.enqueue(object : Callback<ApiObjectArticle> {
            override fun onResponse(call: Call<ApiObjectArticle>, response: Response<ApiObjectArticle>) {
                if (response.isSuccessful) {
                    couponActivityListData.postValue(response.body())
                } else {
                    couponActivityListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ApiObjectArticle>, t: Throwable) {
                couponActivityListData.postValue(null)
            }

        })
    }


    // 好康贈禮
    private var couponGiftListData: MutableLiveData<ApiObjectArticle> = MutableLiveData()
    private var couponGiftListsAdapter: CouponGiftAdapter = CouponGiftAdapter(mutableListOf())

    fun getCouponGiftAdapter(): CouponGiftAdapter {
        return couponGiftListsAdapter
    }

    fun setCouponGiftAdapterData(data: MutableList<Info>) {
        couponGiftListsAdapter.data = data
        couponGiftListsAdapter.notifyDataSetChanged()
        couponGiftListsAdapter.setOnItemClickListener { _, view, i ->
            val intent = Intent(view.context, WebViewActivity::class.java)
            intent.putExtra("ToolbarTitle", "好康贈禮")
            intent.putExtra("extraUrl", data[i].guid)
            startActivity(view.context, intent, null)
        }
    }

    fun getCouponGiftDataObserver(): MutableLiveData<ApiObjectArticle> {
        return couponGiftListData
    }

    fun getCouponGiftApiData(isAsiaRadio: Boolean) {

        callCouponGiftListApi = if (isAsiaRadio) {
            asiaApiInstance.getArticle("goodluckgift", 10)
        } else {
            flyRadioApiInstance.getArticle("goodluckgift", 10)
        }

        callCouponGiftListApi!!.enqueue(object : Callback<ApiObjectArticle> {
            override fun onResponse(call: Call<ApiObjectArticle>, response: Response<ApiObjectArticle>) {
                if (response.isSuccessful) {
                    couponGiftListData.postValue(response.body())
                } else {
                    couponGiftListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ApiObjectArticle>, t: Throwable) {
                couponGiftListData.postValue(null)
            }

        })
    }


}