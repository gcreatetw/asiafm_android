package net.asiafm.asiaAPI.viewModel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

class MainObjectModel : BaseObservable() {


    private var imageUrl: String = ""
    private var musicSinger: String = "I LOVE ASIA FM"
    private var musicName: String = "92.7 亞洲電台"

    @Bindable
    fun getImageUrl(): String {
        return imageUrl
    }

    fun setImageUrl(imageUrl: String) {
        this.imageUrl = imageUrl
        notifyPropertyChanged(net.asiafm.BR.imageUrl)
    }

    @Bindable
    fun getMusicSinger(): String {
        return musicSinger
    }

    fun setMusicSinger(musicSinger: String) {
        this.musicSinger = musicSinger
        notifyPropertyChanged(net.asiafm.BR.musicSinger)
    }


    @Bindable
    fun getMusicName(): String {
        return musicName
    }

    fun setMusicName(musicName: String) {
        this.musicName = musicName
        notifyPropertyChanged(net.asiafm.BR.musicName)
    }


}