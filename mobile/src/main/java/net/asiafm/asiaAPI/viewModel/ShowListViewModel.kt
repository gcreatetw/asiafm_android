package net.asiafm.asiaAPI.viewModel

import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.asiafm.R
import net.asiafm.asiaAPI.RetrofitInstance
import net.asiafm.asiaAPI.dataModel.ApiObjectShowList
import net.asiafm.listener.OnItemClickListener
import net.asiafm.model.adapter.RadioShowListAdapter
import net.asiafm.model.adapter.RadioShowListWeekAdapter
import net.asiafm.util.DateTool
import net.asiafm.view.inAppBrowser.WebViewActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ShowListViewModel : ViewModel() {

    private var objectShowListData: MutableLiveData<ApiObjectShowList> = MutableLiveData()
    private var callShowListApi: Call<ApiObjectShowList>? = null
    private var radioShowListAdapter: RadioShowListAdapter = RadioShowListAdapter()

    private val asiaApiInstance = RetrofitInstance.getAsiaApiInstance()!!
    private val flyRadioApiInstance = RetrofitInstance.getFlyRadioApiInstance()!!

    companion object {
        /** dayOfWeek
         *  0:日、1:一、2:二、3:三
         *  4:四、5:五、6:六
         *  */
        val dayOfWeek = DateTool.getDayOfWeek(Calendar.getInstance().timeInMillis)
        var currentRadioIndexSlug = "asia927"
        var currentDayOfWeekIndex = if (dayOfWeek != 0) {
            dayOfWeek - 1
        } else {
            6
        }
    }

    fun getAdapter(): RadioShowListAdapter {
        return radioShowListAdapter
    }

    fun getWeekAdapter(): RadioShowListWeekAdapter {
        val weekList = mutableListOf("一", "二", "三", "四", "五", "六", "日")
        val weekAdapter = RadioShowListWeekAdapter(currentDayOfWeekIndex, weekList)


        weekAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(view: View?, position: Int) {
                currentDayOfWeekIndex = position
                getShowListApiData(currentRadioIndexSlug, currentDayOfWeekIndex)
            }
        })
        return weekAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setAdapterData(data: ApiObjectShowList) {

        if (!data.info.isNullOrEmpty()) {
            radioShowListAdapter.data = data.info
        } else {
            radioShowListAdapter.data = mutableListOf()
        }
        radioShowListAdapter.notifyDataSetChanged()

        radioShowListAdapter.setOnItemClickListener { _, view, position ->
            val intent = Intent(view.context, WebViewActivity::class.java)
            intent.putExtra("ToolbarTitle", "DJ介紹")
            intent.putExtra("extraUrl", data.info[position].guid)
            ContextCompat.startActivity(view.context, intent, null)
        }
    }

    fun getObjectShowListDataObserver(): MutableLiveData<ApiObjectShowList> {
        return objectShowListData
    }

    fun getShowListApiData(radioSlug: String, dayOfWeekIndex: Int) {
        /**星期日 : 1020
        星期一 : 1021
        星期二 : 1022
        星期三 : 1023
        星期四 : 1024
        星期五 : 1025
        星期六 : 1026
         * */
        when (dayOfWeekIndex) {
            0 -> {
                //  星期一
                when (radioSlug) {
                    "asia927" -> callShowListApi = asiaApiInstance.getShowList("asia927", 1021)
                    "asia923" -> callShowListApi = asiaApiInstance.getShowList("asia923", 1021)
                    "flyradio" -> callShowListApi = flyRadioApiInstance.getShowList("flyradio", 1021)
                }
            }

            1 -> {
                //  星期二
                when (radioSlug) {
                    "asia927" -> callShowListApi = asiaApiInstance.getShowList("asia927", 1022)
                    "asia923" -> callShowListApi = asiaApiInstance.getShowList("asia923", 1022)
                    "flyradio" -> callShowListApi = flyRadioApiInstance.getShowList("flyradio", 1022)
                }
            }

            2 -> {
                //  星期三
                when (radioSlug) {
                    "asia927" -> callShowListApi = asiaApiInstance.getShowList("asia927", 1023)
                    "asia923" -> callShowListApi = asiaApiInstance.getShowList("asia923", 1023)
                    "flyradio" -> callShowListApi = flyRadioApiInstance.getShowList("flyradio", 1023)
                }
            }

            3 -> {
                //  星期四
                when (radioSlug) {
                    "asia927" -> callShowListApi = asiaApiInstance.getShowList("asia927", 1024)
                    "asia923" -> callShowListApi = asiaApiInstance.getShowList("asia923", 1024)
                    "flyradio" -> callShowListApi = flyRadioApiInstance.getShowList("flyradio", 1024)
                }
            }

            4 -> {
                //  星期五
                when (radioSlug) {
                    "asia927" -> callShowListApi = asiaApiInstance.getShowList("asia927", 1025)
                    "asia923" -> callShowListApi = asiaApiInstance.getShowList("asia923", 1025)
                    "flyradio" -> callShowListApi = flyRadioApiInstance.getShowList("flyradio", 1025)
                }
            }

            5 -> {
                //  星期六
                when (radioSlug) {
                    "asia927" -> callShowListApi = asiaApiInstance.getShowList("asia927", 1026)
                    "asia923" -> callShowListApi = asiaApiInstance.getShowList("asia923", 1026)
                    "flyradio" -> callShowListApi = flyRadioApiInstance.getShowList("flyradio", 1026)
                }
            }

            6 -> {
                //  星期日
                when (radioSlug) {
                    "asia927" -> callShowListApi = asiaApiInstance.getShowList("asia927", 1020)
                    "asia923" -> callShowListApi = asiaApiInstance.getShowList("asia923", 1020)
                    "flyradio" -> callShowListApi = flyRadioApiInstance.getShowList("flyradio", 1020)
                }
            }
        }

        callShowListApi!!.enqueue(object : Callback<ApiObjectShowList> {
            @SuppressLint("InflateParams")
            override fun onResponse(call: Call<ApiObjectShowList>, response: Response<ApiObjectShowList>) {
                if (response.isSuccessful) {
                    if (radioSlug == "flyradio") {
                        radioShowListAdapter.setErrorImage(R.drawable.icon_default_showitem2)
                    } else {
                        radioShowListAdapter.setErrorImage(R.drawable.icon_default_showitem)
                    }
                    objectShowListData.postValue(response.body())
                } else {
                    objectShowListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ApiObjectShowList>, t: Throwable) {
                objectShowListData.postValue(null)
            }
        })
    }

}