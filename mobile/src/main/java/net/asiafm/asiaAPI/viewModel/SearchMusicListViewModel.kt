package net.asiafm.asiaAPI.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.asiafm.asiaAPI.RetrofitInstance

import net.asiafm.asiaAPI.dataModel.ApiObjectSearchMusic
import net.asiafm.model.adapter.SearchMusicAdapter
import net.asiafm.util.dayOfWeek
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SearchMusicListViewModel : ViewModel() {

    companion object {
        val dayOfWeek = Calendar.getInstance().dayOfWeek
    }

    var recyclerListData: MutableLiveData<ApiObjectSearchMusic> = MutableLiveData()

    private var searchMusicAdapter: SearchMusicAdapter = SearchMusicAdapter()

    fun getAdapter(): SearchMusicAdapter {
        return searchMusicAdapter
    }

    fun setAdapterData(data: ApiObjectSearchMusic) {
        if (!data.list.isNullOrEmpty()) {
            searchMusicAdapter.data = data.list
        } else {
            searchMusicAdapter.data = mutableListOf()
        }
        searchMusicAdapter.notifyDataSetChanged()
    }

    fun getRecyclerListDataObserver(): MutableLiveData<ApiObjectSearchMusic> {
        return recyclerListData
    }

    fun getApiData(stationId: Int , searchDate: String , timePeriod: String) {

        val stringBuffer = StringBuffer(timePeriod)
        stringBuffer.insert(5 , ":00")
        stringBuffer.insert(stringBuffer.length , ":00")

        var callSearchMusicApi: Call<ApiObjectSearchMusic> = if (stationId == 3) {
            RetrofitInstance.getFlyRadioApiInstance()!!.getSearchMusicList(3 , searchDate , stringBuffer.toString())
        } else {
            RetrofitInstance.getAsiaApiInstance()!!.getSearchMusicList(stationId , searchDate, stringBuffer.toString())
        }
        callSearchMusicApi.enqueue(object : Callback<ApiObjectSearchMusic> {
            override fun onResponse(call: Call<ApiObjectSearchMusic> , response: Response<ApiObjectSearchMusic>) {
                if (response.isSuccessful) {
                    recyclerListData.postValue(response.body())
                } else {
                    recyclerListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ApiObjectSearchMusic> , t: Throwable) {
                recyclerListData.postValue(null)
            }

        })
    }

}