package net.asiafm.asiaAPI.viewModel


import android.content.Intent
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.asiafm.asiaAPI.RetrofitInstance
import net.asiafm.asiaAPI.dataModel.ApiObjectYoutubePlayerList
import net.asiafm.asiaAPI.dataModel.Item
import net.asiafm.model.adapter.YoutubePlayerListAdapter
import net.asiafm.view.inAppBrowser.WebViewActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class YoutubePlayListViewModel : ViewModel() {

    var recyclerListData: MutableLiveData<ApiObjectYoutubePlayerList> = MutableLiveData()
    private var youtubeVideoListAdapter: YoutubePlayerListAdapter = YoutubePlayerListAdapter(mutableListOf())

    var isLoading = MutableLiveData<Boolean>()
    var isShowLoading = MutableLiveData<Int>()
    var isShowVideoList = MutableLiveData<Int>()

    fun getAdapter(): YoutubePlayerListAdapter {
        return youtubeVideoListAdapter
    }

    fun setAdapterData(data: MutableList<Item>) {
        isLoading.value = false
        youtubeVideoListAdapter.data = data
        youtubeVideoListAdapter.notifyDataSetChanged()


        youtubeVideoListAdapter.setOnItemClickListener { _, view, i ->
            net.asiafm.listener.MediaPlayListenerManager.getInstance().sendBroadCast(false)
            val intent = Intent(view.context, WebViewActivity::class.java)
            intent.putExtra("LinkURL", data[i].id.videoId)
            ContextCompat.startActivity(view.context, intent, null)
        }
    }

    fun getRecyclerListDataObserver(): MutableLiveData<ApiObjectYoutubePlayerList> {
        return recyclerListData
    }

    fun getApiData() {
        isLoading.value = true
        val youTubeApiInstance = RetrofitInstance.getAsiaApiInstance()!!
        val call = youTubeApiInstance.getYoutubeVideoList()
        call.enqueue(object : Callback<ApiObjectYoutubePlayerList> {
            override fun onResponse(call: Call<ApiObjectYoutubePlayerList>, response: Response<ApiObjectYoutubePlayerList>) {
                if (response.isSuccessful) {
                    recyclerListData.postValue(response.body())
                } else {
                    recyclerListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ApiObjectYoutubePlayerList>, t: Throwable) {
                recyclerListData.postValue(null)
            }

        })
    }

}