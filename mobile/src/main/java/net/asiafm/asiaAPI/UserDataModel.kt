package net.asiafm.asiaAPI


class UserDataModel(var userId: Int ,
                    var userAvatarBase64String: String ,
                    var userName: String ,
                    var userGender: String ,
                    var userBirthday: String ,
                    var userAddress: String ,
                    var userEmail: String ,
                    var userMobile: String)