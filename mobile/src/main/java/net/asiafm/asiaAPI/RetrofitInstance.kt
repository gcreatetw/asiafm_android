package net.asiafm.asiaAPI

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitInstance {

    companion object {

        //  Asia server
        private const val AsiaDomain = "https://www.asiafm.com.tw/wp-json/api/"
        private var asiaApiInstance: AsiaAPIs? = null

        fun getAsiaApiInstance(): AsiaAPIs? {
            if (asiaApiInstance == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor { chain ->
                    val builder = chain.request().newBuilder()
                    val build = builder.build()
                    chain.proceed(build)
                }.connectTimeout(60 , TimeUnit.SECONDS).readTimeout(60 , TimeUnit.SECONDS).build()
                asiaApiInstance = Retrofit.Builder().baseUrl(AsiaDomain).addConverterFactory(GsonConverterFactory.create()).client(client).build()
                    .create(AsiaAPIs::class.java)
            }
            return asiaApiInstance
        }

        //  FlyRadio server
        private const val FlyRadioDomain = "https://www.flyradio.com.tw/wp-json/api/"
        private var flyRadioInstance: FlyRadioAPIs? = null

        fun getFlyRadioApiInstance(): FlyRadioAPIs? {
            if (flyRadioInstance == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor { chain ->
                    val builder = chain.request().newBuilder()
                    val build = builder.build()
                    chain.proceed(build)
                }.connectTimeout(60 , TimeUnit.SECONDS).readTimeout(60 , TimeUnit.SECONDS).build()
                flyRadioInstance = Retrofit.Builder().baseUrl(FlyRadioDomain).addConverterFactory(GsonConverterFactory.create()).client(client).build()
                    .create(FlyRadioAPIs::class.java)
            }
            return flyRadioInstance
        }

    }
}