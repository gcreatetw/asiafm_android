package net.asiafm.asiaAPI.dataModel

import com.google.gson.annotations.SerializedName

data class ApiObjectSearchMusic(
    @SerializedName("code")
    val code: Int,
    @SerializedName("list")
    val list: MutableList<ItemModel>,
    @SerializedName("message")
    val message: String
)

data class ItemModel(
    @SerializedName("artist")
    val artist: String,
    @SerializedName("playTime")
    val playTime: String,
    @SerializedName("title")
    val title: String
)