package net.asiafm.asiaAPI.dataModel

import com.google.gson.annotations.SerializedName

data class ApiObjectArticle(
    @SerializedName("code")
    val code: Int,
    @SerializedName("info")
    val info: MutableList<Info>,
    @SerializedName("message")
    val message: String
)

data class Info(
    @SerializedName("guid")
    val guid: String,
    @SerializedName("imgUrl")
    val imgUrl: String,
    @SerializedName("postDate")
    val postDate: String,
    @SerializedName("postId")
    val postId: String,
    @SerializedName("postTitle")
    val postTitle: String,
    @SerializedName("postType")
    val postType: String
)