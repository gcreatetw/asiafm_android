package net.asiafm.asiaAPI.dataModel

import com.google.gson.annotations.SerializedName

data class ApiObjectRegister(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val data: RegisterModel,
    @SerializedName("message")
    val message: String,
    @SerializedName("errors")
    val errors: String
)

data class RegisterModel(
    @SerializedName("address")
    val address: String,
    @SerializedName("avatar")
    val avatar: String,
    @SerializedName("birthday")
    val birthday: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("user_id")
    val user_id: Int
)