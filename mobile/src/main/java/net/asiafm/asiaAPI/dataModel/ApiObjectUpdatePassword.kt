package net.asiafm.asiaAPI.dataModel

data class ApiObjectUpdatePassword(
    val code: Int,
    val errors: String,
    val message: String
)