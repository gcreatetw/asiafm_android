package net.asiafm.asiaAPI.dataModel

import com.google.gson.annotations.SerializedName

data class ApiObjectLoginResult(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val data: DataModel,
    @SerializedName("errors")
    val errors: String,
    @SerializedName("message")
    val message: String
)

data class DataModel(
    @SerializedName("address")
    val address: String,
    @SerializedName("avatar")
    val avatar: String,
    @SerializedName("birthday")
    val birthday: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("userId")
    val userId: Int
)