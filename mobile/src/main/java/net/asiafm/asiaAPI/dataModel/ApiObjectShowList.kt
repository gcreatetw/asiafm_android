package net.asiafm.asiaAPI.dataModel

import com.google.gson.annotations.SerializedName

data class ApiObjectShowList(
    @SerializedName("code")
    val code: Int,
    @SerializedName("info")
    val info: MutableList<InfoModel>,
    @SerializedName("message")
    val message: String
)

data class InfoModel(
    @SerializedName("djId")
    val djId: String,
    @SerializedName("djName")
    val djName: String,
    @SerializedName("endTime")
    val endTime: Any,
    @SerializedName("eventId")
    val eventId: String,
    @SerializedName("eventTitle")
    val eventTitle: String,
    @SerializedName("guid")
    val guid: String,
    @SerializedName("imgUrl")
    val imgUrl: String,
    @SerializedName("startTime")
    val startTime: Any,
    @SerializedName("stationId")
    val stationId: String,
    @SerializedName("stationSlug")
    val stationSlug: String,
    @SerializedName("weekDay")
    val weekDay: String,
    @SerializedName("weekdayId")
    val weekdayId: String
)