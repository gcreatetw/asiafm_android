package net.asiafm.asiaAPI.dataModel

import com.google.gson.annotations.SerializedName

data class ApiObjectLatestMusic(
    @SerializedName("code")
    val code: Int,
    @SerializedName("latestMusic")
    val latestMusic: MutableList<LatestMusic>,
    @SerializedName("message")
    val message: String
)

data class LatestMusic(
    @SerializedName("artist_name")
    val artist_name: String,
    @SerializedName("picture_url")
    val picture_url: String,
    @SerializedName("rank")
    val rank: String,
    @SerializedName("song_name")
    val song_name: String
)