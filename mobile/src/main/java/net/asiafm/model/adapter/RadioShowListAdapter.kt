package net.asiafm.model.adapter

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import net.asiafm.R
import net.asiafm.asiaAPI.dataModel.InfoModel

class RadioShowListAdapter : BaseQuickAdapter<InfoModel, BaseViewHolder>(R.layout.rv_item_style2) {


    private var errorImageResource: Int? = 0

    fun setErrorImage(errorImageResource: Int) {
        this.errorImageResource = errorImageResource
    }

    override fun convert(holder: BaseViewHolder, item: InfoModel) {
        val itemImage: ImageView = holder.getView(R.id.img_show)

        holder.setText(R.id.tv_show_name, item.eventTitle)
        holder.setText(R.id.tv_show_time, String.format("%s-%s", item.startTime, item.endTime))
        holder.setText(R.id.tv_show_dj, String.format("DJ %s", item.djName))

        Glide.with(context).load(item.imgUrl).error(errorImageResource!!).into(itemImage)

    }

}