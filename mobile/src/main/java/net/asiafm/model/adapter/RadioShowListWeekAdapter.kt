package net.asiafm.model.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

import net.asiafm.R
import net.asiafm.listener.OnItemClickListener
import net.asiafm.view.ui.login.LoginActivity


/** 首頁-選擇科別 Adapter
 * */
class RadioShowListWeekAdapter(dayOfWeek: Int, private val dataList: MutableList<String>) :
    RecyclerView.Adapter<RadioShowListWeekAdapter.ViewHolder>() {

    private var lastSelectedPosition = dayOfWeek
    private var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.rv_item_week, parent, false)
        v.layoutParams.width = (LoginActivity.windowWidth / 7)
        return ViewHolder(v)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)
        if (payloads.isEmpty()) {
            holder.selectedWeek.text = dataList[position]


            if (position == lastSelectedPosition) {
                holder.checkBox.isChecked = true
                holder.selectedWeek.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.blue00BFFF))
            }

            holder.itemView.setOnClickListener {
                onItemClickListener!!.onItemClick(holder.itemView, position)

                holder.selectedWeek.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.blue00BFFF))
                holder.checkBox.isChecked = true
                if (position != lastSelectedPosition) {
                    notifyItemChanged(lastSelectedPosition, 0)
                }
                lastSelectedPosition = position

            }
        } else {
            when (payloads[0] as Int) {
                0 -> {
                    holder.selectedWeek.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.grayC0C1C1))
                    holder.checkBox.isChecked = false
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        // initial clinicTypeCard
        val checkBox: CheckBox = v.findViewById(R.id.checkbox)
        val selectedWeek: TextView = v.findViewById(R.id.tv_week)

    }


}