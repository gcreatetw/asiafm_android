package net.asiafm.model.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import net.asiafm.R
import net.asiafm.asiaAPI.dataModel.ItemModel
import net.asiafm.databinding.RvItemSearchMusicBinding
import net.asiafm.model.customViewHolder.DataBindBaseViewHolder



class SearchMusicAdapter : BaseQuickAdapter<ItemModel, DataBindBaseViewHolder>(R.layout.rv_item_search_music) {

    override fun convert(holder: DataBindBaseViewHolder, item: ItemModel) {
        val binding: RvItemSearchMusicBinding = holder.getViewBinding() as RvItemSearchMusicBinding
        binding.recyclerData = item
        binding.executePendingBindings()
    }

}