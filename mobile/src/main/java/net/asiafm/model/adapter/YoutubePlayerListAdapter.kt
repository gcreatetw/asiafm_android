package net.asiafm.model.adapter

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import net.asiafm.R
import net.asiafm.asiaAPI.dataModel.Item
import net.asiafm.databinding.RvItemYoutubeBinding
import net.asiafm.model.customViewHolder.DataBindBaseViewHolder


class YoutubePlayerListAdapter(data: MutableList<Item>) :
    BaseQuickAdapter<Item, DataBindBaseViewHolder>(R.layout.rv_item_youtube, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: Item) {

        val binding: RvItemYoutubeBinding = holder.getViewBinding() as RvItemYoutubeBinding
        binding.recyclerData = item
        binding.executePendingBindings()

        holder.setText(R.id.tv_yt_postdate, dateFormat(item.snippet.publishedAt))
    }

    private fun dateFormat(dateInput: String): String {
        val splitDate = dateInput.split('T')
        val date = splitDate[0].split('-')
        return String.format("%s年%s月%s日", date[0], date[1], date[2])
    }

    companion object {

        @BindingAdapter("loadImage")
        @JvmStatic
        fun loadImage(img_youtube: ImageView, url: String) {
            Glide.with(img_youtube.context).load(url).placeholder(R.drawable.main_default_927).centerCrop()
                .error(R.drawable.main_default_927).into(img_youtube)
        }
    }

}
