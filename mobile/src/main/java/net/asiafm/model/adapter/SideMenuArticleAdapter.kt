package net.asiafm.model.adapter

import android.text.Html
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.chad.library.adapter.base.BaseQuickAdapter

import net.asiafm.R
import net.asiafm.asiaAPI.dataModel.Info
import net.asiafm.databinding.RvItemStyle3Binding
import net.asiafm.model.customViewHolder.DataBindBaseViewHolder



class SideMenuArticleAdapter(data: MutableList<Info>) :
    BaseQuickAdapter<Info, DataBindBaseViewHolder>(R.layout.rv_item_style3, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: Info) {
        val binding: RvItemStyle3Binding = holder.getViewBinding() as RvItemStyle3Binding
        binding.recyclerData = item
        binding.executePendingBindings()

        holder.setText(R.id.tv_show_name, Html.fromHtml(item.postTitle, Html.FROM_HTML_MODE_COMPACT))
        holder.setText(R.id.tv_show_time, item.postDate.split(" ")[0].replace("-", "/"))
    }

    companion object {

        @BindingAdapter("setArticleImage")
        @JvmStatic
        fun setArticleImage(img_article: ImageView, url: String) {
            Glide.with(img_article.context).load(url)
                .placeholder(R.drawable.icon_default_showitem)
                .centerCrop()
                .transform(CenterCrop(), RoundedCorners(12))
                .into(img_article)
        }
    }

}
