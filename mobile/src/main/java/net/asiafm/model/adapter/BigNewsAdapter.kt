package net.asiafm.model.adapter

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.zhpan.bannerview.BaseBannerAdapter
import com.zhpan.bannerview.BaseViewHolder
import net.asiafm.R


class BigNewsAdapter : BaseBannerAdapter<String>() {


    override fun bindData(holder: BaseViewHolder<String>?, data: String, position: Int, pageSize: Int) {

//        val dataBinding: ComponentBannerBinding = DataBindingUtil.bind(holder!!.itemView)!!
//        dataBinding.recyclerData


        val imageView: ImageView = holder!!.findViewById(R.id.img)
        Glide.with(holder.itemView.context).load(data).centerCrop()
            .transform(CenterCrop(), RoundedCorners(25))
            .into(imageView)
    }

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.banner_img_layout
    }

}