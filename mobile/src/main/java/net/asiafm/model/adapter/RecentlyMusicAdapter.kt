package net.asiafm.model.adapter

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import net.asiafm.R
import net.asiafm.asiaAPI.dataModel.LatestMusic


class RecentlyMusicAdapter(data: MutableList<LatestMusic>) :
    BaseQuickAdapter<LatestMusic, BaseViewHolder>(R.layout.rv_item_recently_music, data) {

    override fun convert(holder: BaseViewHolder, item: LatestMusic) {

        val itemImage: ImageView = holder.getView(R.id.img_recentlyMusic)
        Glide.with(holder.itemView.context)
            .load(item.picture_url)
            .placeholder(R.drawable.icon_default_recently)
            .into(itemImage)

    }

}
