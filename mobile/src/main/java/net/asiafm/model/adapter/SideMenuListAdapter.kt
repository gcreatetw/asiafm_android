package net.asiafm.model.adapter

import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import net.asiafm.R

class SideMenuListAdapter(data: MutableList<String>) :
    BaseQuickAdapter<String, BaseViewHolder>(R.layout.rv_item_side_menu, data) {

    override fun convert(holder: BaseViewHolder, item: String) {

        val itemTitle: TextView = holder.getView(R.id.tv_title)
        itemTitle.text = item
    }

}



