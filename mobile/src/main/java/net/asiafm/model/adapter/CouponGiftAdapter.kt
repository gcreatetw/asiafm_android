package net.asiafm.model.adapter


import android.net.Uri
import android.os.Build
import android.text.Html
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.chad.library.adapter.base.BaseQuickAdapter
import net.asiafm.Global
import net.asiafm.R
import net.asiafm.asiaAPI.dataModel.Info
import net.asiafm.databinding.RvItemStyle1Binding
import net.asiafm.model.customViewHolder.DataBindBaseViewHolder
import net.asiafm.view.ui.OnlineFragment
import net.asiafm.view.ui.login.LoginActivity


class CouponGiftAdapter(data: MutableList<Info>) : BaseQuickAdapter<Info, DataBindBaseViewHolder>(R.layout.rv_item_style1, data) {

    private val itemWidth = (LoginActivity.windowWidth * 0.35).toInt()
    private var defaultImg = R.drawable.main_default_927

    override fun convert(holder: DataBindBaseViewHolder, item: Info) {

        val binding: RvItemStyle1Binding = holder.getViewBinding() as RvItemStyle1Binding

        val layoutParams = (binding.constraintLayout.layoutParams as? ViewGroup.MarginLayoutParams)

        if (holder.position == data.size - 1) {
            layoutParams!!.setMargins((Global.screenWidth * 0.08).toInt(), 0, (Global.screenWidth * 0.08).toInt(), 0)
        } else {
            layoutParams!!.setMargins((Global.screenWidth * 0.08).toInt(), 0, 0, 0)
        }

        layoutParams.width = (Global.screenWidth * 0.04).toInt()
        binding.constraintLayout.layoutParams = layoutParams


        binding.executePendingBindings()

        holder.getView<ConstraintLayout>(R.id.constraintLayout).layoutParams.width = itemWidth

        when (OnlineFragment.currentRadioIndex) {

            //  亞洲 FM 92.7
            0 -> LifeNewsAdapter.defaultImg = R.drawable.main_default_927
            //  亞太 FM 92.3
            2 -> LifeNewsAdapter.defaultImg = R.drawable.main_default_923
            //  亞太 FM 92.3
            1 -> LifeNewsAdapter.defaultImg = R.drawable.main_default_895

        }

        Glide.with(holder.itemView.context)
            .load(Uri.parse(item.imgUrl))
            .placeholder(defaultImg)
            .error(defaultImg)
            .transform(CenterCrop(), RoundedCorners(24))
            .thumbnail(0.1f).into(holder.getView(R.id.img))


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.setText(R.id.tv_title, Html.fromHtml(item.postTitle, Html.FROM_HTML_MODE_COMPACT))
        } else {
            holder.setText(R.id.tv_title, Html.fromHtml(item.postTitle))
        }

    }

}
