package net.asiafm.model.adapter

import android.net.Uri
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.chad.library.adapter.base.BaseQuickAdapter
import net.asiafm.Global
import net.asiafm.R
import net.asiafm.asiaAPI.dataModel.Info
import net.asiafm.databinding.RvItemLifeNewsBinding
import net.asiafm.model.customViewHolder.DataBindBaseViewHolder
import net.asiafm.view.ui.OnlineFragment


class LifeNewsAdapter(data: MutableList<Info>) :
    BaseQuickAdapter<Info, DataBindBaseViewHolder>(R.layout.rv_item_life_news, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: Info) {
        val binding: RvItemLifeNewsBinding = holder.getViewBinding() as RvItemLifeNewsBinding
        val layoutParams = (binding.clItem.layoutParams as? ViewGroup.MarginLayoutParams)

        if (holder.position == data.size - 1) {
            layoutParams!!.setMargins((Global.screenWidth * 0.08).toInt(), 0, (Global.screenWidth * 0.08).toInt(), 0)
        } else {
            layoutParams!!.setMargins((Global.screenWidth * 0.08).toInt(), 0, 0, 0)
        }

        layoutParams.width = (Global.screenWidth * 0.832).toInt()
        binding.clItem.layoutParams = layoutParams


        binding.recyclerData = item
        binding.executePendingBindings()

        holder.setText(R.id.tv_postdate, item.postDate.split(" ")[0])

    }

    companion object {

        var defaultImg = R.drawable.main_default_927

        @BindingAdapter("setLifeNewsImg")
        @JvmStatic
        fun loadImage(imageView: ImageView, url: String) {

            when (OnlineFragment.currentRadioIndex) {
                0 -> {
                    //  亞洲 FM 92.7
                    defaultImg = R.drawable.main_default_927
                }
                2 -> {
                    //  亞太 FM 92.3
                    defaultImg = R.drawable.main_default_923
                }
                1 -> {
                    //  亞太 FM 92.3
                    defaultImg = R.drawable.main_default_895
                }
            }

            Glide.with(imageView.context)
                .load(Uri.parse(url))
                .placeholder(defaultImg)
                .error(defaultImg)
                .transform(CenterCrop(), RoundedCorners(24))
                .into(imageView)

        }
    }

}
