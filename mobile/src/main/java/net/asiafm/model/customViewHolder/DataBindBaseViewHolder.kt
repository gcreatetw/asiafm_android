package net.asiafm.model.customViewHolder

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.chad.library.adapter.base.viewholder.BaseViewHolder


class DataBindBaseViewHolder(view: View) : BaseViewHolder(view) {

    private var viewBinding: ViewDataBinding? = null

    init {
        viewBinding = DataBindingUtil.bind(itemView)
    }

    fun getViewBinding(): ViewDataBinding? {
        return viewBinding
    }

}