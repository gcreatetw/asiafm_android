package net.asiafm.listener

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import net.asiafm.Global

class FirebaseService : FirebaseMessagingService() {
    private val CHANNEL_ID = "Coder"
    override fun onCreate() {
        super.onCreate()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.notification != null) {
            Log.i("MyFirebaseService", "title " + remoteMessage.notification!!.title);
            Log.i("MyFirebaseService", "body " + remoteMessage.notification!!.body);
            Global.sendNotification(this, remoteMessage.notification!!.title, remoteMessage.notification!!.body)
        }

    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.i("MyFirebaseService", "token $s")
    }
}