package net.asiafm.listener;

import com.google.android.material.appbar.AppBarLayout;

public abstract class AppBarLayoutStateChangeListener implements AppBarLayout.OnOffsetChangedListener {

    public enum ToolbarStatus {
        EXPANDED,//展开
        COLLAPSED,//折叠
        INTERMEDIATE//中间状态
    }

    ToolbarStatus mCurrentToolbarStatus = ToolbarStatus.INTERMEDIATE;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset == 0) {
            if (mCurrentToolbarStatus != ToolbarStatus.EXPANDED) {
                onStateChanged(appBarLayout, ToolbarStatus.EXPANDED);
            }
            mCurrentToolbarStatus = ToolbarStatus.EXPANDED;
        } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
            if (mCurrentToolbarStatus != ToolbarStatus.COLLAPSED) {
                onStateChanged(appBarLayout, ToolbarStatus.COLLAPSED);
            }
            mCurrentToolbarStatus = ToolbarStatus.COLLAPSED;
        } else {
            if (mCurrentToolbarStatus != ToolbarStatus.INTERMEDIATE) {
                onStateChanged(appBarLayout, ToolbarStatus.INTERMEDIATE);
            }
            mCurrentToolbarStatus = ToolbarStatus.INTERMEDIATE;
        }
    }

    public abstract void onStateChanged(AppBarLayout appBarLayout, ToolbarStatus toolbarStatus);
}
