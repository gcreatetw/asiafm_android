package net.asiafm.listener.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import net.asiafm.shared.MusicService.Companion.setIsStartMediaPlayer
import net.asiafm.view.MainActivity
import net.asiafm.view.MainActivity.Companion.radioManagerInstance


class PlayReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val bData = intent.extras!!

        if (radioManagerInstance != null) {
            if (bData.get("msg") != null) {
                when (bData.get("msg")) {
                    "start alarm from mainActivity" -> {
                        setIsStartMediaPlayer(true)
                    }
                    "start alarm from fragment" -> {
                        setIsStartMediaPlayer(true)
                    }
                    "stop alarm from fragment" -> {
                        setIsStartMediaPlayer(false)
                    }
                }
            }
            radioManagerInstance!!.playOrPause(MainActivity.streamURL)
        }
    }
}