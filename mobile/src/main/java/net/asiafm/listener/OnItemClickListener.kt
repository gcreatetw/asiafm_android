package net.asiafm.listener

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View? , position: Int)
}