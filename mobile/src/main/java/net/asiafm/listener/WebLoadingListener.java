package net.asiafm.listener;

public interface WebLoadingListener {
    void notifyViewCancelCall();
}
