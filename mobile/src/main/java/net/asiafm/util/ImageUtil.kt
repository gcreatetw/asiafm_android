package net.asiafm.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream


class ImageUtil{
    @Throws(IllegalArgumentException::class)
    fun base64ConvertBitmap(base64Str: String): Bitmap? {
        val decodedBytes: ByteArray = Base64.decode(base64Str.substring(base64Str.indexOf(",") + 1) , Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(decodedBytes , 0 , decodedBytes.size)
    }

    fun bitmapConvertBase64(bitmap: Bitmap): String {

        val resizedBitmap = Bitmap.createScaledBitmap(bitmap , (bitmap.width*0.2).toInt() , (bitmap.height*0.2).toInt() , false)

        val outputStream = ByteArrayOutputStream()
        resizedBitmap.compress(Bitmap.CompressFormat.JPEG , 100 , outputStream)
        //  base64 encode
    //   val encode = Base64.encode(outputStream.toByteArray()  , Base64.DEFAULT)
        return Base64.encodeToString(outputStream.toByteArray(),Base64.DEFAULT)
    }

    fun BitmapUriConvertBase64(path: String): String {
        //  decode to bitmap
        val bitmap = BitmapFactory.decodeFile(path)
        //  convert to byte array
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG , 100 , baos)
        val bytes = baos.toByteArray()
        //  base64 encode
        val encode = Base64.encode(bytes , Base64.DEFAULT)
        return String(encode)
    }

}