package com.atpc.util.permission

interface GrantListener {

    fun onAgree() // 某权限同意
    fun onDeny() // 某权限拒绝但没选择不再询问项
    fun onDenyNotAskAgain() // 某权限拒绝并选择了不再询问项

}