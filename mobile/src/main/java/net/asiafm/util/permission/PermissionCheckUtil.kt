package com.atpc.util.permission

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.text.TextUtils
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment


/*
* reference article https://blog.csdn.net/csdnzouqi/article/details/106127295
* */
class PermissionCheckUtil(private var context: Context? , private var fragment: Fragment?) {

    companion object {
        val CAMERA = 0x1001 //相机
        val READ_CALENDAR = 0x1002 // 日历
        val WRITE_CALENDAR = 0x1003
        val READ_CONTACTS = 0x1004 // 联系人
        val WRITE_CONTACTS = 0x1005
        val GET_ACCOUNTS = 0x1006
        val ACCESS_FINE_LOCATION = 0x1007 // 位置
        val ACCESS_COARSE_LOCATION = 0x1008
        val RECORD_AUDIO = 0x1009 // 麦克风
        val READ_PHONE_STATE = 0x1010 // 手机
        val CALL_PHONE = 0x1011
        val READ_CALL_LOG = 0x1012
        val WRITE_CALL_LOG = 0x1013
        val ADD_VOICEMAIL = 0x1014
        val USE_SIP = 0x1015
        val PROCESS_OUTGOING_CALLS = 0x1016
        val BODY_SENSORS = 0x1017 // 传感器
        val SEND_SMS = 0x1018 // 短信
        val RECEIVE_SMS = 0x1019
        val READ_SMS = 0x1020
        val RECEIVE_WAP_PUSH = 0x1021
        val RECEIVE_MMS = 0x1022
        val READ_EXTERNAL_STORAGE = 0x1023 // 存储卡
        val WRITE_EXTERNAL_STORAGE = 0x1024
        val ONCE_TIME_APPLY = 0x2000
    }


    private var GROUP_NAME = "" // 声明该变量用来保存危险权限组名

    private var grantListener: GrantListener? = null


    /**
     *
     * 申请危险权限
     * @param requestCode 请求码
     */
    open fun apply(requestCode: Int , grantListener: GrantListener?) {
        this.grantListener = grantListener // 权限同意的监听设置
        var permission: String? = ""
        permission = getPermission(requestCode)
        if (TextUtils.isEmpty(permission)) {
            // 未获取到对应危险权限，一般是请求码不按照给定变量设置或请求的不是危险权限时出现
            return
        }
        // 危险权限申请使用 适配6.0及以上版本
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (fragment != null) {
                fragment!!.requestPermissions(arrayOf(permission) , requestCode)
            } else {
                (context as Activity).requestPermissions(arrayOf(permission) , requestCode)
            }
        }
    }

    /**
     * 根据请求码获取对应危险权限    同时赋值对应的权限组名
     * @param requestCode
     * @return
     */
    private fun getPermission(requestCode: Int): String? {
        var permission = ""
        when (requestCode) {
            CAMERA -> {
                permission = Manifest.permission.CAMERA
                GROUP_NAME = "相机"
            }
            READ_CALENDAR -> {
                permission = Manifest.permission.READ_CALENDAR
                GROUP_NAME = "日历"
            }
            WRITE_CALENDAR -> {
                permission = Manifest.permission.WRITE_CALENDAR
                GROUP_NAME = "日历"
            }
            READ_CONTACTS -> {
                permission = Manifest.permission.READ_CONTACTS
                GROUP_NAME = "联系人"
            }
            WRITE_CONTACTS -> {
                permission = Manifest.permission.WRITE_CONTACTS
                GROUP_NAME = "联系人"
            }
            GET_ACCOUNTS -> {
                permission = Manifest.permission.GET_ACCOUNTS
                GROUP_NAME = "联系人"
            }
            ACCESS_FINE_LOCATION -> {
                permission = Manifest.permission.ACCESS_FINE_LOCATION
                GROUP_NAME = "位置"
            }
            ACCESS_COARSE_LOCATION -> {
                permission = Manifest.permission.ACCESS_COARSE_LOCATION
                GROUP_NAME = "位置"
            }
            RECORD_AUDIO -> {
                permission = Manifest.permission.RECORD_AUDIO
                GROUP_NAME = "麦克风"
            }
            READ_PHONE_STATE -> {
                permission = Manifest.permission.READ_PHONE_STATE
                GROUP_NAME = "手机"
            }
            CALL_PHONE -> {
                permission = Manifest.permission.CALL_PHONE
                GROUP_NAME = "手机"
            }
            READ_CALL_LOG -> {
                permission = Manifest.permission.READ_CALL_LOG
                GROUP_NAME = "手机"
            }
            WRITE_CALL_LOG -> {
                permission = Manifest.permission.WRITE_CALL_LOG
                GROUP_NAME = "手机"
            }
            ADD_VOICEMAIL -> {
                permission = Manifest.permission.ADD_VOICEMAIL
                GROUP_NAME = "手机"
            }
            USE_SIP -> {
                permission = Manifest.permission.USE_SIP
                GROUP_NAME = "手机"
            }
            PROCESS_OUTGOING_CALLS -> {
                permission = Manifest.permission.PROCESS_OUTGOING_CALLS
                GROUP_NAME = "手机"
            }
            BODY_SENSORS -> {
                permission = Manifest.permission.BODY_SENSORS
                GROUP_NAME = "传感器"
            }
            SEND_SMS -> {
                permission = Manifest.permission.SEND_SMS
                GROUP_NAME = "短信"
            }
            RECEIVE_SMS -> {
                permission = Manifest.permission.RECEIVE_SMS
                GROUP_NAME = "短信"
            }
            READ_SMS -> {
                permission = Manifest.permission.READ_SMS
                GROUP_NAME = "短信"
            }
            RECEIVE_WAP_PUSH -> {
                permission = Manifest.permission.RECEIVE_WAP_PUSH
                GROUP_NAME = "短信"
            }
            RECEIVE_MMS -> {
                permission = Manifest.permission.RECEIVE_MMS
                GROUP_NAME = "短信"
            }
            READ_EXTERNAL_STORAGE -> {
                permission = Manifest.permission.READ_EXTERNAL_STORAGE
                GROUP_NAME = "存储卡"
            }
            WRITE_EXTERNAL_STORAGE -> {
                permission = Manifest.permission.WRITE_EXTERNAL_STORAGE
                GROUP_NAME = "存储卡"
            }
        }
        return permission
    }

    /**
     * 用户是否同意权限
     * @param grantResults
     * @return 返回 true 表示用户已同意了，false 则反之
     */
    private fun isGranted(grantResults: IntArray): Boolean {
        return grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
    }

    /**
     * 请求使用某个权限 结果返回
     * @param requestCode
     * @param grantResults
     */
    fun singleApplyResult(requestCode: Int , @NonNull grantResults: IntArray) {
        if (isGranted(grantResults)) {
            // 权限同意
            if (grantListener != null) {
                grantListener!!.onAgree()
            }
        } else {
            // 未同意 分两种情况
            if (ActivityCompat.shouldShowRequestPermissionRationale((context as Activity?)!! , getPermission(requestCode)!!)) {
                // 情况一  用户只拒绝，未选不再提示项
                grantListener!!.onDeny()
            } else {
                // 情况二  用户拒绝了，并选择了不再提示项 这时候可跳转到应用详情页让用户手动选择打开相关权限操作
                grantListener!!.onDenyNotAskAgain()
            }
        }
    }

    /**
     * 一次性同时申请所有的权限 结果返回
     * @param permissions
     * @param grantResults
     */
    fun onceTimeApplyResult(permissions: Array<String> , grantResults: IntArray) {
        for (i in permissions.indices) {
            val permission = permissions[i] // 某个权限
            //            if (grantResults.size > 0 && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
            //            } else {
            //                if (ActivityCompat.shouldShowRequestPermissionRationale((context as Activity?)!! , permission!!)) {
            //                }
            //            }
        }
    }

    /**
     * 根据某权限获取权限组名
     * @param permission
     * @return
     */
    private fun getPermissionGroupName(permission: String?): String {
        var groupName = ""
        when (permission) {
            Manifest.permission.READ_CALENDAR , Manifest.permission.WRITE_CALENDAR -> groupName = "日历"
            Manifest.permission.CAMERA -> groupName = "相机"
            Manifest.permission.READ_CONTACTS , Manifest.permission.WRITE_CONTACTS , Manifest.permission.GET_ACCOUNTS -> groupName = "联系人"
            Manifest.permission.ACCESS_FINE_LOCATION , Manifest.permission.ACCESS_COARSE_LOCATION -> groupName = "位置"
            Manifest.permission.RECORD_AUDIO -> groupName = "麦克风"
            Manifest.permission.READ_PHONE_STATE , Manifest.permission.CALL_PHONE , Manifest.permission.READ_CALL_LOG , Manifest.permission.WRITE_CALL_LOG , Manifest.permission.ADD_VOICEMAIL , Manifest.permission.USE_SIP , Manifest.permission.PROCESS_OUTGOING_CALLS -> groupName =
                "手机"
            Manifest.permission.BODY_SENSORS -> groupName = "传感器"
            Manifest.permission.SEND_SMS , Manifest.permission.RECEIVE_SMS , Manifest.permission.READ_SMS , Manifest.permission.RECEIVE_WAP_PUSH , Manifest.permission.RECEIVE_MMS -> groupName = "短信"
            Manifest.permission.READ_EXTERNAL_STORAGE , Manifest.permission.WRITE_EXTERNAL_STORAGE -> groupName = "存储"
        }
        return groupName
    }


}