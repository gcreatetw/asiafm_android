package net.asiafm.util

import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.facebook.AccessToken
import com.firebase.ui.auth.AuthUI
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.*


class ThirdLogin {

    var mGoogleSignInClient: GoogleSignInClient? = null


    companion object {
        var instance: ThirdLogin? = null

        var user: FirebaseUser? = null
        var mAuth: FirebaseAuth? = null

        fun getThirdLoginInstance(): ThirdLogin {
            if (instance == null) {
                instance = ThirdLogin()
            }
            return instance!!
        }

    }

    //=============================================== Google login =============================================================
    fun googleSignInBuild(mActivity: FragmentActivity) {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("303502601753-f488rk4si5cuhgsesvftkdsj62aut7ve.apps.googleusercontent.com") //從firebase 取得
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(mActivity, gso)

        mAuth = FirebaseAuth.getInstance()
    }

    fun firebaseAuthWithGoogle(mActivity: FragmentActivity, idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(mActivity, OnCompleteListener<AuthResult?> { task ->

                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("ben", "signInWithCredential:success")
                    val user = mAuth!!.currentUser!!
                    Log.d("ben", "user.uid = ${user.uid}")
                    Log.d("ben", "user.displayName = ${user.displayName}")

                } else {
                    // If sign in fails, display a message to the user.
                    Log.d("ben", "signInWithCredential:failure", task.exception)
                }
            })
    }


    fun googleSignOut(mActivity: FragmentActivity) {
        AuthUI.getInstance().signOut(mActivity)
    }

    /* 在 Activity or Fragment 實作下面 function
        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 200) {
                val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    val account = task.getResult(ApiException::class.java)
                    Log.d("ben", "firebaseAuthWithGoogle:" + account.id)
                    ThirdLogin.getThirdLoginInstance().firebaseAuthWithGoogle(requireActivity(),account.idToken!!)
                } catch (e: ApiException) {
                    // Google Sign In failed, update UI appropriately
                    Log.w("ben", "Google sign in failed", e)
                }
            }
        }
    */

    // FB Login

    fun handleFacebookAccessToken(mActivity: FragmentActivity, token: AccessToken) {

        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(mActivity) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("ben", "handleFacebookAccessToken signInWithCredential:success")
                    val user = mAuth!!.currentUser
                    Log.d("ben", "handleFacebookAccessToken user.uid = ${user!!.uid}")
                    Log.d("ben", "handleFacebookAccessToken user.providerData[0].photoUrl = ${user.providerData[0].photoUrl}")
                    Log.d("ben", "handleFacebookAccessToken user.providerData[0].email = ${user.providerData[0].email}")
                    Log.d("ben", "handleFacebookAccessToken user.metadata = ${user.metadata}")
                } else {
                    // If sign in fails, display a message to the user.
                    Log.d("ben", "handleFacebookAccessToken signInWithCredential:failure", task.exception)
                }
            }
    }

}