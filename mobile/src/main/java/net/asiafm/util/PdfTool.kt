package net.asiafm.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.pdf.PdfRenderer
import android.os.ParcelFileDescriptor
import java.io.*

class PdfTool {

    fun pdfToBitmap(context: Context, pdfResource: Int): ArrayList<Bitmap> {

        val bitmaps: ArrayList<Bitmap> = ArrayList()
        val inputStream = context.resources.openRawResource(pdfResource)
        val tempFile = File.createTempFile("pre", "suf")
        copyFile(inputStream, FileOutputStream(tempFile))

        try {
            val renderer = PdfRenderer(
                ParcelFileDescriptor.open(
                    tempFile,
                    ParcelFileDescriptor.MODE_READ_ONLY
                )
            )
            var bitmap: Bitmap
            val pageCount = renderer.pageCount
            for (i in 0 until pageCount) {
                val page = renderer.openPage(i)
                val width: Int =
                    context.resources.displayMetrics.densityDpi / 72 * page.width
                val height: Int =
                    context.resources.displayMetrics.densityDpi / 72 * page.height
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

                val r = Rect(0, 0, width, height)
                page.render(bitmap, r, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)
                bitmaps.add(bitmap)
                // close the page
                page.close()
            }
            // close the renderer
            renderer.close()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return bitmaps
    }

    @Throws(IOException::class)
    private fun copyFile(`in`: InputStream, out: OutputStream) {
        val buffer = ByteArray(1024)
        var read: Int
        while (`in`.read(buffer).also { read = it } != -1) {
            out.write(buffer, 0, read)
        }
    }
}