package net.asiafm

import android.app.Activity
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity

class Global {

    companion object {

        var screenWidth = 0
        var screenHeight = 0
        var isAppOpen = false

        fun getScreenSize(mActivity: Activity) {
            val outMetrics = DisplayMetrics()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val display = mActivity.display
                display?.getRealMetrics(outMetrics)
                screenWidth = outMetrics.widthPixels
                screenHeight = outMetrics.heightPixels
            } else {
                @Suppress("DEPRECATION")
                val display = mActivity.windowManager.defaultDisplay
                @Suppress("DEPRECATION")
                display.getMetrics(outMetrics)
                screenWidth = outMetrics.widthPixels
                screenHeight = outMetrics.heightPixels
            }

        }

        fun hideSoftKeyBroad(activity: FragmentActivity, view: View) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun getStatusBarHeight(activity: Activity): Int {
            val resourceId = activity.resources.getIdentifier("status_bar_height", "dimen", "android")
            return activity.resources.getDimensionPixelSize(resourceId)
        }

        /**
         * check the app is installed
         */
        fun isAppInstalled(context: Context, packagename: String): Boolean {
            var packageInfo: PackageInfo?
            try {
                packageInfo = context.packageManager.getPackageInfo(packagename, 0)
            } catch (e: PackageManager.NameNotFoundException) {
                packageInfo = null
                e.printStackTrace()
            }
            return packageInfo != null
        }


        fun sendNotification(context: Context, messageTitle: String?, messageBody: String?) {
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channelId = "default_notification_channel_id"
            val channelDescription = "Others"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                var notificationChannel = notificationManager.getNotificationChannel(channelId)
                if (notificationChannel == null) {
                    val importance = NotificationManager.IMPORTANCE_HIGH //Set the importance level
                    notificationChannel = NotificationChannel(channelId, channelDescription, importance)
                    notificationChannel.lightColor = Color.GREEN //Set if it is necesssary
                    notificationChannel.enableVibration(true) //Set if it is necesssary
                    notificationManager.createNotificationChannel(notificationChannel)
                }
            }

            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder: NotificationCompat.Builder =
                // in app catch notify
                NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.icon_start_asia_radiofamily_logo)
                    .setColor(ContextCompat.getColor(context, R.color.redE50606))
                    .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.asia_logo))
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setChannelId(channelId)

            if (messageBody!!.split("，").toTypedArray().isNotEmpty()) {
                notificationBuilder.setContentTitle(messageTitle)
            }
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
        }
    }
}