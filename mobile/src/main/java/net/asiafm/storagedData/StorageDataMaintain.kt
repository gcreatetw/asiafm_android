package net.asiafm.storagedData

import android.content.Context
import android.content.Context.MODE_PRIVATE
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONException
import org.json.JSONObject
import java.util.*


object StorageDataMaintain {

    //------------------------------------------- init -------------------------------------------
    fun getIsFistTimeInApp(context: Context): Boolean {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        return editor.getBoolean("isFistTimeInApp" , true)
    }

    fun setIsFistTimeInApp(context: Context , isFistTimeInApp: Boolean) {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE).edit()
        editor.putBoolean("isFistTimeInApp" , isFistTimeInApp)
        editor.apply()
        editor.commit()
    }

    //------------------------------------------- 記住是否第一次安裝 -------------------------------------------
    fun getIsFirstInstallApp(context: Context): Boolean {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        return editor.getBoolean("isFirstInstallApp" , true)
    }

    fun setIsFirstInstallApp(context: Context , isLogin: Boolean) {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE).edit()
        editor.putBoolean("isFirstInstallApp" , isLogin)
        editor.apply()
        editor.commit()
    }

    //------------------------------------------- 記住是否登入 -------------------------------------------
    fun getIsLogin(context: Context): Boolean {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        return editor.getBoolean("isLogin" , false)
    }

    fun setIsLogin(context: Context , isLogin: Boolean) {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE).edit()
        editor.putBoolean("isLogin" , isLogin)
        editor.apply()
        editor.commit()
    }

    //-----------------------------------remember user account / password -------------------------------------------
    fun saveMap(context: Context , inputMap: Map<String , String?>) {
        val pSharedPref = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        if (pSharedPref != null) {
            val jsonObject = JSONObject(inputMap)
            val jsonString = jsonObject.toString()
            pSharedPref.edit().remove("userInfoMap").putString("userInfoMap" , jsonString).apply()
        }
    }

    fun loadMap(context: Context): Map<String , String> {
        val outputMap: MutableMap<String , String> = HashMap()
        val pSharedPref = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        try {
            if (pSharedPref != null) {
                val jsonString = pSharedPref.getString("userInfoMap" , JSONObject().toString())
                if (jsonString != null) {
                    val jsonObject = JSONObject(jsonString)
                    val keysItr = jsonObject.keys()
                    while (keysItr.hasNext()) {
                        val key = keysItr.next()
                        val value = jsonObject.getString(key)
                        outputMap[key] = value
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return outputMap
    }

    //------------------------------------------- 記住是否登入後就撥放 -------------------------------------------
    fun getIsStartRadio(context: Context): Boolean {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        return editor.getBoolean("isStartRadio" , false)
    }

    fun setIsStartRadio(context: Context , isLogin: Boolean) {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE).edit()
        editor.putBoolean("isStartRadio" , isLogin)
        editor.apply()
        editor.commit()
    }

    //------------------------------------------- 記住是否有自動開啟鬧鐘 -------------------------------------------
    fun getIsOpenAlarmStartClock(context: Context): Boolean {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        return editor.getBoolean("isOpenAlarmClock" , false)
    }

    fun setIsOpenAlarmStartClock(context: Context , isOpenAlarmClock: Boolean) {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE).edit()
        editor.putBoolean("isOpenAlarmClock" , isOpenAlarmClock)
        editor.apply()
        editor.commit()
    }
    //------------------------------------------- 記住是否有自動關閉鬧鐘 -------------------------------------------
    fun getIsOpenAlarmStopClock(context: Context): Boolean {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        return editor.getBoolean("isOpenAlarmStopClock" , false)
    }

    fun setIsOpenAlarmStopClock(context: Context , isOpenAlarmStopClock: Boolean) {
        val editor = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE).edit()
        editor.putBoolean("isOpenAlarmStopClock" , isOpenAlarmStopClock)
        editor.apply()
        editor.commit()
    }

    //-----------------------------------remember alarm clock start time -------------------------------------------
    fun setAlarmStartTime(context: Context , inputMap: Map<String , String?>) {
        val pSharedPref = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        if (pSharedPref != null) {
            val jsonObject = JSONObject(inputMap)
            val jsonString = jsonObject.toString()
            pSharedPref.edit().remove("alarmStartTime").putString("alarmStartTime" , jsonString).apply()
        }
    }

    fun getAlarmStartTime(context: Context): Map<String , String> {
        val outputMap: MutableMap<String , String> = HashMap()
        val pSharedPref = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        try {
            if (pSharedPref != null) {
                val jsonString = pSharedPref.getString("alarmStartTime" , JSONObject().toString())
                if (jsonString != null) {
                    val jsonObject = JSONObject(jsonString)
                    val keysItr = jsonObject.keys()
                    while (keysItr.hasNext()) {
                        val key = keysItr.next()
                        val value = jsonObject.getString(key)
                        outputMap[key] = value
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return outputMap
    }

    //-----------------------------------remember alarm clock stop time -------------------------------------------
    fun setAlarmStopTime(context: Context , inputMap: Map<String , String?>) {
        val pSharedPref = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        if (pSharedPref != null) {
            val jsonObject = JSONObject(inputMap)
            val jsonString = jsonObject.toString()
            pSharedPref.edit().remove("alarmStopTime").putString("alarmStopTime" , jsonString).apply()
        }
    }

    fun getAlarmStopTime(context: Context): Map<String , String> {
        val outputMap: MutableMap<String , String> = HashMap()
        val pSharedPref = (context as AppCompatActivity).applicationContext.getSharedPreferences("SavedData" , MODE_PRIVATE)
        try {
            if (pSharedPref != null) {
                val jsonString = pSharedPref.getString("alarmStopTime" , JSONObject().toString())
                if (jsonString != null) {
                    val jsonObject = JSONObject(jsonString)
                    val keysItr = jsonObject.keys()
                    while (keysItr.hasNext()) {
                        val key = keysItr.next()
                        val value = jsonObject.getString(key)
                        outputMap[key] = value
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return outputMap
    }

}