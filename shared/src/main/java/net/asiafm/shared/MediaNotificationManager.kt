package net.asiafm.shared

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationManagerCompat
import androidx.media.app.NotificationCompat


class MediaNotificationManager(private val service: MusicService) {
    private val PRIMARY_CHANNEL = "PRIMARY_CHANNEL_ID"
    private val PRIMARY_CHANNEL_NAME = "PRIMARY"
    private val strAppName: String
    private val strLiveBroadcast: String
    private val resources: Resources
    private val notificationManager: NotificationManagerCompat

    fun startNotify(playbackStatus: String) {
        var icon = R.drawable.icon_notification_pause
        val playbackAction = Intent(service, MusicService::class.java)
        playbackAction.action = MusicService.ACTION_PAUSE
        var action = PendingIntent.getService(service, 1, playbackAction, 0)

        if (playbackStatus == PlaybackStatus.PAUSED) {

            icon = R.drawable.icon_notification_play
            playbackAction.action = MusicService.ACTION_PLAY
            action = PendingIntent.getService(service, 2, playbackAction, 0)
        }

        val stopIntent = Intent(service, MusicService::class.java)
        stopIntent.action = MusicService.ACTION_STOP
        val stopAction = PendingIntent.getService(service, 3, stopIntent, 0)

        /** 點擊通知窗開起APP
        val intent = Intent(service, MainActivity::class.java)
        intent.action = Intent.ACTION_MAIN
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        val pendingIntent = PendingIntent.getActivity(service, 0, intent, 0)
         */

        notificationManager.cancel(NOTIFICATION_ID)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val manager = service.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(PRIMARY_CHANNEL, PRIMARY_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW)
            channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
            manager.createNotificationChannel(channel)
        }
        val largeIcon = BitmapFactory.decodeResource(resources, R.drawable.icon_logo_923)
        //  version 1 , can work
        val builder = androidx.core.app.NotificationCompat.Builder(service, PRIMARY_CHANNEL)
            .setAutoCancel(false)
            .setContentTitle(strLiveBroadcast)
            .setContentText(strAppName)
            .setLargeIcon(largeIcon)
            .setDefaults(androidx.core.app.NotificationCompat.DEFAULT_ALL)
            //.setContentIntent(pendingIntent)
            .setVisibility(androidx.core.app.NotificationCompat.VISIBILITY_PUBLIC)
            .setSmallIcon(android.R.drawable.stat_sys_headset)
            .addAction(icon, "pause", action)
            .addAction(R.drawable.ic_stop_white, "stop", stopAction)
            .setPriority(androidx.core.app.NotificationCompat.PRIORITY_HIGH)
            .setWhen(System.currentTimeMillis())
            .setStyle(NotificationCompat.MediaStyle()
                .setMediaSession(service.mediaSession.sessionToken)
                .setShowActionsInCompactView(0, 1)
                .setShowCancelButton(true)
                .setCancelButtonIntent(stopAction))


        service.startForeground(NOTIFICATION_ID, builder.build())
    }

    fun cancelNotify() {
        service.stopForeground(true)
    }

    companion object {
        const val NOTIFICATION_ID = 555
    }

    init {
        resources = service.resources
        strAppName = resources.getString(R.string.app_name)
        strLiveBroadcast = resources.getString(R.string.live_broadcast)
        notificationManager = NotificationManagerCompat.from(service)
    }
}