package net.asiafm.shared

import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import android.support.v4.media.MediaBrowserCompat.MediaItem
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.text.TextUtils
import android.util.Log
import androidx.media.MediaBrowserServiceCompat
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import org.greenrobot.eventbus.EventBus


open class MusicService : MediaBrowserServiceCompat(), AudioManager.OnAudioFocusChangeListener {

    private val iBinder: IBinder = LocalBinder()

    inner class LocalBinder : Binder() {
        val service: MusicService
            get() = this@MusicService
    }


    override fun onBind(intent: Intent): IBinder {
        return iBinder
    }

    override fun onUnbind(intent: Intent): Boolean {
        if (status == PlaybackStatus.IDLE) stopSelf()
        return super.onUnbind(intent)
    }

    private var transportControls: MediaControllerCompat.TransportControls? = null
    private var onGoingCall = false

    private var wifiLock: WifiManager.WifiLock? = null
    private var audioManager: AudioManager? = null
    private var notificationManager: MediaNotificationManager? = null

    var status: String? = null
        private set


    val isPlaying: Boolean
        get() = status == PlaybackStatus.PLAYING

    private var streamUrl = "https://stream.rcs.revma.com/xpgtqc74hv8uv"


    //MediaSession 就像是 Player 的代理人，要控制 Player，都必須透過 MediaSession 來控制
    lateinit var mediaSession: MediaSessionCompat

    private val playerListener = PlayerListener()
    private val BANDWIDTH_METER = DefaultBandwidthMeter()

    val FANCY_BROWSABLE_ROOT = "/"
    val FANCY_EMPTY_ROOT = "@empty@"

    private lateinit var playerEventListener: PlayerListener

    //ExoPlayer 設定實作

    private val exoPlayer: ExoPlayer by lazy {
        SimpleExoPlayer.Builder(this).build().apply {
            setAudioAttributes(musicAudioAttributes, true)
            setHandleAudioBecomingNoisy(true)
            addListener(playerListener)

        }
    }

    override fun onCreate() {
        super.onCreate()
        val strAppName = resources.getString(R.string.app_name)
        val strLiveBroadcast = resources.getString(R.string.live_broadcast)


        mediaSession = MediaSessionCompat(this, "MyMusicService").apply {
            isActive = true
            //setSessionActivity(sessionActivityPendingIntent)
//            setMetadata(MediaMetadataCompat.Builder()
//                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, "...")
//                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, strAppName)
//                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, strLiveBroadcast)
//                .build())
            setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS or
                    MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)

            setCallback(mediasSessionCallback)
        }
        playerEventListener = PlayerListener()

        sessionToken = mediaSession.sessionToken
        transportControls = mediaSession.controller.transportControls

        onGoingCall = false
        audioManager = getSystemService(AUDIO_SERVICE) as AudioManager
        notificationManager = MediaNotificationManager(this)


        status = PlaybackStatus.IDLE

    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val action = intent.action
        if (TextUtils.isEmpty(action)) return START_NOT_STICKY
        val result = audioManager!!.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
        if (result != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            stop()
            return START_NOT_STICKY
        }
        when {
            action.equals(ACTION_PLAY, ignoreCase = true) -> {
                resume()
                transportControls!!.play()
                setIsStartMediaPlayer(true)
            }
            action.equals(ACTION_PAUSE, ignoreCase = true) -> {
                transportControls!!.pause()
                setIsStartMediaPlayer(false)
            }
            action.equals(ACTION_STOP, ignoreCase = true) -> {
                transportControls!!.stop()
                setIsStartMediaPlayer(false)
            }
        }
        return START_NOT_STICKY
    }

    override fun onAudioFocusChange(focusChange: Int) {
        when (focusChange) {
            AudioManager.AUDIOFOCUS_GAIN -> {
                exoPlayer.volume = 0.8f
                resume()
            }
            AudioManager.AUDIOFOCUS_LOSS -> stop()
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> if (isPlaying) pause()
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> if (isPlaying) exoPlayer.volume = 0.1f
        }
    }

    //  生命週期相關
    override fun onDestroy() {

        pause()
        exoPlayer.release()
        exoPlayer.removeListener(playerEventListener)

        notificationManager!!.cancelNotify()
        mediaSession.release()

        super.onDestroy()
    }

    // 繼承 MediaBrowserServiceCompat 後，有兩個 abstract function 是要實作的：
    // MediaBrowserService 有两个方法来处理客户端连接：
    // onGetRoot() 控制对服务的访问，
    // onGetRoot 有很重要的功能，可以決定要不要讓其他 App 或是服務連接上，連上後可以瀏覽音樂檔案並進行音樂播放相關的操作
    override fun onGetRoot(clientPackageName: String, clientUid: Int, rootHints: Bundle?): BrowserRoot? {

//        val isKnownCaller = allowBrowsing(clientPackageName)
//        return if (isKnownCaller) {
//            BrowserRoot(FANCY_BROWSABLE_ROOT, null)
//        } else {
//            if (BuildConfig.DEBUG) {
//                BrowserRoot(FANCY_EMPTY_ROOT, null)
//            } else {
//                null
//            }
//        }
        return MediaBrowserServiceCompat.BrowserRoot("root", null)
    }

    // onLoadChildren() 使客户端能够构建和显示 内容层次结构菜单。
    //首先是 onLoadChildren 屬於瀏覽歌曲相關，可以看到會傳入 parentId，然後再將 result 傳出
    override fun onLoadChildren(parentId: String, result: Result<MutableList<MediaItem>>) {
        result.sendResult(ArrayList())
    }


//    private fun allowBrowsing(clientPackageName: String): Boolean {
//        return clientPackageName == packageName
//    }


    private val musicAudioAttributes = AudioAttributes.Builder().setContentType(C.CONTENT_TYPE_MUSIC).setUsage(C.USAGE_MEDIA).build()

    //設定 listener，去監聽播放器的狀態
    private inner class PlayerListener : Player.Listener {
        // IDLE The player does not have any media to play.
        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            status = when (playbackState) {
                Player.STATE_BUFFERING -> PlaybackStatus.LOADING
                Player.STATE_ENDED -> PlaybackStatus.STOPPED
                Player.STATE_IDLE -> PlaybackStatus.IDLE
                Player.STATE_READY -> if (playWhenReady) PlaybackStatus.PLAYING else PlaybackStatus.PAUSED
                else -> PlaybackStatus.IDLE
            }
            Log.d("ben", "status = $status")
            if (status != PlaybackStatus.IDLE) notificationManager!!.startNotify(status!!)
            EventBus.getDefault().post(status)
        }

    }


    private val userAgent: String
        get() = Util.getUserAgent(this, javaClass.simpleName)


    //===================================== 撥放器行為 =============================================
    private val mediasSessionCallback = object : MediaSessionCompat.Callback() {
        override fun onPause() {
            super.onPause()
            pause()
        }

        override fun onStop() {
            super.onStop()
            stop()
            notificationManager!!.cancelNotify()
        }

        override fun onPlay() {
            super.onPlay()
            resume()
        }

        override fun onSkipToQueueItem(queueId: Long) {}

        override fun onSeekTo(position: Long) {}

        override fun onPlayFromMediaId(mediaId: String?, extras: Bundle?) {}

        override fun onSkipToNext() {}

        override fun onSkipToPrevious() {}

        override fun onCustomAction(action: String?, extras: Bundle?) {}

        override fun onPlayFromSearch(query: String?, extras: Bundle?) {}
    }

    private fun play(streamUrl: String) {
        this.streamUrl = streamUrl
        if (wifiLock != null && !wifiLock!!.isHeld) {
            wifiLock!!.acquire()
        }
        val dataSourceFactory = DefaultDataSourceFactory(applicationContext, userAgent, BANDWIDTH_METER)
        val mediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
            .setExtractorsFactory(DefaultExtractorsFactory())
            .createMediaSource(Uri.parse(streamUrl))
        exoPlayer.prepare(mediaSource)
        exoPlayer.playWhenReady = true
    }

    private fun resume() {
        if (streamUrl != null) play(streamUrl)
    }

    private fun pause() {
        exoPlayer.playWhenReady = false
        audioManager!!.abandonAudioFocus(this)
        //wifiLockRelease()
    }

    fun stop() {
        exoPlayer.stop()
        audioManager!!.abandonAudioFocus(this)
    }

    fun release() {
        exoPlayer.pause()
        exoPlayer.release()
    }

    fun playOrPause(url: String) {
        if (streamUrl != null && streamUrl == url) {
            //  相同頻道下，沒播放就播音樂
            if (!isPlaying) {
                play(streamUrl)
            } else {
                if (!isStartMediaPlayer) {
                    pause()
                }
            }
        } else {
            //  切換頻道後，先暫停再撥放
            if (isPlaying) {
                pause()
            }
            play(url)
        }
    }


    companion object {
        const val ACTION_PLAY = "net.asiafm.viewmain.ACTION_PLAY"
        const val ACTION_PAUSE = "net.asiafm.viewmain.ACTION_PAUSE"
        const val ACTION_STOP = "net.asiafm.viewmain.ACTION_STOP"

        var isStartMediaPlayer = false


        fun setIsStartMediaPlayer(boolean: Boolean) {
            isStartMediaPlayer = boolean
        }

        fun getIsStartMediaPlayer(): Boolean {
            return isStartMediaPlayer
        }
    }

}